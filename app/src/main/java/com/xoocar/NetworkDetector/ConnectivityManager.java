package com.xoocar.NetworkDetector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

import com.xoocar.EventBusModels.InternetAvailable;
import com.xoocar.SessionManager.SessionManager;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sushant on 8/7/17.
 */

public class ConnectivityManager extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        android.net.ConnectivityManager conn = (android.net.ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conn.getActiveNetworkInfo();
        SessionManager sessionManager = new SessionManager(context);

        // Checks the user prefs and the network connection. Based on the result, decides whether
        // to refresh the display or keep the current display.
        // If the userpref is Wi-Fi only, checks to see if the device has a Wi-Fi connection.
        if (/*WIFI.equals(sPref) &&*/ networkInfo != null && networkInfo.getType() == android.net.ConnectivityManager.TYPE_WIFI) {
            // If device has its Wi-Fi connection, sets refreshDisplay
            // to true. This causes the display to be refreshed when the user
            // returns to the app.
//            refreshDisplay = true;
//            Toast.makeText(context, R.string.wifi_connected, Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new InternetAvailable(true));
            sessionManager.setInternetAvailable(true);

            // If the setting is ANY network and there is a network connection
            // (which by process of elimination would be mobile), sets refreshDisplay to true.
        } else if (/*ANY.equals(sPref) &&*/ networkInfo != null) {
//            refreshDisplay = true;

            // Otherwise, the app can't download content--either because there is no network
            // connection (mobile or Wi-Fi), or because the pref setting is WIFI, and there
            // is no Wi-Fi connection.
            // Sets refreshDisplay to false.
            EventBus.getDefault().post(new InternetAvailable(true));
            sessionManager.setInternetAvailable(true);

        } else {
            EventBus.getDefault().post(new InternetAvailable(false));
            sessionManager.setInternetAvailable(false);

        }

    }
}