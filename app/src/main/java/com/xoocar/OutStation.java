package com.xoocar;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.xoocar.Requests.IntercityCities.GetCitiesRequest;
import com.xoocar.Requests.IntercityCities.GetCitiesResponceData;
import com.xoocar.Requests.IntercityCities.GetCitiesResponceFields;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsRequestData;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsRequestFields;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsResponceData;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsResponceFields;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateRequestData;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateRequestFields;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateResponceData;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import org.angmarch.views.NiceSpinner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 9/11/17.
 */

public class OutStation extends AppCompatActivity implements View.OnClickListener {

    static final int START_DATE = 1;
    static final int END_DATE = 2;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 2;
    int cur = 0;
    Calendar myCalendar;
    private LinearLayout roundTripLinear;
    private LinearLayout round_trip_outstation_layout;
    private TextView round_trip_outstation;
    private LinearLayout one_way_trip_outstation_layout;
    private TextView one_way_trip_outstation;
    private TextView date_time_outstation;
    ViewGroup view_group;
    private TextView est_outstation;
    private SessionManager sessionManager;
    private List<GetCitiesResponceData> citiesList;
    private TextView tripTypeText;
    private NiceSpinner niceSpinner;
    private ArrayList<String> dataset;
    private NiceSpinner niceSpinner2;
    private ArrayList<String> dataset2;
    private ProgressDialog pd;
    private List<GetIntercityDestsResponceData> destinationList;
    private TextView pickupLocationText;
    private String endDate;
    private String startDate;
    private Context ctx;
    private TextView dateTimeReturnTrip;
    private TextView fareHachBack;
    private TextView fareSedan;
    private TextView fareSuv;
    private TextView fareAnyother;
    private List<GetRideEstimateResponceData> estimateList;
    private String pickupCityid;
    private String dropCityId;
    private String isReturn="2";
    private String cityName;
    private String dropCityName;
    private String startTime;
    private String returnTime;
    private long startUnix=0;
    private long returnUnix=0;
    private int fareHachBk;
    private int fareSdn;
    private int fareSv;
    private int fareAnyOtr;
    private int estDis;
    private long days=0;
    private boolean isOneWay;

    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            int mMinute;
            int mHour;
            if (cur == START_DATE) {
                int month = monthOfYear + 1;
                startDate = dayOfMonth + "/" + month + "/" + year;

                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog = new TimePickerDialog(OutStation.this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                        String min ;
                                        if( minute < 10 ) {
                                            min = "0" + minute;
                                        } else {
                                            min= String.valueOf(minute);
                                        }
                                        startTime = hourOfDay + ":" + min;
                                        startUnix=convertDate(startDate + " " + startTime);

                                        if( startUnix - System.currentTimeMillis() >= 2*3600000 ){
                                            if( startUnix-System.currentTimeMillis() > 604800000 ){
                                                date_time_outstation.setText("");
                                                Toast.makeText(OutStation.this
                                                        , "Travel date cannot be greater than 7 days"
                                                        , Toast.LENGTH_LONG).show();
                                            } else {
                                                if(isReturn.equals("1")){
                                                    getRideEstimate();
                                                }
                                                date_time_outstation.setText(startDate + " " + startTime);
                                            }
                                        } else {
                                            Toast.makeText(ctx, "Booking must be 2 hr prior to travel", Toast.LENGTH_LONG).show();
                                        }

                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
            } else {
//              updateEnd();
                int month = monthOfYear + 1;
                endDate = dayOfMonth + "/" + month + "/" + year;

                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(OutStation.this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    String mins;
                                    if( minute < 10 ){
                                        mins = "0" + minute;
                                    }else{
                                        mins= String.valueOf(minute);
                                    }

                                    returnTime = hourOfDay + ":" + mins;
                                    returnUnix=convertDate(endDate + " " + returnTime);
                                    if( returnUnix-startUnix < 21600000 ) {
                                        dateTimeReturnTrip.setText("");
                                        Toast.makeText(OutStation.this, "Return time must be greater than 6 hours", Toast.LENGTH_SHORT).show();
                                    } else {
                                        days= TimeUnit.DAYS.convert(returnUnix-startUnix, TimeUnit.MILLISECONDS);
                                        dateTimeReturnTrip.setText(endDate + " " + returnTime);

                                            getRideEstimate();

                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Slide slide=new Slide();
//        slide.setDuration(500);
        getWindow().setSharedElementExitTransition(TransitionInflater.from(this).inflateTransition(R.transition.activity_explode));

        setContentView(R.layout.activity_outstation);
//        setupWindowAnimations();

        LinearLayout to_city_Outstation_layout = (LinearLayout) findViewById(R.id.to_city_Outstation_layout);
        roundTripLinear = (LinearLayout) findViewById(R.id.roundTripLinear);
        LinearLayout pickup_location_outstation_layout = (LinearLayout) findViewById(R.id.pickup_location_outstation_layout);
        round_trip_outstation_layout = (LinearLayout) findViewById(R.id.round_trip_outstation_layout);
        view_group =  findViewById(R.id.view_group);
        round_trip_outstation = (TextView) findViewById(R.id.round_trip_outstation);
        one_way_trip_outstation_layout = (LinearLayout) findViewById(R.id.one_way_trip_outstation_layout);
        one_way_trip_outstation = (TextView) findViewById(R.id.one_way_trip_outstation);
        date_time_outstation = (TextView) findViewById(R.id.date_time_outstation);
        LinearLayout cab_type_outstation_layout = (LinearLayout) findViewById(R.id.cab_type_outstation_layout);
        ImageView img_cab_type_outstation = (ImageView) findViewById(R.id.img_cab_type_outstation);
        TextView txt_cab_type_outstation = (TextView) findViewById(R.id.txt_cab_type_outstation);
        dateTimeReturnTrip = (TextView) findViewById(R.id.dateTimeReturnTrip);
        tripTypeText = (TextView) findViewById(R.id.tripTypeText);

        ImageView hachBackIcon = (ImageView) findViewById(R.id.hachBackIcon);

        RelativeLayout anyOtherNext = findViewById(R.id.anyOtherNext);
        RelativeLayout hachBackNext = findViewById(R.id.hachBackNext);
        RelativeLayout sedanNext = findViewById(R.id.sedanNext);
        RelativeLayout suvNext = findViewById(R.id.suvNext);

        fareHachBack = (TextView) findViewById(R.id.fareHachback);
        fareSedan= (TextView) findViewById(R.id.fareSedan);
        fareSuv= (TextView) findViewById(R.id.fareSuv);
        fareAnyother= (TextView) findViewById(R.id.fareAnyother);

        LinearLayout est_outstation_layout = (LinearLayout) findViewById(R.id.est_outstation_layout);
        est_outstation = findViewById(R.id.est_outstation);
        pickupLocationText = findViewById(R.id.pickupLocationText);
        LinearLayout editPickup = findViewById(R.id.editPickup);

        RelativeLayout returnDateRelative = findViewById(R.id.returnDateRelative);
        RelativeLayout travelTimeRelative = findViewById(R.id.travelTimeRelative);

        sessionManager = new SessionManager(OutStation.this);
        citiesList = new ArrayList<>();
        destinationList = new ArrayList<>();
        pd = new ProgressDialog(OutStation.this);
        myCalendar = Calendar.getInstance();
        ctx=OutStation.this;
        getCityList();

        niceSpinner = findViewById(R.id.nice_spinner);
        dataset = new ArrayList<>();
        niceSpinner.setText("From city");
        niceSpinner.setTextColor(Color.parseColor("#000000"));

        niceSpinner2 = findViewById(R.id.nice_spinner2);
        dataset2 = new ArrayList<>();
        niceSpinner2.setText("To city");
        niceSpinner2.setTextColor(Color.parseColor("#000000"));

        niceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pickupCityid=citiesList.get(i).getaCityId();
                getDestinationCities(citiesList.get(i).getaCityId());
                cityName = citiesList.get(i).gettCityName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        niceSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dropCityId=destinationList.get(i).getCityTo();
                dropCityName = destinationList.get(i).gettCityName();
                getRideEstimate();
                isOneWay = !destinationList.get(i).getIs_oneway().equals("2");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        to_city_Outstation_layout.setOnClickListener(this);
        pickup_location_outstation_layout.setOnClickListener(this);
        round_trip_outstation_layout.setOnClickListener(this);
        one_way_trip_outstation_layout.setOnClickListener(this);
        cab_type_outstation_layout.setOnClickListener(this);
        img_cab_type_outstation.setOnClickListener(this);
        txt_cab_type_outstation.setOnClickListener(this);
        est_outstation_layout.setOnClickListener(this);
        est_outstation.setOnClickListener(this);
        editPickup.setOnClickListener(this);

        returnDateRelative.setOnClickListener(this);
        travelTimeRelative.setOnClickListener(this);

        anyOtherNext.setOnClickListener(this);
        hachBackNext.setOnClickListener(this);
        sedanNext.setOnClickListener(this);
        suvNext.setOnClickListener(this);
        hachBackIcon.setOnClickListener(this);

        pickupLocationText.setText(sessionManager.getPickupAddress());
        startUnix=System.currentTimeMillis();
        startDate=CommonMethods.convertDateUnixToStringFormat(System.currentTimeMillis()+7200000,"dd/MM/yyyy");
        date_time_outstation.setText(CommonMethods.convertDateUnixToStringFormat(System.currentTimeMillis()+7200000,"dd/MM/yyyy HH:mm"));
    }

    private void getDestinationCities(String cityId) {

        pd.setMessage("Getting destination cities...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetIntercityDestsResponceFields> call = request.getIntercityDestination(sessionManager.getAuthToken()
                , "api/intercityapi/citylistfrom/format/json/"
                , new GetIntercityDestsRequestFields(new GetIntercityDestsRequestData(cityId)));

        call.enqueue(new Callback<GetIntercityDestsResponceFields>() {

            @Override
            public void onResponse(Call<GetIntercityDestsResponceFields> call, Response<GetIntercityDestsResponceFields> response) {
                Log.d("aaaa", response.toString());
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(OutStation.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        dataset2.clear();
                        destinationList = response.body().getDataArray();
                        for (GetIntercityDestsResponceData city :
                                destinationList) {
                            dataset2.add(city.gettCityName());
                        }
                        niceSpinner2.attachDataSource(dataset2);

                        dropCityId=destinationList.get(0).getCityTo();
                        dropCityName = destinationList.get(0).gettCityName();
                        getRideEstimate();
                        isOneWay = !destinationList.get(0).getIs_oneway().equals("2");
                    }
                }
            }

            @Override
            public void onFailure(Call<GetIntercityDestsResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }

    private void getCityList() {

        pd.setMessage("Getting cities...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetCitiesResponceFields> call = request.getIntercityCities(sessionManager.getAuthToken()
                , "api/intercityapi/citylist/format/json/"
                , new GetCitiesRequest(""));

        call.enqueue(new Callback<GetCitiesResponceFields>() {

            @Override
            public void onResponse(Call<GetCitiesResponceFields> call, Response<GetCitiesResponceFields> response) {
                Log.d("aaaa", response.toString() );
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                if ( response.body() != null ) {
                    if ( response.body().getResponseCode() == 410 ) {
                        Toast.makeText(OutStation.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if ( response.body().getResponseCode() == 400 ) {
                        citiesList = response.body().getDataArray();
                        dataset.clear();
                        for ( GetCitiesResponceData city :
                                citiesList ) {
                            dataset.add(city.gettCityName());
                        }
                        niceSpinner.attachDataSource(dataset);

                        pickupCityid=citiesList.get(0).getaCityId();
                        getDestinationCities(citiesList.get(0).getaCityId());
                        cityName = citiesList.get(0).gettCityName();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCitiesResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.editPickup:
                getPickupAddress();
                break;

            case R.id.round_trip_outstation_layout:
                onClickRoundTrip();
                break;

            case R.id.one_way_trip_outstation_layout:
                if( isOneWay ) {
                    onClickOneWay();
                } else {
                    Toast.makeText(ctx, "One way trip is not available to this city", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.returnDateRelative:
                onClickReturnDate();
                break;

            case R.id.travelTimeRelative:
                onClickTravelDate();
                break;

            case R.id.anyOtherNext:
                onClickAnyOther();
                break;

            case R.id.hachBackNext:
                onClickHachBack(view);
                break;

//            case R.id.hachBackIcon:
//                onClickHachBack(view);
//                break;

            case R.id.sedanNext:
                onClickSedan();
                break;

            case R.id.suvNext:
                onClickSuv();
                break;
        }
    }

    private void onClickSuv() {
        proceedBooking("SUV");
    }

    private void proceedBooking(String type) {

        if( estimateList != null && estimateList.get(2) != null ) {
            if ( !pickupLocationText.getText().toString().equals("Pickup Location") ) {
                if( startUnix != 0 ) {
                    if( isReturn.equals("2")  ) {
                        if( returnUnix != 0 ){
                            proceed(type);
                        } else {
                            Toast.makeText(ctx, "Enter return date", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        proceed(type);
                    }
                } else {
                    Toast.makeText(ctx, "Enter pickup date", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ctx, "Select pickup address", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ctx, "Select a destination first", Toast.LENGTH_SHORT).show();
        }
    }

    private void proceed(String type) {
        View view1 = null;
        Intent i = new Intent(OutStation.this, ConfirmOutstation.class);
        switch (type) {
            case "SUV":
                view1 = findViewById(R.id.iconSuv);
                i.putExtra(Constants.TOTALESTIMATEDFARE, String.valueOf(fareSv));
                i.putExtra(Constants.CATEGORY_TYPE, "6");
                i.putExtra(Constants.DRIVER_PER_DAY_CHARGE, estimateList.get(2).getDA());
                i.putExtra(Constants.PER_KM_CHARGE, estimateList.get(2).getCharge());

                break;

            case "SEDAN":

                view1 = findViewById(R.id.iconSedan);
                i.putExtra(Constants.TOTALESTIMATEDFARE, String.valueOf(fareSdn));
                i.putExtra(Constants.CATEGORY_TYPE, "3");
                i.putExtra(Constants.DRIVER_PER_DAY_CHARGE, estimateList.get(1).getDA());
                i.putExtra(Constants.PER_KM_CHARGE, estimateList.get(1).getCharge());
                break;

            case "HATCHBACK":

                view1 = findViewById(R.id.hachBackIcon);
                i.putExtra(Constants.TOTALESTIMATEDFARE, String.valueOf(fareHachBk));
                i.putExtra(Constants.CATEGORY_TYPE, "2");
                i.putExtra(Constants.DRIVER_PER_DAY_CHARGE, estimateList.get(0).getDA());
                i.putExtra(Constants.PER_KM_CHARGE, estimateList.get(0).getCharge());

                break;

            case "ANY OTHER":
                view1 = findViewById(R.id.hachBackIcon);
                i.putExtra(Constants.TOTALESTIMATEDFARE, String.valueOf(fareAnyOtr));
                i.putExtra(Constants.CATEGORY_TYPE, "7");
                i.putExtra(Constants.DRIVER_PER_DAY_CHARGE, estimateList.get(3).getDA());
                i.putExtra(Constants.PER_KM_CHARGE, estimateList.get(3).getCharge());

                break;
        }


        view1.setTransitionName("selectedbutton");
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view1, view1.getTransitionName());


        i.putExtra(Constants.DISTANCE, String.valueOf(estDis));
        i.putExtra(Constants.FROMCITY, cityName);
        i.putExtra(Constants.PICKUP_CITY_ID, pickupCityid);
        i.putExtra(Constants.DROP_CITY_ID, dropCityId);
        i.putExtra(Constants.TOCITY, dropCityName);
        i.putExtra(Constants.PICKUPLOCATIONIMG, pickupLocationText.getText().toString());
        i.putExtra(Constants.TRIPTYPE, isReturn);
        i.putExtra(Constants.TRAVELDATE, startDate);
        i.putExtra(Constants.PICKUPTIME, String.valueOf(startUnix));
        i.putExtra(Constants.CAB_TYPE, type);
        i.putExtra(Constants.NUM_OF_DAYS, String.valueOf(days + 1));

        startActivity(i, options.toBundle());
    }

    private void onClickSedan() {
        proceedBooking("SEDAN");
    }

    private void onClickHachBack(View view) {
        proceedBooking("HATCHBACK");
    }

    private void onClickAnyOther() {
        proceedBooking("ANY OTHER");
    }

    private void onClickTravelDate() {
        cur = START_DATE;
        new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR)
                , myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void onClickReturnDate() {
        cur = END_DATE;
        new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR)
                , myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void onClickOneWay() {
        isReturn="1";
        getRideEstimate();

        Explode slide=new Explode();
        slide.setDuration(1000);
        TransitionManager.beginDelayedTransition(view_group,slide);

        roundTripLinear.setVisibility(View.GONE);

        one_way_trip_outstation_layout.setBackgroundResource(R.drawable.round_trip_background_round);
        round_trip_outstation_layout.setBackgroundResource(R.drawable.one_way_trip_background_round);
        round_trip_outstation.setTextColor(Color.parseColor("#ffffff"));
        one_way_trip_outstation.setTextColor(Color.parseColor("#000000"));
        tripTypeText.setText("One Way");
//        oneWayLinear.setVisibility(View.GONE);
    }

    private void onClickRoundTrip() {
        isReturn="2";

        Explode slide=new Explode();
        slide.setDuration(1000);
        TransitionManager.beginDelayedTransition(view_group,slide);
        roundTripLinear.setVisibility(View.VISIBLE);

        round_trip_outstation.setTextColor(Color.parseColor("#000000"));
        one_way_trip_outstation.setTextColor(Color.parseColor("#ffffff"));
        round_trip_outstation_layout.setBackgroundResource(R.drawable.round_trip_background_round);
        one_way_trip_outstation_layout.setBackgroundResource(R.drawable.one_way_trip_background_round);
        tripTypeText.setText("Round Trip");
        getRideEstimate();
    }

    private void getRideEstimate() {

        pd.setMessage("Getting details...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetRideEstimateResponceFields> call = request.getIntercityRideEstimate(sessionManager.getAuthToken()
                , "api/intercityapi/getpricelist/format/json/"
                , new GetRideEstimateRequestFields(new GetRideEstimateRequestData(pickupCityid,dropCityId,isReturn,String.valueOf(days+1))));

        call.enqueue(new Callback<GetRideEstimateResponceFields>() {

            @Override
            public void onResponse(Call<GetRideEstimateResponceFields> call, Response<GetRideEstimateResponceFields> response) {
                Log.d("aaaa", response.toString());
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(OutStation.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        estimateList=response.body().getDataArray();

                        double d = estimateList.get(0).getDistance();
                        double d1 = Double.parseDouble(estimateList.get(0).getEstimatedprice());
                        double d2 = Double.parseDouble(estimateList.get(1).getEstimatedprice());
                        double d3 = Double.parseDouble(estimateList.get(2).getEstimatedprice());
                        double d4 = Double.parseDouble(estimateList.get(3).getEstimatedprice());

                        estDis = (int) d;
                        fareHachBk = (int) d1;
                        fareSdn = (int) d2;
                        fareSv = (int) d3;
                        fareAnyOtr = (int) d4;

                        est_outstation.setText(String.valueOf(estDis));

                        fareHachBack.setText(getString(R.string.rupee_symbol) + " " + fareHachBk);
                        fareSedan.setText(getString(R.string.rupee_symbol) + " " + fareSdn);
                        fareSuv.setText(getString(R.string.rupee_symbol) + " " + fareSv);
                        fareAnyother.setText(getString(R.string.rupee_symbol) + " " + fareAnyOtr);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetRideEstimateResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }

    private void getPickupAddress() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setCountry("IN")
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);


        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                pickupLocationText.setText(place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Toast.makeText(this, "No place found please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private long convertDate(String str_date) {
//        String str_date = month + "-" + day + "-" + yr;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = null;
        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long output = date.getTime() / 1000L;
        String str = Long.toString(output);
       return Long.parseLong(str) * 1000;
    }
}
