package com.xoocar;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.xoocar.EventBusModels.Notifications;
import com.xoocar.Realm.RealmManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sushant on 8/10/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    JSONObject jsonObject;
    private String rideId,driverId;
    private String notType="0";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                handleNow();
        }

        try {
            notType=remoteMessage.getData().get("notification_type");
            if( remoteMessage.getData().get("driver_array") != null ) {
                jsonObject = new JSONObject(remoteMessage.getData().get("driver_array"));
                rideId = jsonObject.optString("rideid");
                driverId = jsonObject.optString("driver_id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch ( notType ) {
            case Constants.ACCEPTED_BY_DRIVER :
                sendNotification(remoteMessage.getData().get("message"),"Booking Confirmed");
                RealmManager.addNotification(remoteMessage.getData().get("message"));
                EventBus.getDefault().post(new Notifications(notType,driverId,rideId));
                break;
            case Constants.REJECTED_BY_DRIVER :
                RealmManager.addNotification(remoteMessage.getData().get("message"));
                EventBus.getDefault().post(new Notifications(notType,driverId,rideId));
                break;
            case Constants.REJECTED_AFTER_SCHEDULE :
                EventBus.getDefault().post(new Notifications(notType,driverId,rideId));
                break;
            case Constants.RIDE_COMPLETED :
                EventBus.getDefault().post(new Notifications(notType,driverId,rideId));
                break;
            case Constants.RUNNING:
                EventBus.getDefault().post(new Notifications(notType,driverId,rideId));
                break;
            case Constants.GENERAL_NOTIFICATION:
                sendNotification(remoteMessage.getData().get("message"),"Information");
                RealmManager.addNotification(remoteMessage.getData().get("message"));
                break;
//            case "5" : break;
            default : break;
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }
    // [END receive_message]


    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody,String title) {
        Intent intent = new Intent(this, OffersAndFreeRides.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setColor(Color.parseColor("#000000"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
