package com.xoocar.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.xoocar.fragments.RideCancleFragment;
import com.xoocar.fragments.RideCompleteFragment;
import com.xoocar.fragments.RidePendingFragment;
import com.xoocar.fragments.RideRunningFragment;
import com.xoocar.fragments.RideScheduleFragment;


/**
 * Created by sushant on 9/8/17.
 */

public class RideHistoryPagerAdapter extends FragmentStatePagerAdapter {

    private int mCount;

    public RideHistoryPagerAdapter(FragmentManager fm, int Count) {
        super(fm);
        this.mCount = Count;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new RideCompleteFragment();
            case 1:
                return new RideRunningFragment();
            case 2:
                return new RideScheduleFragment();
            case 3:
                return new RideCancleFragment();
            case 4:
                return new RidePendingFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mCount;
    }
}
