package com.xoocar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.R;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;

import java.util.ArrayList;

/**
 * Created by sushant on 8/5/17.
 */

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.FollowerViewHolder> {

    private Context context;
    private ArrayList<ResponseDataArray> couponList;
    private AdapterToMain adapterToMain;

    public CouponAdapter( ArrayList<ResponseDataArray> couponList, Context ctx) {
        this.couponList =couponList;
        adapterToMain= (AdapterToMain) ctx;
        context=ctx;
    }

    @Override
    public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_coupon, parent, false);
        return new FollowerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, final int position) {
        final ResponseDataArray coupon = couponList.get(holder.getAdapterPosition());
        holder.couponName.setText(coupon.getCouponName());
        holder.couponDesc.setText(coupon.getDesc());

        holder.applyCouponBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterToMain.addCoupon(coupon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class FollowerViewHolder extends RecyclerView.ViewHolder {

        private TextView couponName,couponDesc;
        private Button applyCouponBtn;

        FollowerViewHolder(View convertView) {
            super(convertView);

            couponName =  convertView.findViewById(R.id.couponName);
            couponDesc =  convertView.findViewById(R.id.couponDesc);
            applyCouponBtn=convertView.findViewById(R.id.applyCouponBtn);
        }

    }
}
