package com.xoocar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.xoocar.RideScheduled;
import com.xoocar.Constants;
import com.xoocar.R;
import com.xoocar.Realm.RideHistoryRealm;
import com.xoocar.Requests.GetRideById.RequestData;
import com.xoocar.RideRunningScreen;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.TripHistory;
import com.xoocar.WaitingScreenIntercity;

import java.util.List;

/**
 * Created by sushant on 8/20/17.
 */

public class RideHistoryListAdapter extends RecyclerView.Adapter<RideHistoryListAdapter.RideHistoryViewHolder> {

    private Context context;
    private List<RideHistoryRealm> couponList;

    public RideHistoryListAdapter(List<RideHistoryRealm> couponList, Context ctx) {
        this.couponList =couponList;
        context=ctx;
    }

    @Override
    public RideHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_row_triphistory, parent, false);
        return new RideHistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RideHistoryViewHolder holder, final int position) {
         final RideHistoryRealm ride = couponList.get(holder.getAdapterPosition());
        holder.driverName.setText(ride.getDname());
        holder.dropAddress.setText(ride.getDestination());
        holder.pickupAddress.setText(ride.getSource());
        holder.rideDate.setText(ride.getD_CreatedOn());

        String url = "http://velenesa.com/vele/index.php/assets/profileimage/" + ride.getDriver_pic();

        Glide.with(context)
                .load(url)
                .asBitmap()
                .into(new BitmapImageViewTarget(holder.driverImage) {

                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        super.onResourceReady(resource, glideAnimation);

                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.driverImage.setImageDrawable(circularBitmapDrawable);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                    }
                });

        holder.linearParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( couponList.get(position).getBooking_status().equals("Running") ){
                    Intent i=new Intent(context, RideRunningScreen.class);
                    i.putExtra(Constants.RIDE_ID,couponList.get(position).getOid());
                    new SessionManager(context).setDestinationAddress(couponList.get(position).getDestination());
                    new SessionManager(context).setPickupAddress(couponList.get(position).getSource());
                    context.startActivity(i);

                } else if( couponList.get(position).getBooking_status().equals("Schedule") ) {
                    Intent i=new Intent(context, RideScheduled.class);

                    RequestData cab= new RequestData();
                    cab.setCrnNo( couponList.get(position).getCrn_no() );
                    cab.setFirstName( couponList.get(position).getDname() );
                    new SessionManager(context).setDestinationAddress(couponList.get(position).getDestination());
                    new SessionManager(context).setPickupAddress(couponList.get(position).getSource());
                    cab.setProfilePic( couponList.get(position).getDriver_pic() );
                    cab.setRideid( couponList.get(position).getOid() );

                    i.putExtra(Constants.CAB_BY_ID,new Gson().toJson(cab));
                    context.startActivity(i);

                } else if( couponList.get(position).getBooking_status().equals("Pending") ){
                    Intent i = new Intent(context, WaitingScreenIntercity.class);
                    i.putExtra(Constants.CREATED_ON, couponList.get(position).getD_CreatedOn());
                    context.startActivity( i );
                }else {
                    Intent i = new Intent(context, TripHistory.class);
                    i.putExtra(Constants.RIDE_DETAIL, new Gson().toJson(ride));
                    context.startActivity( i );
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class RideHistoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView driverImage;
        private TextView driverName,pickupAddress,dropAddress,rideDate;
        private LinearLayout linearParent;

        RideHistoryViewHolder(View convertView) {
            super(convertView);

            driverName = convertView.findViewById(R.id.driverNameRideHistory);
            pickupAddress =convertView.findViewById(R.id.pickupRideHistory);
            dropAddress =  convertView.findViewById(R.id.dropRideHistory);
            rideDate =  convertView.findViewById(R.id.dateRideHistory);
            driverImage =  convertView.findViewById(R.id.driverPicRideHistory);
            linearParent =  convertView.findViewById(R.id.linearParent);
        }

    }
}

