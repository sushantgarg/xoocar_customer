package com.xoocar.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoocar.R;
import com.xoocar.ShareIntentItems;

import java.util.List;

/**
 * Created by sushant on 8/17/17.
 */

public class AdapterShare extends BaseAdapter {
    private List<ShareIntentItems> items;
    private LayoutInflater mInflater;
    private Context context;

    public AdapterShare(Context context, List<ShareIntentItems> items) {
        this.mInflater = LayoutInflater.from(context);
        this.items = items;
        this.context = context;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_share, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txtView);
            holder.logo = (ImageView) convertView
                    .findViewById(R.id.imageShareView);
            //		holder.loginButton = (LoginButton) convertView
            //			.findViewById(R.id.login_button);
            convertView.setTag(holder);

            convertView.setTag(R.id.txtView, holder.name);
            convertView.setTag(R.id.imageShareView, holder.logo);
            //		convertView.setTag(R.id.login_button, holder.loginButton);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setTag(position);
        holder.logo.setTag(position);
        //	holder.loginButton.setTag(position);

        holder.name.setText(items.get(position).getName());

        holder.logo.setImageDrawable((items.get(position)).getImageName());

        return convertView;
    }

    class ViewHolder {
        TextView name;
        ImageView logo;
        //LoginButton loginButton;
    }
}