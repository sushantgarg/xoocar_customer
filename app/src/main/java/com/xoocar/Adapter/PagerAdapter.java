package com.xoocar.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.xoocar.fragments.BikeFragment;
import com.xoocar.fragments.EasyFragment;
import com.xoocar.fragments.EasyPlusFragment;
import com.xoocar.fragments.EasyPrimeFragment;
import com.xoocar.fragments.EasyRangerFragment;
import com.xoocar.fragments.LiftFragment;

/**
 * Created by sushant on 8/1/17.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    private int mTabCount;

    //Constructor to the class
    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.mTabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return new BikeFragment();
            case 1:
                return new LiftFragment();
            case 2:
                return new EasyFragment();
            case 3:
                return new EasyPlusFragment();
//            case 4:
//                return new EasyPrimeFragment();
            case 4:
                return new EasyRangerFragment();
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return mTabCount;
    }
}
