package com.xoocar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.R;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;

import java.util.List;

/**
 * Created by sushant on 8/8/17.
 */

public class HopNGoAdapter extends RecyclerView.Adapter<HopNGoAdapter.HopNGoViewHolder> {

    private List<CabDetailHopNGo> couponList;
    private AdapterToMain adapterToMain;

    public HopNGoAdapter(List<CabDetailHopNGo> couponList, Context ctx) {
        this.couponList =couponList;
        adapterToMain= (AdapterToMain) ctx;
    }

    @Override
    public HopNGoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_hopngo, parent, false);
        return new HopNGoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HopNGoViewHolder holder, int position) {
       final CabDetailHopNGo cabDetail= couponList.get(holder.getAdapterPosition());
        holder.numberPlate.setText(cabDetail.getVLicenceNo());
        holder.distance.setText(cabDetail.getDistance()+" min");

        String categoryName = RealmManager.getCabCategory(cabDetail.getId()).getCategoryName();

        if(categoryName != null) {

            holder.carModel.setText(cabDetail.getVVehiclename() + " ( " + categoryName + " )");
        }

        holder.bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterToMain.rideDetail(cabDetail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class HopNGoViewHolder extends RecyclerView.ViewHolder {

        private TextView numberPlate,carModel,distance;
        private ImageView cabIcon;
        private Button bookBtn;

        HopNGoViewHolder(View convertView) {
            super(convertView);

            numberPlate =  convertView.findViewById(R.id.numPlate);
            carModel =  convertView.findViewById(R.id.carModel);
            distance =  convertView.findViewById(R.id.distanceHop);
            cabIcon = convertView.findViewById(R.id.iconCategory);
            bookBtn = convertView.findViewById(R.id.bookBtn);
        }

    }
}