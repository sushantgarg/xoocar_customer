package com.xoocar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.R;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;

import java.util.ArrayList;

/**
 * Created by sushant on 8/21/17.
 */

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.FollowerViewHolder> {

    private Context context;
    private ArrayList<ResponseDataArray> couponList;
    private AdapterToMain adapterToMain;

    public OffersAdapter( ArrayList<ResponseDataArray> couponList, Context ctx) {
        this.couponList =couponList;
        adapterToMain= (AdapterToMain) ctx;
        context=ctx;
    }

    @Override
    public OffersAdapter.FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_offers_free_rides, parent, false);
        return new FollowerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, final int position) {
        final ResponseDataArray coupon = couponList.get(holder.getAdapterPosition());
        holder.couponCode.setText("USE COUPON - " +coupon.getCouponName());
        holder.couponDesc.setText(coupon.getDesc());

        holder.couponLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterToMain.addCoupon(coupon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class FollowerViewHolder extends RecyclerView.ViewHolder {

        private TextView couponCode,couponDesc;
        private LinearLayout couponLinear;

        FollowerViewHolder(View convertView) {
            super(convertView);

            couponCode = convertView.findViewById(R.id.couponCodeOffers);
            couponDesc =  convertView.findViewById(R.id.couponDescOffers);
            couponLinear=convertView.findViewById(R.id.couponLinear);
        }

    }
}