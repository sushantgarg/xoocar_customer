package com.xoocar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xoocar.CommonMethods;
import com.xoocar.R;
import com.xoocar.Realm.NotificationsRealm;

import java.util.List;

/**
 * Created by sushant on 8/21/17.
 */

public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.NotiViewHolder> {

    private Context context;
    private List<NotificationsRealm> couponList;

    public NotiAdapter( List<NotificationsRealm> couponList, Context ctx) {
        this.couponList =couponList;
        context=ctx;
    }

    @Override
    public NotiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_notification, parent, false);
        return new NotiViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotiViewHolder holder, final int position) {
        NotificationsRealm coupon = couponList.get(holder.getAdapterPosition());
        holder.message.setText(coupon.getMessage());
        holder.time.setText( CommonMethods.convertDateUnixToStringFormat(coupon.getTime(),"yyyy-MM-dd HH:mm"));
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class NotiViewHolder extends RecyclerView.ViewHolder {

        private TextView message,time;

        NotiViewHolder(View convertView) {
            super(convertView);

            message =  convertView.findViewById(R.id.message);
            time =  convertView.findViewById(R.id.time);
        }

    }
}
