package com.xoocar;

import android.app.DownloadManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.xoocar.Realm.RideHistoryRealm;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailRequestData;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailRequestField;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailResponceData;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailResponceField;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 21/8/17.
 */

public class TripHistory extends AppCompatActivity implements View.OnClickListener,OnMapReadyCallback {
    private TextView pickupAddress_trip_history,dropAddress_trip_history,trip_history_driverName,trip_history_vehicleName,trip_history_driver_total_fare,trip_history_date_time,trip_history_trip_fare,trip_history_taxes,trip_history_coupon_discount,trip_history_total_fare,trip_history_cash;
//    private ImageView start1,start2,start3,start4,start5;
    private SessionManager sessionManager;
    private RideHistoryRealm rideDetail;
    private GoogleMap gMap;
    private RideHistoryDetailResponceData rideDetails;
    private ProgressDialog pd;
    private Polyline navigationPathPoliline;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_history);
        Intent i=getIntent();
        String ride=i.getStringExtra(Constants.RIDE_DETAIL);
        rideDetail=new Gson().fromJson(ride,RideHistoryRealm.class);
        sessionManager=new SessionManager(this);

        pd=new ProgressDialog(this);
        pd.setMessage("Fetching records...");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();

        MapFragment mMapFragment = MapFragment.newInstance();
        mMapFragment.getMapAsync(this);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapFragmentTripHistory, mMapFragment);
        fragmentTransaction.commitAllowingStateLoss();

        pickupAddress_trip_history= findViewById(R.id.pickupAddress_trip_history);
        dropAddress_trip_history=findViewById(R.id.dropAddress_trip_history);
        trip_history_driverName=findViewById(R.id.trip_history_driverName);
        trip_history_vehicleName=findViewById(R.id.trip_history_vehicleName);
        trip_history_driver_total_fare=findViewById(R.id.trip_history_driver_total_fare);
        trip_history_date_time= findViewById(R.id.trip_history_date_time);
        trip_history_trip_fare =findViewById(R.id.trip_history_trip_fare);
        trip_history_taxes=findViewById(R.id.trip_history_taxes);
        trip_history_coupon_discount=findViewById(R.id.trip_history_coupon_discount);
        trip_history_total_fare =findViewById(R.id.trip_history_total_fare);
        trip_history_cash =findViewById(R.id.trip_history_cash);
        LinearLayout trip_history_download_pdf =findViewById(R.id.trip_history_download_pdf);

//        trip_history_download_pdf.setOnClickListener(this);

    }

    private void getRideDetail(String oid) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<RideHistoryDetailResponceField> call = request.rideHistoryDetail(sessionManager.getAuthToken()
                ,"api/newcustomersapi/customerridehistoryRow/format/json/"
                ,new RideHistoryDetailRequestField(new RideHistoryDetailRequestData(oid)));
        call.enqueue(new Callback<RideHistoryDetailResponceField>() {

            @Override
            public void onResponse(Call<RideHistoryDetailResponceField> call
                    , Response<RideHistoryDetailResponceField> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(TripHistory.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        rideDetails=response.body().getDataArray();
                        pickupAddress_trip_history.setText(rideDetails.getSource());
                        dropAddress_trip_history.setText(rideDetails.getDestination());
                        trip_history_driverName.setText(rideDetails.getDname());
                        trip_history_vehicleName.setText(rideDetails.getvVehiclename());
                        trip_history_driver_total_fare.setText(rideDetails.getActualCost());
                        trip_history_date_time.setText(rideDetails.getdCreatedOn());
                        trip_history_taxes.setText(rideDetails.getTaxCharge());
                        trip_history_coupon_discount.setText(rideDetails.getCouponDiscount());
                        trip_history_total_fare.setText(rideDetails.getActualCost());
                        drawNavigationPath(rideDetails.getPoly());

                        if(rideDetails.getCashPaid().equals("")){
                            trip_history_cash.setText("0");
                        }else {
                            trip_history_cash.setText(rideDetails.getCashPaid());
                        }


                        if( rideDetails.getcLatt() != null && ! rideDetails.getcLatt().equals(""))
                        {
                            Drawable mDrawable = getResources().getDrawable(R.drawable.pickup_homescreen_svg);
                            final Bitmap b = CommonMethods.convertDrawableToBitmap(mDrawable);

                            gMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(rideDetails.getcLatt()), Double.parseDouble(rideDetails.getcLong())))
                                    .icon(BitmapDescriptorFactory.fromBitmap(b))
                                    .title("Pickup Location")
                            );



                        }

                        if(rideDetails.getcDestLng() != null  && ! rideDetails.getcDestLng().equals("")) {
                            Drawable mDrawable2 = getResources().getDrawable(R.drawable.dropoff_homescreen_svg);
                            final Bitmap b2 = CommonMethods.convertDrawableToBitmap(mDrawable2);

                            gMap.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromBitmap(b2))
                                    .position(new LatLng(Double.parseDouble(rideDetails.getcDestLat())
                                            , Double.parseDouble(rideDetails.getcDestLng())))
                                    .title("Destination")
                            );
                        }
                    }
                }

                if(pd != null && pd.isShowing()){
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<RideHistoryDetailResponceField> call, Throwable t) {
                Log.d("aaaa","error");
                if(pd != null && pd.isShowing()){
                    pd.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.trip_history_download_pdf:
                if( rideDetails.getWayBill() != null ){
                    String url = "http://velenesa.com/vele/index.php/"+rideDetails.getWayBill();
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    request.setDescription("Bill pdf");
                    request.setTitle("Bill Summary");
// in order for this if to run, you must use the android 3.2 to compile your app
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, rideDetails.getWayBill()+".ext");

// get download service and enqueue file
                    DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                } else {
                    Toast.makeText(this, "Bill not available", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap=googleMap;
        getRideDetail(rideDetail.getOid());
    }

    private void drawNavigationPath(String encodedPath) {
        List<LatLng> points;
        if( navigationPathPoliline != null && navigationPathPoliline.getPoints().size() > 0 ) {
            navigationPathPoliline.remove();
        }
        PolylineOptions lineOptions = null;
        lineOptions = new PolylineOptions();
        points= PolyUtil.decode(encodedPath);

        if( points.size() > 0 ) {
            lineOptions.addAll(points);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            for ( LatLng point :
                    points ) {
                builder.include( point );
            }

            LatLngBounds bounds = builder.build();

            int width = 1250;
            int height = 1920;
            int padding = (int) (width * 0.32); // offset from edges of the map 12% of screen

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            gMap.animateCamera(cu);

            navigationPathPoliline=gMap.addPolyline(lineOptions);
            navigationPathPoliline.setWidth(5f);
        }
// Drawing polyline in the Google Map for the i-th route

    }
}
