package com.xoocar;

import android.graphics.drawable.Drawable;

/**
 * Created by sushant on 8/17/17.
 */

public class ShareIntentItems {
    String Name;
    Drawable imageId;
    String packageName;
    String className;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Drawable getImageName() {
        return imageId;
    }

    public void setImageName(Drawable drawable) {
        this.imageId = drawable;
    }
}
