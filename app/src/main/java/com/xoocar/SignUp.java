package com.xoocar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;
import com.xoocar.Requests.Login.LoginResponceFields;
import com.xoocar.Requests.SignUp.SignUpRequestData;
import com.xoocar.Requests.SignUp.SignUpRequestFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/8/17.
 */

public class SignUp extends AppCompatActivity  {
    private EditText name,emaiId,adharNumber;
    private String mobNum;
    private SessionManager sessionManager;
    ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = findViewById(R.id.name);
        emaiId = findViewById(R.id.emailId);
        TextView mobileNumber = findViewById(R.id.mobileNumber);
        Button signUpbutton = findViewById(R.id.signUpButton);
        adharNumber = findViewById(R.id.adharNumber);

        Intent i=getIntent();
        sessionManager=new SessionManager(this);

        mobNum=i.getStringExtra(Constants.MOB_NUM);
        mobileNumber.setText(mobNum);

        signUpbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(CommonMethods.validateName(name.getText().toString())) {
                    if(emaiId.getText().toString().equals("")) {
                        if(adharNumber.getText().toString().equals("")) {
                            signUp();
                        } else {
                            if( CommonMethods.isValidAdhar(adharNumber.getText().toString()) ) {
                                signUp();
                            } else {
                                Toast.makeText(SignUp.this, "Enter valid aadhar number", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if( CommonMethods.isValidEmail(emaiId.getText().toString()) ) {
                            if(adharNumber.getText().toString().equals("")) {
                                signUp();
                            } else {
                                if( CommonMethods.isValidAdhar(adharNumber.getText().toString()) ) {
                                    signUp();
                                } else {
                                    Toast.makeText(SignUp.this, "Enter valid aadhar number", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(SignUp.this, "Enter valid email id", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(SignUp.this, "Enter valid name", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void signUp() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        pd = new ProgressDialog(SignUp.this);
        pd.setMessage("Verifying details...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<LoginResponceFields> call = request.signUp("api/newloginapi/customer_registration/format/json/"
                ,new SignUpRequestFields(new SignUpRequestData(mobNum
                        ,emaiId.getText().toString()
                        ,name.getText().toString()
                        ,"Android"
                        ,adharNumber.getText().toString()
                        ,sessionManager.getFcmToken())));

        call.enqueue(new Callback<LoginResponceFields>() {
            @Override
            public void onResponse(Call<LoginResponceFields> call, Response<LoginResponceFields> response) {

                if( response.body() != null  ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( SignUp.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            sessionManager.saveUserId(response.body().getDataArray().getUid());
                            sessionManager.saveAuthToken(response.body().getDataArray().getAuthtoken());
                            sessionManager.setIsLogedIn(true);
                            sessionManager.saveUserNAme(name.getText().toString());
                            sessionManager.saveUserNumber(mobNum);
                            sessionManager.saveWalletBalance("0");

                            Answers.getInstance().logSignUp(new SignUpEvent()
                                    .putMethod(mobNum)
                                    .putSuccess(true));

                            Intent i=new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(i);
                        }
                    } else {
                        Toast.makeText(SignUp.this, "Some error occured please try again", Toast.LENGTH_SHORT).show();
                    }
                } if( pd!= null && pd.isShowing() ){
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginResponceFields> call, Throwable t) {
                    if(pd!= null && pd.isShowing()){
                        pd.dismiss();
                }
            }
        });
    }


}
