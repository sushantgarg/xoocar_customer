package com.xoocar.Realm;

import com.xoocar.Requests.RideHistory.RideHistoryResponceData;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sushant on 8/21/17.
 */

public class RideHistoryRealm extends RealmObject {
    private String dname;
    private String source;
    private String destination;
    private String pickup_time;
    private String driver_pic;
    private String booking_status;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @PrimaryKey
    private String oid;

    public String getD_CreatedOn() {
        return d_CreatedOn;
    }

    public void setD_CreatedOn(String d_CreatedOn) {
        this.d_CreatedOn = d_CreatedOn;
    }

    private String d_CreatedOn;

    public RideHistoryRealm(RideHistoryResponceData ride) {
        this.dname = ride.getDname();
        this.source = ride.getSource();
        this.destination = ride.getDestination();
        this.pickup_time = ride.getPickupTime();
        this.driver_pic = ride.getDriverPic();
        this.booking_status = ride.getBookingStatus();
        this.crn_no = ride.getCrnNo();
        this.d_CreatedOn=ride.getdCreatedOn();
        this.oid=ride.getOid();
    }

    public RideHistoryRealm() {
    }

    public String getCrn_no() {

        return crn_no;
    }

    public void setCrn_no(String crn_no) {
        this.crn_no = crn_no;
    }

    private String crn_no;

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getDriver_pic() {
        return driver_pic;
    }

    public void setDriver_pic(String driver_pic) {
        this.driver_pic = driver_pic;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }
}
