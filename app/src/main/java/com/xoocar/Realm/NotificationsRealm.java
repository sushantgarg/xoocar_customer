package com.xoocar.Realm;

import io.realm.RealmObject;

/**
 * Created by sushant on 8/21/17.
 */

public class
NotificationsRealm extends RealmObject {
    private String message;
    private long time;

    NotificationsRealm(String message, long time) {
        this.message = message;
        this.time = time;
    }

    public NotificationsRealm() {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
