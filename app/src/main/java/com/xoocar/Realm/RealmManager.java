package com.xoocar.Realm;

import android.content.Context;

import com.xoocar.CommonMethods;
import com.xoocar.Requests.RideHistory.RideHistoryResponceData;
import com.xoocar.models.CabPricing;

import java.util.List;

import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by sushant on 8/6/17.
 */

public class RealmManager {
//    public static void addCabs(final List<Driver> driversData) {
//        try(Realm realm=Realm.getDefaultInstance()) {
//            realm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//                    for (Driver cab : driversData) {
//                        CabsRealm cabRealm=new CabsRealm(cab);
//                        realm.insertOrUpdate(cabRealm);
//                    }
//                }
//            });
//        }
//    }

    public static List<CabsRealm> getAllCabs() {
        try(Realm realm=Realm.getDefaultInstance()) {
            return realm.copyFromRealm(realm.where(CabsRealm.class).findAll().sort("durationValue",Sort.ASCENDING));
        }

    }

    public static List<CabsRealm> getCabByCategory(String type) {
        try(Realm realm=Realm.getDefaultInstance()) {
            return realm.copyFromRealm(realm.where(CabsRealm.class).equalTo("cabType",type).findAll().sort("durationValue", Sort.ASCENDING));
        }
    }

//    public static void addCategoryDetails(final List<CategoryPrice> categoryPrice) {
//        try(Realm realm=Realm.getDefaultInstance()) {
//            realm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//                    for (CategoryPrice cat:
//                         categoryPrice) {
//                        CategoryPriceRealm category=new CategoryPriceRealm(cat);
//                        realm.insertOrUpdate(category);
//                    }
//                }
//            });
//        }
//    }

    public static CabPricing getCabCategory(String s) {
        CabPricing categoryPrice = null;
        try(Realm realm=Realm.getDefaultInstance()) {
            if( realm.where(PriceCategoryRealm.class)
                    .equalTo("categoryId",Integer.parseInt(s))
                    .findFirst() != null ) {
                categoryPrice = new CabPricing(realm.copyFromRealm(realm.where(PriceCategoryRealm.class)
                        .equalTo("categoryId", Integer.parseInt(s))
                        .findFirst()));
            }
        }
        return categoryPrice;
    }

    public static void deleteCabs() {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(CabsRealm.class);
                }
            });
        }
    }

    public static void logout() {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.deleteAll();
                }
            });
        }
    }

    public static List<RideHistoryRealm> getRideHistoryByCategory(final String status) {
        try(Realm realm=Realm.getDefaultInstance()) {
        return realm.copyFromRealm(realm.where(RideHistoryRealm.class).equalTo("booking_status",status).findAll());
        }
    }

    public static void addRideHistory(final List<RideHistoryResponceData> rideLIst) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (RideHistoryResponceData ride:rideLIst) {
                        RideHistoryRealm rideRealm=new RideHistoryRealm(ride);
                        realm.insertOrUpdate(rideRealm);
                    }
                }
            });
        }
    }

    public static void addNotification(final String message) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    NotificationsRealm notificationsRealm=new NotificationsRealm(message,System.currentTimeMillis());
                    realm.insertOrUpdate(notificationsRealm);
                }
            });
        }
    }

    public static List<NotificationsRealm> getNotifications() {
        try(Realm realm=Realm.getDefaultInstance()) {
            return realm.copyFromRealm(realm.where(NotificationsRealm.class).findAll().sort("time",Sort.DESCENDING));
        }
    }

    public static void addCab(final CabsRealm cabRealm, final Context ctx) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                        cabRealm.setDurationValue(CommonMethods.getArrivalTime(cabRealm.getdLatt(),cabRealm.getdLong(),ctx));
                        realm.insertOrUpdate(cabRealm);

                }
            });
        }
    }

    public static void addCategory(final CabPricing price, String key) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                        PriceCategoryRealm category=new PriceCategoryRealm(price);
                        realm.insertOrUpdate(category);

                }
            });
        }
    }
}
