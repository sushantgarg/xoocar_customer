package com.xoocar.Realm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.GetCabs.Driver;
import com.xoocar.models.CabPojo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sushant on 8/6/17.
 */

public class CabsRealm extends RealmObject {
    /*
    * we are now treating origin addresses as vehicle number
    *and destinationaddress as cab model
    *  */

    public CabsRealm() {
    }

    public CabsRealm(CabPojo cab) {
        this.cabType=cab.getCategory();
        this.firstName=cab.getDriverName();
        this.dLatt= String.valueOf(cab.getLocation().getLatitude());
        this.dLong= String.valueOf(cab.getLocation().getLongitude());
        this.uid=cab.getDriverId();
        this.course= String.valueOf(cab.getLocation().getCourse());
        this.originAddresses=cab.getVehicleNumber();
        this.destinationAddresses=cab.getModel();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getcCapacity() {
        return cCapacity;
    }

    public void setcCapacity(String cCapacity) {
        this.cCapacity = cCapacity;
    }

    public String getCabType() {
        return cabType;
    }

    public void setCabType(String cabType) {
        this.cabType = cabType;
    }

    public String getdLatt() {
        return dLatt;
    }

    public void setdLatt(String dLatt) {
        this.dLatt = dLatt;
    }

    public String getdLong() {
        return dLong;
    }

    public void setdLong(String dLong) {
        this.dLong = dLong;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOriginAddresses() {
        return originAddresses;
    }

    public void setOriginAddresses(String originAddresses) {
        this.originAddresses = originAddresses;
    }

    public String getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(String destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getDistanceText() {
        return distanceText;
    }

    public void setDistanceText(String distanceText) {
        this.distanceText = distanceText;
    }

    public Integer getDistanceValue() {
        return distanceValue;
    }

    public void setDistanceValue(Integer distanceValue) {
        this.distanceValue = distanceValue;
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public Integer getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(Integer durationValue) {
        this.durationValue = durationValue;
    }

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @PrimaryKey
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("c_capacity")
    @Expose
    private String cCapacity;
    @SerializedName("cab_type")
    @Expose
    private String cabType;
    @SerializedName("d_latt")
    @Expose
    private String dLatt;
    @SerializedName("d_long")
    @Expose
    private String dLong;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("origin_addresses")
    @Expose
    private String originAddresses;
    @SerializedName("destination_addresses")
    @Expose
    private String destinationAddresses;
    @SerializedName("distance_text")
    @Expose
    private String distanceText;
    @SerializedName("distance_value")
    @Expose
    private Integer distanceValue;
    @SerializedName("duration_text")
    @Expose
    private String durationText;
    @SerializedName("duration_value")
    @Expose
    private Integer durationValue;
    private String course;

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
