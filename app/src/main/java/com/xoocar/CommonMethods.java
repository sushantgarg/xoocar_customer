package com.xoocar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.xoocar.Adapter.AdapterShare;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.SessionManager.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sushant on 8/7/17.
 */

public class CommonMethods {

    static Bitmap convertDrawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    static void animateMarkerToNewLoaction(final Marker marker, double lat, double lng) {
        LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
        MarkerAnimations.animateMarkerToICS(marker,new LatLng( lat,lng ),latLngInterpolator);
    }

    static void showShareIntentDialog(final String text, final Context mContext) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.shareintents_layout);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
        sendIntent.setType("image/*");

        ListView lv = (ListView) dialog.findViewById(R.id.lstShareIntent);

        NamesParserShareIntent namesParserShareIntent = new NamesParserShareIntent();
        final List<ShareIntentItems> intentItems = namesParserShareIntent.getData((Activity) mContext);
        final AdapterShare customShareAdapter = new AdapterShare(mContext, intentItems);
        lv.setAdapter(customShareAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                Intent targetedShare = new Intent(
                        android.content.Intent.ACTION_SEND);
                targetedShare.setClassName(intentItems.get(arg2)
                        .getPackageName(), intentItems.get(arg2)
                        .getClassName());
                targetedShare.setType("text/plain");

                targetedShare.putExtra(Intent.EXTRA_SUBJECT, "LOGO");
                targetedShare.putExtra(Intent.EXTRA_TITLE, "LOGO");
                targetedShare.putExtra(android.content.Intent.EXTRA_TEXT, text);

                mContext.startActivity(targetedShare);

                if (dialog.isShowing())
                    dialog.dismiss();

            }
        });

        dialog.show();
    }

    static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    static boolean isValidPhone(String phone) {
        boolean check = false;
        if( phone.contains("+") ){
            phone=phone.replace("+","");
        }
        if( phone.contains("-") ){
            phone=phone.replace("-","");
        }

        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 10 || phone.length() > 10) {
                check = false;
            } else {
                long num=Long.parseLong(phone);
                if( num < 6999999999L) {
                    check = false;
                } else {
                    check = true;
                }
//                check = true;

            }
        } else {
            check = false;
        }
        return check;
    }

    private static final String USERNAME_PATTERN = "^[a-zA-Z\\s]*$";

    static boolean validateName(String password) {
        if( password.length() < 3 || password.length() > 29 ){
            return false;
        }
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(USERNAME_PATTERN);

        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    static boolean isValidAdhar(String password) {

        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^\\d{12}?$");

        matcher = pattern.matcher(password);
        return matcher.matches();
    }


    public static String convertDateUnixToStringFormat(long timestamp,String format) {

        String timeStamp = String.valueOf(timestamp);
        String upToNCharacters = String.valueOf(timestamp);

        if ( timeStamp.length() > 10 )
            upToNCharacters = timeStamp.substring(0, 10);

        long unixSeconds = Long.parseLong(upToNCharacters);

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date(unixSeconds * 1000L);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }


    public static String getEta(CabsRealm cabsRealm ,Context ctx) {
        String eta="";
        SessionManager sessionManager=new SessionManager(ctx);
        Location loc=new Location("gps");
        loc.setLatitude(Double.parseDouble(sessionManager.getPickupMarkerLat()));
        loc.setLongitude(Double.parseDouble(sessionManager.getPickupMarkerLng()));

        Location cabLoc=new Location("gps");
        cabLoc.setLatitude(Double.parseDouble(cabsRealm.getdLatt()));
        cabLoc.setLongitude(Double.parseDouble(cabsRealm.getdLong()));

        eta= String.valueOf(loc.distanceTo(cabLoc)/100);
        eta=eta.substring(0,1);

        return  eta+" min";
    }

    public static Integer getArrivalTime(String lat, String lng, Context ctx) {
        String eta;
        SessionManager sessionManager=new SessionManager(ctx);
        Location loc=new Location("gps");
        loc.setLatitude(Double.parseDouble(sessionManager.getPickupMarkerLat()));
        loc.setLongitude(Double.parseDouble(sessionManager.getPickupMarkerLng()));

        Location cabLoc=new Location("gps");
        cabLoc.setLatitude(Double.parseDouble(lat));
        cabLoc.setLongitude(Double.parseDouble(lng));

//        eta= String.valueOf((loc.distanceTo(cabLoc)/1000) * 4);

        return (int) ((loc.distanceTo(cabLoc)/1000) * 5);
    }

    public static String convertDateUnixToTime(long timestamp) {

        String timeStamp = String.valueOf(timestamp);
        String upToNCharacters = String.valueOf(timestamp);

        if ( timeStamp.length() > 10 )
            upToNCharacters = timeStamp.substring(0, 10);

        long unixSeconds = Long.parseLong(upToNCharacters);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date(unixSeconds * 1000L);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }

    static Bitmap getCabIcon(String cabType, Context ctx) {

        Bitmap b = null;
        Drawable mDrawable;

        switch (cabType) {
            case "13":
                 mDrawable = ctx.getResources().getDrawable(R.drawable.bike);
                b = CommonMethods.convertDrawableToBitmap(mDrawable);
                break;

            case "1":
                 mDrawable = ctx.getResources().getDrawable(R.drawable.cab_icon);
                b = CommonMethods.convertDrawableToBitmap(mDrawable);
                break;

            case "2":
                 mDrawable = ctx.getResources().getDrawable(R.drawable.cab_icon);
                b = CommonMethods.convertDrawableToBitmap(mDrawable);
                break;
            case "3":
                 mDrawable = ctx.getResources().getDrawable(R.drawable.sedan_icon_map);
                b = CommonMethods.convertDrawableToBitmap(mDrawable);
                break;
            case "6":
                 mDrawable = ctx.getResources().getDrawable(R.drawable.suv_icon_map);
                b = CommonMethods.convertDrawableToBitmap(mDrawable);
                break;
        }

        return b;
    }
}
