package com.xoocar.EventBusModels;

/**
 * Created by sushant on 8/6/17.
 */

public class Message {
    String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
