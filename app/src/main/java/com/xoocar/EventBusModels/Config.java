package com.xoocar.EventBusModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/28/17.
 */

public class Config {

    @SerializedName("androidBannerUrl")
    @Expose
    private String androidBannerUrl;
    @SerializedName("androidBannerId")
    @Expose
    private int androidBannerId;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    private float taxRate;
    private int cancellation_charge;

    public int getAndroidBannerId() {
        return androidBannerId;
    }

    public void setAndroidBannerId(int androidBannerId) {
        this.androidBannerId = androidBannerId;
    }

    public int getCancellation_charge() {
        return cancellation_charge;
    }

    public void setCancellation_charge(int cancellation_charge) {
        this.cancellation_charge = cancellation_charge;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(float taxRate) {
        this.taxRate = taxRate;
    }

    public String getAndroidBannerUrl() {
        return androidBannerUrl;
    }

    public void setAndroidBannerUrl(String androidBannerUrl) {
        this.androidBannerUrl = androidBannerUrl;
    }

    public int getBannerId() {
        return androidBannerId;
    }

    public void setBannerId(int bannerActive) {
        androidBannerId = bannerActive;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }
}
