package com.xoocar.EventBusModels;

/**
 * Created by sushant on 8/11/17.
 */

public class
Notifications {
    String type,driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Notifications(String type, String driverId, String rideId) {
        this.type = type;
        this.rideId = rideId;
        this.driverId=driverId;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    String rideId;
}
