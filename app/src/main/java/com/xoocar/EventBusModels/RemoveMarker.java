package com.xoocar.EventBusModels;

/**
 * Created by sushant on 10/3/17.
 */

public class RemoveMarker {
    private String driverId;

    public RemoveMarker(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverId() {

        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
