package com.xoocar.EventBusModels;

/**
 * Created by sushant on 8/6/17.
 */

public class CouponDetail {
    private String name;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    private String source;

    public CouponDetail(String name, String value, String stringExtra,String source) {
        this.name=name;
        this.value=value;
        this.id=stringExtra;
        this.source=source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
