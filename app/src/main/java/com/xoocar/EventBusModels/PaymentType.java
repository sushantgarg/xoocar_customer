package com.xoocar.EventBusModels;

/**
 * Created by sushant on 12/8/17.
 */

public class PaymentType {
    String paymentType;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentType(String paymentType) {
        this.paymentType=paymentType;
    }

}
