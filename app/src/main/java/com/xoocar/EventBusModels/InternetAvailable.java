package com.xoocar.EventBusModels;

/**
 * Created by sushant on 8/7/17.
 */

public class InternetAvailable {
    private boolean isInternetAvailable;

    public InternetAvailable(boolean isInternetAvailable) {
        this.isInternetAvailable = isInternetAvailable;
    }

    public boolean isInternetAvailable() {

        return isInternetAvailable;
    }

    public void setInternetAvailable(boolean internetAvailable) {
        isInternetAvailable = internetAvailable;
    }
}
