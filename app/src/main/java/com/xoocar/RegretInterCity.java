package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sushant on 10/13/17.
 */

 public class RegretInterCity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regret);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(RegretInterCity.this,MainActivity.class);
        startActivity(i);
    }
}
