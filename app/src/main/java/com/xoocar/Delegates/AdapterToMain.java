package com.xoocar.Delegates;

import com.xoocar.Realm.CabsRealm;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;

/**
 * Created by sushant on 8/5/17.
 */

public interface AdapterToMain {
    public void addCoupon(ResponseDataArray coupon);
    public void rideDetail(CabDetailHopNGo cab);
}
