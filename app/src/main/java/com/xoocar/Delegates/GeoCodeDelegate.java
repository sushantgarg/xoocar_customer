package com.xoocar.Delegates;

/**
 * Created by sushant on 8/30/17.
 */

public interface GeoCodeDelegate {
    void processFinish(String encodedPath, String shortName);
}
