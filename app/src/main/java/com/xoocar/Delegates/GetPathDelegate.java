package com.xoocar.Delegates;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by sushant on 31/7/17.
 */

public interface GetPathDelegate {
        void processFinish(List<LatLng> encodedPath);

}
