package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xoocar.EventBusModels.Notifications;
import com.xoocar.Requests.ConfirmBooking.CabDetailsResponce;
import com.xoocar.Requests.ConfirmBooking.FieldsGetBooking;
import com.xoocar.Requests.ConfirmBooking.RequestBookingRequest;
import com.xoocar.Requests.ConfirmBooking.ResponceBooking;
import com.xoocar.Requests.GetRideById.FieldsRequestGetCabById;
import com.xoocar.Requests.GetRideById.RequestFieldsGetCabById;
import com.xoocar.Requests.GetRideById.RequestModelGetCabById;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/10/17.
 */

public class WaitingScreenRideNow extends AppCompatActivity {
    private SessionManager sessionManager;
    private CountDownTimer cdt;
    private int count = 0;
    private String couponId, paymentMode, vehicleType;
    private List<CabDetailsResponce> drivers;
    private LinearLayout noCabsScreen, animationLayout;
    private boolean isAlive;
    private String couponStatus = "1";
    private String mobile, name, rideEstimate, eta, rideId;
    private DilatingDotsProgressBar mDilatingDotsProgressBar;
    private String driverIds;
    private CountDownTimer timercontDown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_ridenow);

        if( !EventBus.getDefault().isRegistered(this))
        EventBus.getDefault().register(WaitingScreenRideNow.this);

        Intent i = getIntent();

        sessionManager = new SessionManager(WaitingScreenRideNow.this);

        mDilatingDotsProgressBar = findViewById(R.id.progress);
        mDilatingDotsProgressBar.showNow();

        couponId = i.getStringExtra(Constants.COUPON_ID);
        paymentMode = i.getStringExtra(Constants.PAYMENT_MODE);
        vehicleType = i.getStringExtra(Constants.VEHICLE_TYPE);
        rideEstimate = i.getStringExtra(Constants.RIDE_ESTIMATE);
        eta = i.getStringExtra(Constants.ETA);
        driverIds=i.getStringExtra(Constants.DRIVER_IDS);

        if (couponId == null) {
            couponStatus = "0";
        }

        noCabsScreen = findViewById(R.id.noCabsImage);
        animationLayout = findViewById(R.id.animationLayout);
        mobile = sessionManager.getFrndNumber();
        name = sessionManager.getFrndName();

        requestBooking();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAlive = true;
    }

    private void requestBooking() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ResponceBooking> call = request.requestBooking( sessionManager.getAuthToken(), "api/firebaseapi/bookingconfirm/format/json/"
                , new FieldsGetBooking(new RequestBookingRequest(sessionManager.getPickupMarkerLat()
                        , sessionManager.getPickupMarkerLng()
                        , vehicleType
                        , sessionManager.getUserId()
                        , sessionManager.getPickupAddress()
                        , sessionManager.getDSestinationAddress()
                        , rideEstimate
                        , eta
                        , paymentMode.toLowerCase()
                        , couponId
                        , couponStatus
                        , mobile
                        , name
                        , ""
                        , sessionManager.getCityId()
                        , sessionManager.getDestLat()
                        , sessionManager.getDestLng()
                        , driverIds
                        ,"1"
                )));

        call.enqueue(new Callback<ResponceBooking>() {
            @Override
            public void onResponse(Call<ResponceBooking> call, Response<ResponceBooking> response) {
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(WaitingScreenRideNow.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body() != null) {
                            if(response.body().getDataArray().getIsvalid().equals("1")) {
                                rideId = response.body().getDataArray().getRideid();
                                startTimer();
                            } else {

                                mDilatingDotsProgressBar.hideNow();
                                animationLayout.setVisibility(View.GONE);

                                sessionManager.setRideId("0");
                                sessionManager.setFrndName("");
                                sessionManager.setFrndNumber("");

                                Toast.makeText(WaitingScreenRideNow.this, "Bike ride not available for this destination", Toast.LENGTH_LONG).show();
                                Intent i=new Intent(WaitingScreenRideNow.this,MainActivity.class);
                                startActivity(i);

                            }
                        } else {
                            showNoRespoceScreen();
                        }
                    } else {
                        showNoRespoceScreen();
                    }
                } else {
                    showNoRespoceScreen();
                }
            }

            @Override
            public void onFailure(Call<ResponceBooking> call, Throwable t) {
                Log.d("aaaa", "error");
                showNoRespoceScreen();
            }
        });
    }

    public void startTimer() {
        cdt = new CountDownTimer(45 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("aaaa", "" + millisUntilFinished);
            }

            public void onFinish() {
                getRideById(rideId);
            }
        };
        cdt.start();
    }

    private void showNoRespoceScreen() {
        if (cdt != null) {
            cdt.cancel();
            cdt = null;
        }
        mDilatingDotsProgressBar.hideNow();
        animationLayout.setVisibility(View.GONE);

        sessionManager.setRideId("0");
        sessionManager.setFrndName("");
        sessionManager.setFrndNumber("");
        noCabsScreen.setVisibility(View.VISIBLE);

        startTimerForExit();
    }

    private void startTimerForExit() {
        try {
            timercontDown = new CountDownTimer(5*1000, 1000) {

                public void onTick( long millisUntilFinished ) {
                }
                public void onFinish() {
                    Intent i=new Intent(WaitingScreenRideNow.this,MainActivity.class);
                    startActivity(i);
                }
            }.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void getRideById(final String oid) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<RequestFieldsGetCabById> call = request.requestCabById(sessionManager.getAuthToken()
                , "api/processapi/get_ur_driverinfo/format/json/"
                , new RequestModelGetCabById(new FieldsRequestGetCabById(oid)));

        call.enqueue(new Callback<RequestFieldsGetCabById>() {
            @Override
            public void onResponse(Call<RequestFieldsGetCabById> call, Response<RequestFieldsGetCabById> response) {

                if ( response.body() != null && isAlive ) {
                    if ( response.body().getResponseCode() == 410 ) {
                        Toast.makeText(WaitingScreenRideNow.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if ( response.body().getResponseCode() == 400 ) {
                        if ( response.body() != null ) {
                            com.xoocar.Requests.GetRideById.RequestData cab = response.body().getDataArray().get(0);
                            Intent i = new Intent(WaitingScreenRideNow.this, RideScheduled.class);
                            i.putExtra(Constants.CAB_BY_ID, new Gson().toJson(cab));
                            startActivity(i);
                            finish();
                        } else {
                            showNoRespoceScreen();
                        }
                    } else {
                        showNoRespoceScreen();
                    }
                } else {
                    showNoRespoceScreen();
                }
            }

            @Override
            public void onFailure(Call<RequestFieldsGetCabById> call, Throwable t) {
                showNoRespoceScreen();
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Notifications msg) {
        if (msg.getType().equals(Constants.ACCEPTED_BY_DRIVER)) {
            if (cdt != null) {
                cdt.cancel();
                cdt = null;
            }
            getRideById(msg.getRideId());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this))
        EventBus.getDefault().unregister(this);
        if (cdt != null) {
            cdt = null;
        }

        isAlive = false;
    }

    @Override
    public void onBackPressed() {
        if( timercontDown != null )
        timercontDown.cancel();

        if (cdt == null) {
            finish();
        } else {
            Toast.makeText(this, "Please wait booking your ride", Toast.LENGTH_SHORT).show();
        }
    }

}
