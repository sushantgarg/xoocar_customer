package com.xoocar.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.xoocar.Delegates.GetPathDelegate;
import com.xoocar.EventBusModels.Message;
import com.xoocar.MainActivity;
import com.xoocar.SessionManager.SessionManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sushant on 9/13/17.
 */

public class GetPathDistance extends AsyncTask<String, Void, String> {
    private LatLng startPoint,endPoint;
    private Context ctx;
    public GetPathDistance asyncObject;

    public GetPathDistance( LatLng startPoint,Context context,LatLng endPoint ) {
        this.startPoint=startPoint;
        this.endPoint=endPoint;
        ctx=context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        asyncObject=this;

        new CountDownTimer(7000, 1000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }
            public void onFinish() {
                // stop async task if not in progress
                if (asyncObject.getStatus() == AsyncTask.Status.RUNNING) {
//                    asyncObject.cancel(false);
                    // Add any specific task you wish to do as your extended class variable works here as well.
                }
            }
        }.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {
        String data = "";
        SessionManager sessionManager = new SessionManager(ctx);

        try {
            // Fetching the data from web service
            data = downloadUrl( getUrl( endPoint , startPoint ) );
        } catch (Exception ignored) {
        }

        JSONObject jObject;
        String retValue="";

        try {
            jObject = new JSONObject(data);

            String distancMeters = ((jObject.optJSONArray("rows")).getJSONObject(0)).optJSONArray("elements").getJSONObject(0).optJSONObject("distance").optString("value");
            String durationSeconds = ((jObject.optJSONArray("rows")).getJSONObject(0)).optJSONArray("elements").getJSONObject(0).getJSONObject("duration").optString("value");

            int dis = (int) (.10 * Integer.parseInt(distancMeters));
            int time= (int) (.10 * Integer.parseInt(durationSeconds));

            sessionManager.setDestinationDistanceMeters(Integer.parseInt(distancMeters) + dis);
            sessionManager.setDestinationDurationSeconds(Integer.parseInt(durationSeconds) + time);
            EventBus.getDefault().post(new Message("RefreshCabs"));


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return retValue;
        }
//        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

    private String getUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = dest.latitude+","+dest.longitude;

//        http://maps.googleapis.com/maps/api/distancematrix/json?origins=28.7321,77.55487&destinations=28.14525,77.570993&mode=driving&language=en-EN&sensor=true

        return "http://maps.googleapis.com/maps/api/distancematrix/json?origins="+str_origin+"&destinations="
                +str_dest+"&mode=driving&language=en-EN&sensor=true";
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        HttpURLConnection urlConnection = null;
        URL url = new URL(strUrl);

        // Creating an http connection to communicate with url
        urlConnection = (HttpURLConnection) url.openConnection();

        // Connecting to url
        urlConnection.connect();

        // Reading data from url
        try (InputStream iStream = urlConnection.getInputStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } finally {
            urlConnection.disconnect();
        }
        return data;
    }
}
