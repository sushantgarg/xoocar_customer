package com.xoocar.AsyncTasks;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.xoocar.Delegates.GeoCodeDelegate;
import com.xoocar.Delegates.GetPathDelegate;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sushant on 8/30/17.
 */

public class GetGeoCodeAsync extends AsyncTask<String, Void, String> {
    private GeoCodeDelegate delegate = null;
    private ArrayList<LatLng> locations;
    private LatLng startPoint;
    private String shortName="";

    public GetGeoCodeAsync(LatLng startPoint, GeoCodeDelegate asyncDelegateInterface) {
        delegate=asyncDelegateInterface;
        this.startPoint=startPoint;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {
        String data = "";

        try {
            // Fetching the data from web service
            data = downloadUrl(getUrl(startPoint));
        } catch (Exception ignored) {
            Log.d("aaaa","fjhkjf");
        }

        JSONObject jObject;
//        String encodedPath="";
        String retValue="";

        try {
            jObject = new JSONObject(data);
            if( ((JSONObject)(jObject.optJSONArray("results").get(0))).optJSONArray("address_components").get(7) != null ) {
                shortName = ((JSONObject) ((JSONObject) (jObject.optJSONArray("results").get(0))).optJSONArray("address_components").get(7)).getString("short_name");
            }
            retValue= ((JSONObject)(jObject.optJSONArray("results").get(0))).optString("formatted_address");
        } catch (Exception e) {
            e.printStackTrace();
            retValue="No address found";
        } finally {
            return retValue;
        }
//        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result,shortName);

        super.onPostExecute(result);
    }

    private String getUrl(LatLng origin) {
        // Origin of route
//        String str_origin = "origin="+origin.latitude+","+origin.longitude;
//
//        // Sensor enabled
//        String sensor = "sensor=false";
//
//        // Building the parameters to the web service
//        String parameters = str_origin+"&"+str_dest+"&"+sensor;
//
//        // Output format
        String output = origin.latitude+","+origin.longitude;

        // Building the url to the web service
//        http://maps.google.com/maps/api/geocode/json?lat/lng: (28.6244537,77.3872307)&sensor=true
        return "http://maps.google.com/maps/api/geocode/json?latlng="+output+"&sensor=true";
//        http://maps.google.com/maps/api/geocode/json?latlng=28.610607,77.387665&sensor=true
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        HttpURLConnection urlConnection = null;
        URL url = new URL(strUrl);

        // Creating an http connection to communicate with url
        urlConnection = (HttpURLConnection) url.openConnection();

        // Connecting to url
        urlConnection.connect();

        // Reading data from url
        try (InputStream iStream = urlConnection.getInputStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } finally {
            urlConnection.disconnect();
        }
        return data;
    }
}
