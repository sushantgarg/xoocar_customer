package com.xoocar.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.xoocar.Delegates.GetPathDelegate;
import com.xoocar.SessionManager.SessionManager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sushant on 31/7/17.
 */

public class GetpathAsync extends AsyncTask<String, Void, String> {
    private GetPathDelegate delegate = null;
    private LatLng startPoint,endPoint;
    private Context ctx;
    private List<LatLng> points=new ArrayList<>();
    private GetpathAsync asyncObject;

    public GetpathAsync(LatLng startPoint,Context context,LatLng endPoint, GetPathDelegate asyncDelegateInterface) {
        delegate=asyncDelegateInterface;
        this.startPoint=endPoint;
        this.endPoint=startPoint;
        ctx=context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        asyncObject=this;

        new CountDownTimer(7000, 1000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }
            public void onFinish() {
                // stop async task if not in progress
                if (asyncObject.getStatus() == AsyncTask.Status.RUNNING) {
                    delegate.processFinish(points);
                    asyncObject.cancel(false);
                    // Add any specific task you wish to do as your extended class variable works here as well.
                }
            }
        }.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {
        String data = "";
        SessionManager sessionManager = new SessionManager(ctx);

        try {
            // Fetching the data from web service
            data = downloadUrl(getUrl(startPoint,endPoint));
        } catch (Exception ignored) {
        }

        JSONObject jObject;
        String retValue="";

        try {
            jObject = new JSONObject(data);
            // Starts parsing data
            JSONObject jObj= (JSONObject) jObject.getJSONArray("routes").get(0);

           String encodedPath = jObj.optJSONObject("overview_polyline").optString("points");

            String distancMeters = ((JSONObject)(jObj.getJSONArray("legs").get(0))).getJSONObject("distance").optString("value");
            String durationSeconds = ((JSONObject)((jObj).getJSONArray("legs").get(0))).getJSONObject("duration").optString("value");

            sessionManager.setDestinationDistanceMeters(Integer.parseInt(distancMeters));
            sessionManager.setDestinationDurationSeconds(Integer.parseInt(durationSeconds));

            points= PolyUtil.decode(encodedPath);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return retValue;
        }
//        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(points);
        super.onPostExecute(result);
    }

    private String getUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service

        return "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        HttpURLConnection urlConnection = null;
        URL url = new URL(strUrl);

        // Creating an http connection to communicate with url
        urlConnection = (HttpURLConnection) url.openConnection();

        // Connecting to url
        urlConnection.connect();

        // Reading data from url
        try (InputStream iStream = urlConnection.getInputStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } finally {
            urlConnection.disconnect();
        }
        return data;
    }
}
