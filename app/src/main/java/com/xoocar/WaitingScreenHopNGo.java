package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xoocar.EventBusModels.Notifications;
import com.xoocar.Requests.CancelRide.CancelRideRequestData;
import com.xoocar.Requests.CancelRide.CancelRideRequestFields;
import com.xoocar.Requests.CancelRide.CancelRideResponce;
import com.xoocar.Requests.ConfirmBooking.FieldsGetBooking;
import com.xoocar.Requests.ConfirmBooking.RequestBookingRequest;
import com.xoocar.Requests.ConfirmBooking.ResponceBooking;
import com.xoocar.Requests.GetRideById.FieldsRequestGetCabById;
import com.xoocar.Requests.GetRideById.RequestFieldsGetCabById;
import com.xoocar.Requests.GetRideById.RequestModelGetCabById;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/9/17.
 */

public class WaitingScreenHopNGo extends AppCompatActivity {
    private SessionManager sessionManager;
    private CountDownTimer cdt;
    private LinearLayout noCabsImageHop;
    private boolean isAlive;
    private String rideId;
    private String couponStatus="1";
    private DilatingDotsProgressBar mDilatingDotsProgressBar;
    private LinearLayout animationLayout;
    private String rideEstimate;
    private String eta;
    private CountDownTimer timercontDown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        isAlive=true;

//        cabAnimationRideNow= (ImageView) findViewById(R.id.cabAnimationHopnGo);
        noCabsImageHop= (LinearLayout) findViewById(R.id.noCabsImageHop);

        mDilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progressHop);
        animationLayout = (LinearLayout) findViewById(R.id.animationLayoutHop);
        mDilatingDotsProgressBar.showNow();

        if( !EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(WaitingScreenHopNGo.this);

        Intent i=getIntent();
        sessionManager=new SessionManager(WaitingScreenHopNGo.this);

         String cab = i.getStringExtra(Constants.HOPNGO_CAB_DETAIL);
        rideEstimate= i.getStringExtra(Constants.RIDE_ESTIMATE);
        eta = i.getStringExtra(Constants.ETA);

        CabDetailHopNGo cabDetailHopNGo = new Gson().fromJson(cab, CabDetailHopNGo.class);


        if(cabDetailHopNGo.getCouponId() == null) {
            couponStatus="0";
        }

//        Answers.getInstance().logCustom(new CustomEvent("HopNGo")
//                .putCustomAttribute("userId", sessionManager.getUserId())
//                .putCustomAttribute("Current_lat", sessionManager.getPickupMarkerLat()+","+sessionManager.getPickupMarkerLng())
//        );
        requestBooking(cabDetailHopNGo);
    }

    private void requestBooking(CabDetailHopNGo cabDetailHopNGo) {

        /*
        * vehicle id is category id
        *
        *
        * */

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ResponceBooking> call = request.requestBooking(sessionManager.getAuthToken(),"api/firebaseapi/bookingconfirm/format/json/"
                , new FieldsGetBooking( new RequestBookingRequest(sessionManager.getPickupMarkerLat()
                        ,sessionManager.getPickupMarkerLng()
                        ,cabDetailHopNGo.getId()
                        ,sessionManager.getUserId()
                        ,sessionManager.getPickupAddress()
                        ,sessionManager.getDSestinationAddress()
                        ,rideEstimate
                        ,eta
                        ,cabDetailHopNGo.getPaymenyMode().toLowerCase()
                        ,cabDetailHopNGo.getCouponId()
                        ,couponStatus
                        ,""
                        ,""
                        ,cabDetailHopNGo.getDriverId()
                        ,sessionManager.getCityId()
                        ,sessionManager.getDestLat()
                        ,sessionManager.getDestLng()
                        ,cabDetailHopNGo.getDriverId()
                        ,"0"
                )
                ));

        call.enqueue(new Callback<ResponceBooking>() {
            @Override
            public void onResponse(Call<ResponceBooking> call, Response<ResponceBooking> response) {
                Log.d("aaaa",response.toString());

                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(WaitingScreenHopNGo.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        rideId=response.body().getDataArray().getRideid();
//                    EventBus.getDefault().post( new Message("RefreshCabs") );
                        startTimer();
                    }
                } else {
//                    Todo :add no cabs screen here
                      showNoRespoceScreen();
                }
            }

            @Override
            public void onFailure(Call<ResponceBooking> call, Throwable t) {
                Log.d("aaaa","error 11");
                showNoRespoceScreen();
            }
        });
    }

    public void startTimer() {
       cdt= new CountDownTimer(40*1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("aaaa","" + millisUntilFinished);
            }

            public void onFinish() {
//                Toast.makeText(WaitingScreenHopNGo.this, "Sorry no responce from driver, Please try again", Toast.LENGTH_LONG).show();
                getRideById(rideId);
            }
        };
        cdt.start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Notifications msg) {
        if( isAlive ) {
            if (msg.getType().equals(Constants.ACCEPTED_BY_DRIVER)) {
                if (cdt != null) {
                    cdt.cancel();
                    cdt = null;
                }
                getRideById(msg.getRideId());
            } else if (msg.getType().equals(Constants.REJECTED_BY_DRIVER)) {
//            here rideId is driver id
                if (cdt != null) {
                    cdt.cancel();
                    cdt = null;
                }
                cancelRide(msg.getRideId());
            }
        }
    }

    private void cancelRide(String rideId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CancelRideResponce> call = request.cancelRide(sessionManager.getAuthToken(),"api/processapi/burn_ride/format/json/"
                ,new CancelRideRequestFields(new CancelRideRequestData(rideId,"")));

        call.enqueue(new Callback<CancelRideResponce>() {
            @Override
            public void onResponse(Call<CancelRideResponce> call, Response<CancelRideResponce> response) {

                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( WaitingScreenHopNGo.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            showNoRespoceScreen();
                        } else {
                            showNoRespoceScreen();
                        }
                    } else {
                        showNoRespoceScreen();
                    }
                }else{
                    showNoRespoceScreen();
                }
            }

            @Override
            public void onFailure(Call<CancelRideResponce> call, Throwable t) {
            }
        });
    }

    private void getRideById(String oid) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<RequestFieldsGetCabById> call = request.requestCabById(sessionManager.getAuthToken(),"api/processapi/get_ur_driverinfo/format/json/"
                ,new RequestModelGetCabById(new FieldsRequestGetCabById(oid)));

        call.enqueue(new Callback<RequestFieldsGetCabById>() {
            @Override
            public void onResponse(Call<RequestFieldsGetCabById> call, Response<RequestFieldsGetCabById> response) {

                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( WaitingScreenHopNGo.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            com.xoocar.Requests.GetRideById.RequestData cab= response.body().getDataArray().get(0);
                            sessionManager.setRideId(cab.getRideid());
                            Intent i=new Intent(WaitingScreenHopNGo.this,RideScheduled.class);
                            i.putExtra(Constants.CAB_BY_ID,new Gson().toJson(cab));
                            startActivity(i);
                            finish();
                        } else {
                            showNoRespoceScreen();
                        }
                    } else {
                        showNoRespoceScreen();
                    }
                } else {
                    cancelRide(rideId);
                }
            }

            @Override
            public void onFailure(Call<RequestFieldsGetCabById> call, Throwable t) {
            }
        });
    }

    private void showNoRespoceScreen() {
        if(cdt != null) {
            cdt.cancel();
            cdt=null;
        }
        sessionManager.setRideId("0");
        sessionManager.setFrndName("");
        sessionManager.setFrndNumber("");
        mDilatingDotsProgressBar.hideNow();
        animationLayout.setVisibility(View.GONE);
        noCabsImageHop.setVisibility(View.VISIBLE);

        startExitTimer();
    }

    private void startExitTimer() {
            try {
                timercontDown = new CountDownTimer(5*1000, 1000) {

                    public void onTick( long millisUntilFinished ) {
                    }
                    public void onFinish() {
                        Intent i=new Intent(WaitingScreenHopNGo.this,MainActivity.class);
                        startActivity(i);
                    }
                }.start();
            } catch (Throwable e) {
                e.printStackTrace();
            }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if( timercontDown != null )
        {
            timercontDown.cancel();
        }

        if(cdt != null) {
            cdt=null;
        }
        if( EventBus.getDefault().isRegistered(this) )
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if( cdt == null ) {
            finish();
        } else {
            Toast.makeText(this, "Please wait booking your ride", Toast.LENGTH_SHORT).show();
        }
    }
}
