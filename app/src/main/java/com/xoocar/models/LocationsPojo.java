package com.xoocar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/23/17.
 */

public class LocationsPojo {
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("timestamp")
    @Expose
    private Long timestamp;
    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("accuracy")
    @Expose
    private Boolean accuracy;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("course")
    @Expose
    private Float course;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Boolean accuracy) {
        this.accuracy = accuracy;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Float getCourse() {
        return course;
    }

    public void setCourse(Float course) {
        this.course = course;
    }
}
