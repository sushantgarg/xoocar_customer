package com.xoocar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Realm.PriceCategoryRealm;

/**
 * Created by sushant on 10/4/17.
 */

public class CabPricing {

    @SerializedName("range1Charge")
    @Expose
    private Integer range1Charge;
    @SerializedName("range2Lower")
    @Expose
    private Integer range2Lower;
    @SerializedName("baseFare")
    @Expose
    private Integer baseFare;
    @SerializedName("range2Charge")
    @Expose
    private Integer range2Charge;
    @SerializedName("range1Upper")
    @Expose
    private Integer range1Upper;
    @SerializedName("perMinCharge")
    @Expose
    private Double perMinCharge;
    @SerializedName("baseKm")
    @Expose
    private Integer baseKm;
    @SerializedName("range1Lower")
    @Expose
    private Integer range1Lower;
    @SerializedName("range2Upper")
    @Expose
    private Integer range2Upper;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    private Integer categoryId;

    private String cityId;
    private Integer numberOfSeats;

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public CabPricing(PriceCategoryRealm price) {
        this.range1Charge = price.getRange1Charge();
        this.range2Lower = price.getRange2Lower();
        this.baseFare = price.getBaseFare();
        this.range2Charge = price.getRange2Charge();
        this.range1Upper = price.getRange1Upper();
        this.perMinCharge = price.getPerMinCharge();
        this.baseKm = price.getBaseKm();
        this.range1Lower = price.getRange1Lower();
        this.range2Upper = price.getRange2Upper();
        this.categoryName = price.getCategoryName();
        this.categoryId = price.getCategoryId();
        this.cityId = price.getCityId();
        this.numberOfSeats=price.getNumberOfSeats();
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getRange1Charge() {
        return range1Charge;
    }

    public void setRange1Charge(Integer range1Charge) {
        this.range1Charge = range1Charge;
    }

    public Integer getRange2Lower() {
        return range2Lower;
    }

    public void setRange2Lower(Integer range2Lower) {
        this.range2Lower = range2Lower;
    }

    public Integer getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(Integer baseFare) {
        this.baseFare = baseFare;
    }

    public Integer getRange2Charge() {
        return range2Charge;
    }

    public void setRange2Charge(Integer range2Charge) {
        this.range2Charge = range2Charge;
    }

    public Integer getRange1Upper() {
        return range1Upper;
    }

    public void setRange1Upper(Integer range1Upper) {
        this.range1Upper = range1Upper;
    }

    public Double getPerMinCharge() {
        return perMinCharge;
    }

    public void setPerMinCharge(Double perMinCharge) {
        this.perMinCharge = perMinCharge;
    }

    public Integer getBaseKm() {
        return baseKm;
    }

    public void setBaseKm(Integer baseKm) {
        this.baseKm = baseKm;
    }

    public Integer getRange1Lower() {
        return range1Lower;
    }

    public void setRange1Lower(Integer range1Lower) {
        this.range1Lower = range1Lower;
    }

    public Integer getRange2Upper() {
        return range2Upper;
    }

    public void setRange2Upper(Integer range2Upper) {
        this.range2Upper = range2Upper;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
