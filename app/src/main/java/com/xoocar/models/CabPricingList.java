package com.xoocar.models;

import java.util.ArrayList;

/**
 * Created by sushant on 10/4/17.
 */

public class CabPricingList {
    public ArrayList<CabPricing> getPriceList() {
        return priceList;
    }

    public void setPriceList(ArrayList<CabPricing> priceList) {
        this.priceList = priceList;
    }

    ArrayList<CabPricing> priceList;
}
