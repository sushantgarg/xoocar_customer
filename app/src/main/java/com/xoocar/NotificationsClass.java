package com.xoocar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.xoocar.Adapter.NotiAdapter;
import com.xoocar.Realm.RealmManager;
import com.xoocar.SessionManager.SessionManager;

/**
 * Created by sushant on 14/8/17.
 */

public class NotificationsClass extends AppCompatActivity {
    private NotificationsClass ctx;
    private SessionManager sessionManager;
    private RecyclerView notiList;
    private NotiAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        sessionManager=new SessionManager(this);
        ctx=NotificationsClass.this;

        notiList = (RecyclerView) findViewById(R.id.notificationList);
        notiList.setLayoutManager(new LinearLayoutManager(ctx));

        adapter = new NotiAdapter(RealmManager.getNotifications(), ctx);
        notiList.setAdapter(adapter);

    }
}
