package com.xoocar;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.xoocar.AsyncTasks.GetpathAsync;
import com.xoocar.Delegates.GetPathDelegate;
import com.xoocar.EventBusModels.Notifications;
import com.xoocar.FireBase.FirebaseManager;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.CancelRide.CancelRideRequestData;
import com.xoocar.Requests.CancelRide.CancelRideRequestFields;
import com.xoocar.Requests.CancelScheduledRide.CancelRideResponceFields;
import com.xoocar.Requests.GetDriverPosition.GetDriverPositionRequestData;
import com.xoocar.Requests.GetDriverPosition.GetDriverPositionResponce;
import com.xoocar.Requests.GetDriverPosition.GetDriverRequestFields;
import com.xoocar.Requests.GetDriverPosition.GetDriverResponceFields;
import com.xoocar.Requests.GetRideById.RequestData;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.UnsupportedEncodingException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 3/8/17.
 */

public class RideScheduled extends AppCompatActivity implements OnMapReadyCallback {

    private ImageView driverImage;
    Handler handler = new Handler();
    private Marker driverMarker;
    private GoogleMap gMap;
    private SessionManager sessionManager;
    private float zoom = 16;
    private TextView driverNum,carModel,carNum;
    private RequestData cab;
    private String driverId="0";
    private Polyline navigationPathPoliline;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_confirmation_new);

        TextView driverName = findViewById(R.id.driverName);
        driverNum = findViewById(R.id.driverNum);
        carModel = findViewById(R.id.carModelConfirm);
        carNum = findViewById(R.id.carNum);
        TextView binNum = findViewById(R.id.binNum);
        TextView sourcsAddress = findViewById(R.id.source_address_booking);
        TextView destAddress = findViewById(R.id.destination_address_booking);

        LinearLayout callDriver = findViewById(R.id.callDriver);
        LinearLayout cancelLinear = findViewById(R.id.cancelRide);
        LinearLayout shareRide = findViewById(R.id.shareRide);

        ImageView myLoc=findViewById(R.id.myLocBtnBooking);

        driverImage = findViewById(R.id.driverImage);

        SupportMapFragment mMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapFragmentConfirm);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.mapFragmentConfirm
                    , mMapFragment).commitAllowingStateLoss();
        }

        mMapFragment.getMapAsync(this);

        Intent i = getIntent();
        final String cabDetail = i.getStringExtra(Constants.CAB_BY_ID);
        cab = new Gson().fromJson(cabDetail, RequestData.class);

        sessionManager = new SessionManager(this);
        driverName.setText(cab.getFirstName());

        if( cab.getContact() != null ) driverNum.setText(cab.getContact());
        if( cab.getModal()!= null ) carModel.setText(cab.getModal());

        carNum.setText(cab.getVLicenceNo());
        binNum.setText("BIN : " + cab.getCrnNo());

        sessionManager.setCancellationFee(RealmManager.getCabCategory(cab.getVCategory()).getBaseFare()/2);

        sourcsAddress.setText(sessionManager.getPickupAddress());
        destAddress.setText(sessionManager.getDSestinationAddress());

        setProfilePic();

        callDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String driverNum=cab.getContact();

                if ( driverNum != null ) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driverNum));
                    if (ActivityCompat.checkSelfPermission(RideScheduled.this, android.Manifest.permission.CALL_PHONE) !=
                            PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        return;
                    }
                    startActivity(intent);
                }
            }
        });

        cancelLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmDialog("Cancel Ride", "Are you sure you want to cancel ride?");
            }
        });

        shareRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] data = new byte[0];
                try {
//                    Log.d("dkp share", ":link:" + sessionManager.getRideId());
                    data = cab.getRideid().getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                String share_link_base64 = Constants.SHARE_LINK + base64;
                Log.d("dkp share", ":link:" + share_link_base64);
                share_link_base64 = share_link_base64.replace("==", "");
                share_link_base64 = share_link_base64.replace("=", "");

                String base = sessionManager.getUserName() + " is on ride with XooCar. Follow his ride";

                CommonMethods.showShareIntentDialog(base + " " + "http://velenesa.com/vele/index.php/" + share_link_base64, RideScheduled.this);
            }
        });

        myLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                Double.parseDouble(sessionManager.getCurrentLat())
                                ,Double.parseDouble( sessionManager.getCurrentLng()))
                        , zoom));

            }
        });
    }

    private void setProfilePic() {
        if( cab.getProfilePic() != null ) {

            String url = "http://velenesa.com/vele/index.php/assets/profileimage/" + cab.getProfilePic();

            Glide.with(this)
                    .load(url)
                    .asBitmap()
                    .into(new BitmapImageViewTarget(driverImage) {

                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            super.onResourceReady(resource, glideAnimation);

                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            driverImage.setImageDrawable(circularBitmapDrawable);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                        }
                    });
        }
    }

    protected void onSaveInstanceState(Bundle icicle) {
        icicle.putLong("param", 10000);
        super.onSaveInstanceState(icicle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            PackageManager packageManager = RideScheduled.this.getPackageManager();
            Intent intent = packageManager.getLaunchIntentForPackage(RideScheduled.this.getPackageName());
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
            RideScheduled.this.startActivity(mainIntent);
            System.exit(0);
        }
    }


    private void calcelScheduledRide() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<CancelRideResponceFields> call = request.cancelScheduledRide(sessionManager.getAuthToken()
                , "api/processapi/cancelledbycustomer/format/json/"
                , new CancelRideRequestFields(new CancelRideRequestData(cab.getRideid(), "2")));

        call.enqueue(new Callback<CancelRideResponceFields>() {
            @Override
            public void onResponse(Call<CancelRideResponceFields> call, Response<CancelRideResponceFields> response) {

                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideScheduled.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body() != null) {
//                            sessionManager.setRideId("0");
                            sessionManager.setFrndName("");
                            sessionManager.setFrndNumber("");
                            Toast.makeText(RideScheduled.this, "Ride cancelled successfully", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(RideScheduled.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CancelRideResponceFields> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Notifications msg) {
        if (msg.getType().equals(Constants.REJECTED_AFTER_SCHEDULE)) {
            showRideCancelScreen();
        } else if (msg.getType().equals(Constants.RUNNING)) {
            rideRunning();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CabsRealm cab) {
        /* Do something */
        if( driverId.equals( cab.getUid() ) ) {
            animateMarker(cab);
        }
    }

    private void showRideCancelScreen() {
        handler.removeCallbacks(runnable);
        Intent i = new Intent(RideScheduled.this, BookingCancelled.class);
        startActivity(i);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            doPost();
            handler.postDelayed(runnable, 25 * 1000);
        }
    };

    private void doPost() {
        getDriverPosition();
    }

    private void getDriverPosition() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetDriverPositionResponce> call = request.getDriverPosition(sessionManager.getAuthToken()
                , "api/processapi/mydriverloc/format/json/"
                , new GetDriverRequestFields(new GetDriverPositionRequestData(cab.getRideid(), sessionManager.getUserId()))
        );

        call.enqueue(new Callback<GetDriverPositionResponce>() {
            @Override
            public void onResponse(Call<GetDriverPositionResponce> call, Response<GetDriverPositionResponce> response) {

                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideScheduled.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body() != null) {
                            GetDriverResponceFields status = response.body().getDataArray();

                             driverNum.setText(status.getContact());
                             carModel.setText(status.getModal());
                             cab.setContact(status.getContact());
                             carNum.setText(status.getV_licence_no());
                            driverId=status.getDriverId();

                            switch (status.getBookingStatus()) {
                                case "Schedule":
//                        call pickup fragment
                                    FirebaseManager.getInstance().getCabPosition(status.getDriverId());
                                    break;

                                case "Running":
                                    rideRunning();
                                    break;

                                case "Completed":
                                    rideRunning();
                                    break;

                                case "Cancelled":
                                    showRideCancelScreen();
//                                    sessionManager.setRideId("0");
                                    sessionManager.setFrndName("");
                                    sessionManager.setFrndNumber("");
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetDriverPositionResponce> call, Throwable t) {
            }
        });
    }

    private void rideRunning() {
        Intent i = new Intent(getApplicationContext(), RideRunningScreen.class);
        i.putExtra(Constants.RIDE_ID,cab.getRideid());
        startActivity(i);
        handler.removeCallbacks(runnable);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if( EventBus.getDefault().isRegistered(this) ) {
            EventBus.getDefault().unregister(this);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    private void animateMarker(CabsRealm cabs) {

        if ( driverMarker == null ) {

            addDriverMarker(Double.parseDouble(cabs.getdLatt())
                    , Double.parseDouble(cabs.getdLong())
                    , Float.parseFloat(cabs.getCourse())
                    ,cabs.getCabType());

            addPath( Double.parseDouble(cabs.getdLatt()),Double.parseDouble(cabs.getdLong()));
            addMyLocMarker();
        } else {
            driverMarker.setRotation(Float.parseFloat(cabs.getCourse()));
            CommonMethods.animateMarkerToNewLoaction(driverMarker
                    , Double.parseDouble(cabs.getdLatt())
                    , Double.parseDouble(cabs.getdLong()));

            addPath( Double.parseDouble(cabs.getdLatt()),Double.parseDouble(cabs.getdLong()));

            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    Double.parseDouble(cabs.getdLatt()), Double.parseDouble(cabs.getdLong())), zoom));
        }
    }

    private void addDriverMarker(double lat, double lng, float course, String cabType) {

        Bitmap b=CommonMethods.getCabIcon(cabType,RideScheduled.this);

        driverMarker = gMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.fromBitmap(b))
                .rotation(course));

        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                lat, lng), zoom));
    }

    private void addMyLocMarker() {

        Drawable mDrawable = getResources().getDrawable(R.drawable.pickup_homescreen_svg);
        final Bitmap b= CommonMethods.convertDrawableToBitmap(mDrawable);

        gMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(sessionManager.getPickupMarkerLat())
                        ,Double.parseDouble(sessionManager.getPickupMarkerLng())))
                .icon(BitmapDescriptorFactory.fromBitmap(b))
                .title("Pickup Location")
        );
    }

    private void addPath(double lat, double lng) {
        new GetpathAsync(new LatLng(Double.parseDouble(sessionManager.getPickupMarkerLat())
                ,Double.parseDouble(sessionManager.getPickupMarkerLng()))
                , this
                ,new LatLng(lat,lng)
                ,new GetPathDelegate() {
            @Override
            public void processFinish(List<LatLng> encodedPath) {

                if (encodedPath.size() >= 1) {
                    drawNavigationPath(encodedPath);
                }
            }
        }).execute();
    }

    private void drawNavigationPath(List<LatLng> encodedPath) {

        if( navigationPathPoliline != null && navigationPathPoliline.getPoints().size() > 0 ) {
            navigationPathPoliline.remove();
        }
        PolylineOptions lineOptions = null;
        lineOptions = new PolylineOptions();

        lineOptions.addAll(encodedPath);
        navigationPathPoliline=gMap.addPolyline(lineOptions);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }

        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                Double.parseDouble(sessionManager.getCurrentLat())
                ,Double.parseDouble( sessionManager.getCurrentLng()))
                , zoom));

        handler.postDelayed(runnable,5*1000);

        gMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
               zoom= gMap.getCameraPosition().zoom;
            }
        });

    }

    public void confirmDialog(String title, String msg) {

        dialog = new Dialog(RideScheduled.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmation_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        TextView tv=dialog.findViewById(R.id.message_cancel);
        TextView titleTv=dialog.findViewById(R.id.dialog_title);

        titleTv.setText("Confirm Cancellation");
        String message="Ride cancellation will levi "
                +getString(R.string.rupee_symbol)
                +sessionManager.getCancellationCharge()
                +" as cancellation fee. \n Tap ok to proceed cancellation";
        tv.setText(message);

        dialog.findViewById(R.id.ok_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        calcelScheduledRide();
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        dialog.show();

    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(RideScheduled.this,MainActivity.class);
        startActivity(i);
        handler.removeCallbacks( runnable );
        finish();
    }
}
