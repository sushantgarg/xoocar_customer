package com.xoocar;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xoocar.FireBase.FirebaseManager;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Requests.AddCouponAfterScheduled.AddCouponLaterRequestData;
import com.xoocar.Requests.AddCouponAfterScheduled.AddCouponLaterRequestFields;
import com.xoocar.Requests.AddCouponAfterScheduled.AddCouponLaterResponceFields;
import com.xoocar.Requests.ChangePaymentMode.ChangePAymentRequestFields;
import com.xoocar.Requests.ChangePaymentMode.ChangePaymentRequestData;
import com.xoocar.Requests.ChangePaymentMode.ChangePaymentResponceFields;
import com.xoocar.Requests.EditDestination.EditDestinationRequestData;
import com.xoocar.Requests.EditDestination.EditDestinationResponceFields;
import com.xoocar.Requests.EditDestination.EditDestinationrequestFields;
import com.xoocar.Requests.GetDriverPosition.GetDriverPositionRequestData;
import com.xoocar.Requests.GetDriverPosition.GetDriverPositionResponce;
import com.xoocar.Requests.GetDriverPosition.GetDriverRequestFields;
import com.xoocar.Requests.GetDriverPosition.GetDriverResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/17/17.
 */

public class RideRunningScreen extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    Handler handler = new Handler();
    private Marker driverMarker;
    private GoogleMap gMap;
    private SessionManager sessionManager;
    private float zoom = 16;
    private TextView destAddress;
    private ImageView paymentTypeIcon;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION = 3;
    private String rideId;
    private String tripType;
    private String driverId="0";
    private int categoryId=-1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);
        sessionManager = new SessionManager(this);

        LinearLayout paymentMode = findViewById(R.id.paymentModeRunning);
        LinearLayout applyCoupon = findViewById(R.id.applyCouponRunning);
        LinearLayout shareRide = findViewById(R.id.shareRideRunning);
        TextView sourceAddress = findViewById(R.id.source_address_running);
        destAddress = findViewById(R.id.destination_address_running);
        TextView sos = findViewById(R.id.sosImage);
        paymentTypeIcon = findViewById(R.id.paymentTypeIcon);
        ImageView myLocBtnRideRunning = findViewById(R.id.myLocBtnRideRunning);

        SupportMapFragment mMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapFragmentRunning);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.mapFragmentRunning
                    , mMapFragment).commitAllowingStateLoss();
        }

        mMapFragment.getMapAsync(this);

        Intent i=getIntent();
        rideId=i.getStringExtra(Constants.RIDE_ID);

        paymentMode.setOnClickListener(this);
        applyCoupon.setOnClickListener(this);
        shareRide.setOnClickListener(this);
        destAddress.setOnClickListener(this);
        sos.setOnClickListener(this);
        myLocBtnRideRunning.setOnClickListener(this);

        sourceAddress.setText(sessionManager.getPickupAddress());
        destAddress.setText(sessionManager.getDSestinationAddress());
    }

    protected void onSaveInstanceState(Bundle icicle) {
        icicle.putLong("param", 10000);
        super.onSaveInstanceState(icicle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            PackageManager packageManager = RideRunningScreen.this.getPackageManager();
            Intent intent = packageManager.getLaunchIntentForPackage(RideRunningScreen.this.getPackageName());
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
            RideRunningScreen.this.startActivity(mainIntent);
            System.exit(0);
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            doPost();
            handler.postDelayed(runnable, 15 * 1000);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (! EventBus.getDefault().isRegistered(RideRunningScreen.this)) {
            EventBus.getDefault().register(RideRunningScreen.this);
        }
        doPost();
    }

    private void doPost() {
        getDriverPosition();
    }

    private void getDriverPosition() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetDriverPositionResponce> call = request.getDriverPosition(sessionManager.getAuthToken(), "api/processapi/mydriverloc/format/json/"
                , new GetDriverRequestFields(new GetDriverPositionRequestData(rideId, sessionManager.getUserId()))
        );

        call.enqueue(new Callback<GetDriverPositionResponce>() {
            @Override
            public void onResponse(Call<GetDriverPositionResponce> call
                    , Response<GetDriverPositionResponce> response) {

                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideRunningScreen.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body() != null) {
                            GetDriverResponceFields status = response.body().getDataArray();
                            driverId=status.getDriverId();
                            tripType=status.getRidetype();
                            categoryId=status.getCat_id();
                            if (status.getPay_mode().equals("cash")) {
                                paymentTypeIcon.setImageResource(R.drawable.cash_icon);
                            } else {
                                paymentTypeIcon.setImageResource(R.drawable.wallet_icon);
                            }

                            switch (status.getBookingStatus()) {
                                case "Running":
//                                    animateMarker(Double.parseDouble(status.getdLatt())
//                                            , Double.parseDouble(status.getdLong())
//                                            , status.getCourse());

                                    FirebaseManager.getInstance().getCabPosition(status.getDriverId());
                                    break;

                                case "Completed":
                                    showBillSummary();
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetDriverPositionResponce> call, Throwable t) {
            }
        });
    }

    private void showBillSummary() {
        handler.removeCallbacks(runnable);
        Intent i = new Intent(getApplicationContext(), BillDetail.class);
        i.putExtra(Constants.RIDE_ID,rideId);
        startActivity(i);
    }

    private void animateMarker(double lat, double lng, String course, String cabType) {
        if (driverMarker == null) {
            addDriverMarker(lat, lng, Float.parseFloat(course),cabType);
        } else {
            driverMarker.setRotation(Float.parseFloat(course));
            CommonMethods.animateMarkerToNewLoaction(driverMarker, lat, lng);
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    lat, lng), zoom));
        }
    }

    private void addDriverMarker(double lat, double lng, float course, String cabType) {
        Bitmap b=CommonMethods.getCabIcon(cabType,RideRunningScreen.this);

        driverMarker = gMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.fromBitmap(b))
                .rotation(course));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.paymentModeRunning:
                Intent payment = new Intent(RideRunningScreen.this, PaymentActivity.class);
                startActivityForResult(payment, Constants.PAYMENT_CODE);

                break;
            case R.id.applyCouponRunning:

                if( categoryId == -1 ){
                    Toast.makeText(this, "Please wait for a moment...", Toast.LENGTH_SHORT).show();
                } else {
                    Intent pickCoupon = new Intent(RideRunningScreen.this, CouponActivity.class);
                    pickCoupon.putExtra(Constants.TRIPTYPE, tripType);
                    pickCoupon.putExtra(Constants.VEHICLE_TYPE, ""+categoryId);
                    pickCoupon.putExtra(Constants.COUPON_SOURCE,"running");
                    startActivityForResult(pickCoupon, Constants.PICK_COUPONS);
                }
                break;

            case R.id.myLocBtnRideRunning:
                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                Double.parseDouble(sessionManager.getCurrentLat())
                                ,Double.parseDouble( sessionManager.getCurrentLng()))
                        , zoom));

                break;
            case R.id.shareRideRunning:

                byte[] data = new byte[0];
                try {
                    data = rideId.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                String share_link_base64 = Constants.SHARE_LINK + base64;
                Log.d("dkp share", ":link:" + share_link_base64);
                share_link_base64 = share_link_base64.replace("==", "");
                share_link_base64 = share_link_base64.replace("=", "");

                String base = sessionManager.getUserName() + " is on ride with XooCar. Follow his ride";

                CommonMethods.showShareIntentDialog(base + " " + "http://velenesa.com/vele/index.php/" + share_link_base64, RideRunningScreen.this);
                break;

            case R.id.destination_address_running:
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setCountry("IN")
                        .build();

                Intent intent =
                        null;
                try {
                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION);
                break;

            case R.id.sosImage:
                Intent intentSos = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "100"));
                if (ActivityCompat.checkSelfPermission(RideRunningScreen.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                startActivity(intentSos);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Constants.PAYMENT_CODE) {
            // Make sure the request was successful
            String paymentType = data.getStringExtra(Constants.PAYMENT_TYPE);
            if (paymentType.equals("cash")) {
                changePaymentMode("cash");
                paymentTypeIcon.setImageResource(R.drawable.cash_icon);
            } else {
                changePaymentMode(paymentType.toLowerCase());
                paymentTypeIcon.setImageResource(R.drawable.wallet_icon);
            }
        } else if (resultCode == Constants.PICK_COUPONS) {
            // Make sure the request was successful
            changeCoupon(data.getStringExtra(Constants.COUPON_ID));
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                sendDestination(place.getLatLng(), place.getAddress());
                destAddress.setText("" + place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Toast.makeText(this, "No place found please try again", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    private void sendDestination(LatLng latLng, CharSequence address) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<EditDestinationResponceFields> call = request.editDestination(sessionManager.getAuthToken()
                , "api/processapi/changedestination/format/json/"
                , new EditDestinationrequestFields(new EditDestinationRequestData(rideId
                        , (String) address
                        , String.valueOf(latLng.latitude)
                        , String.valueOf(latLng.longitude))));
        call.enqueue(new Callback<EditDestinationResponceFields>() {

            @Override
            public void onResponse(Call<EditDestinationResponceFields> call, Response<EditDestinationResponceFields> response) {
                Log.d("aaaa", response.toString());
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideRunningScreen.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        Toast.makeText(RideRunningScreen.this, "Destination changed successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RideRunningScreen.this, "Error changing destination", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditDestinationResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
                Toast.makeText(RideRunningScreen.this, "Error changing destination", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changeCoupon(String couponId) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<AddCouponLaterResponceFields> call = request.changeCoupon(sessionManager.getAuthToken()
                , "api/processapi/applycoupon/format/json/"
                , new AddCouponLaterRequestFields(new AddCouponLaterRequestData(rideId, couponId)));
        call.enqueue(new Callback<AddCouponLaterResponceFields>() {

            @Override
            public void onResponse(Call<AddCouponLaterResponceFields> call, Response<AddCouponLaterResponceFields> response) {
                Log.d("aaaa", response.toString());
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideRunningScreen.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        Toast.makeText(RideRunningScreen.this, "Coupon applied successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RideRunningScreen.this, "Error applying coupon", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddCouponLaterResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
                Toast.makeText(RideRunningScreen.this, "Error applying coupon", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changePaymentMode(String paymentType) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ChangePaymentResponceFields> call = request.changePayment(sessionManager.getAuthToken()
                , "api/processapi/changepaymode/format/json/"
                , new ChangePAymentRequestFields(new ChangePaymentRequestData(rideId, paymentType.toLowerCase())));
        call.enqueue(new Callback<ChangePaymentResponceFields>() {

            @Override
            public void onResponse(Call<ChangePaymentResponceFields> call, Response<ChangePaymentResponceFields> response) {
                Log.d("aaaa", response.toString());
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RideRunningScreen.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        Toast.makeText(RideRunningScreen.this, "Payment mode changed successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePaymentResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        Double.parseDouble(sessionManager.getCurrentLat())
                        ,Double.parseDouble( sessionManager.getCurrentLng()))
                , zoom));

        handler.postDelayed(runnable,1000);

        gMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                zoom= gMap.getCameraPosition().zoom;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(EventBus.getDefault().isRegistered(RideRunningScreen.this)) {
            EventBus.getDefault().unregister(RideRunningScreen.this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(com.xoocar.EventBusModels.Notifications msg) {
        if(msg.getType().equals(Constants.RIDE_COMPLETED)) {
            showBillSummary();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CabsRealm cab) {
        /* Do something */
        if( cab.getUid().equals(driverId)) {
            animateMarker(Double.parseDouble(cab.getdLatt())
                    , Double.parseDouble(cab.getdLong())
                    , cab.getCourse()
                    ,cab.getCabType());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(RideRunningScreen.this,MainActivity.class);
        startActivity(i);
        handler.removeCallbacks( runnable );
        finish();
    }
}
