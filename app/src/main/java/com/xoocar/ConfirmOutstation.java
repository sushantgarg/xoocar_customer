package com.xoocar;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.xoocar.CCAvenue.CCAvenue;
import com.xoocar.Requests.ConfirmIntercityBooking.ConfirmIntercityRequestData;
import com.xoocar.Requests.ConfirmIntercityBooking.ConfirmIntercityRequestFields;
import com.xoocar.Requests.ConfirmIntercityBooking.ConfirmIntercityResponceFields;
import com.xoocar.Requests.WalletBalanceRequest.RequestFieldsWallet;
import com.xoocar.Requests.WalletBalanceRequest.RequestWalletBalance;
import com.xoocar.Requests.WalletBalanceRequest.ResponceWalletBalance;
import com.xoocar.Requests.WalletBalanceRequest.WalletDataArray;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 11/10/17.
 */

public class ConfirmOutstation extends AppCompatActivity implements View.OnClickListener {
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 14;
    private TextView totalEstimatedFare;
    private TextView toCity;
    private TextView paymentMode;
    private TextView applyCoupon;
    private TextView confirmPickUpLocationText;
    private TextView cabType;
    private String couponId="";
    private SessionManager sessionManager;
    private String trip_Type;
    private String perKmCharge;
    private String categoryType;
    private String estimatedFare;
    private ImageView cabImage;
    private String driverCharge;
    private String nod;
    private String travel_Date;
    private String pickUp;
    private String pickupCityId;
    private String dropCityId;
    private String pickUp_Time;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.activity_explode));
        setContentView(R.layout.activity_confirm_outstation);

        confirmPickUpLocationText =(TextView) findViewById(R.id.confirmPickUpLocationText);

        totalEstimatedFare = (TextView) findViewById(R.id.totalEstimatedFare);
        TextView fromCity = (TextView) findViewById(R.id.fromCity);
        toCity = (TextView) findViewById(R.id.toCity);
        LinearLayout editPickUpLocation = findViewById(R.id.editPickUpLocation);
        TextView tripType = (TextView) findViewById(R.id.tripType);
        TextView travelDate = (TextView) findViewById(R.id.travelDate);
        TextView pickUpTime = (TextView) findViewById(R.id.pickUpTime);
        ImageView termsAndCondition = (ImageView) findViewById(R.id.termsAndCondition);
        paymentMode = (TextView) findViewById(R.id.paymentMode);
        applyCoupon = (TextView) findViewById(R.id.applyCoupon);
        TextView bookingConfirmation = (TextView) findViewById(R.id.bookingConfirmation);

        LinearLayout fareBreakkup = (LinearLayout) findViewById(R.id.fareBreakkup);
        cabImage = (ImageView) findViewById(R.id.cabImage);

        LinearLayout paymentModeLinear = findViewById(R.id.paymentModeLinear);
        LinearLayout applyCouponLinear = findViewById(R.id.applyCouponLinear);
        cabType = (TextView) findViewById(R.id.cabType);
        TextView distanceConfirm = (TextView) findViewById(R.id.distanceConfirm);
        sessionManager=new SessionManager(ConfirmOutstation.this);

        Intent intent = getIntent();
        estimatedFare = intent.getStringExtra(Constants.TOTALESTIMATEDFARE);
        String from_City = intent.getStringExtra(Constants.FROMCITY);
        String to_City = intent.getStringExtra(Constants.TOCITY);
        pickUp = intent.getStringExtra(Constants.PICKUPLOCATIONIMG);
        trip_Type = intent.getStringExtra(Constants.TRIPTYPE);
        travel_Date = intent.getStringExtra(Constants.TRAVELDATE);
        pickUp_Time = intent.getStringExtra(Constants.PICKUPTIME);
        String cabTypeString = intent.getStringExtra(Constants.CAB_TYPE);
        String distance = intent.getStringExtra(Constants.DISTANCE);
        perKmCharge = intent.getStringExtra(Constants.PER_KM_CHARGE);
        categoryType = intent.getStringExtra(Constants.CATEGORY_TYPE);
        driverCharge = intent.getStringExtra(Constants.DRIVER_PER_DAY_CHARGE);
        nod = intent.getStringExtra(Constants.NUM_OF_DAYS);
        pickupCityId = intent.getStringExtra(Constants.PICKUP_CITY_ID);
        dropCityId = intent.getStringExtra(Constants.DROP_CITY_ID);

        totalEstimatedFare.setText(getString(R.string.rupee_symbol)+estimatedFare);
        fromCity.setText(from_City);
        toCity.setText(to_City);
        confirmPickUpLocationText.setText(pickUp);
        distanceConfirm.setText("Distance: "+ distance +" Km");
        if(trip_Type.equals("2")) {
            tripType.setText("Trip Type: Round Trip");
        }else{
            tripType.setText("Trip Type: One Way");
        }
        travelDate.setText(travel_Date);

        pickUpTime.setText( CommonMethods.convertDateUnixToTime(Long.parseLong(pickUp_Time)) );
        cabType.setText(cabTypeString);

        getCabIcon(categoryType);

        paymentModeLinear.setOnClickListener(this);
        applyCouponLinear.setOnClickListener(this);
        totalEstimatedFare.setOnClickListener(this);
        fromCity.setOnClickListener(this);
        toCity.setOnClickListener(this);
        editPickUpLocation.setOnClickListener(this);
        tripType.setOnClickListener(this);
        travelDate.setOnClickListener(this);
        pickUpTime.setOnClickListener(this);
        termsAndCondition.setOnClickListener(this);
        bookingConfirmation.setOnClickListener(this);
        fareBreakkup.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWalletDetails();
    }

    private void getCabIcon(String categoryType) {
        switch (categoryType){
            case "2":
                cabImage.setImageResource(R.drawable.hachback_icon);
                break;
            case "3":
                cabImage.setImageResource(R.drawable.sedan_icon);
                break;
            case "6":
                cabImage.setImageResource(R.drawable.suv_icon);
                break;
            case "7":
                cabImage.setImageResource(R.drawable.minibus_icon);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.editPickUpLocation:
                getPickupAddress();
                break;

            case R.id.paymentModeLinear :
                onClickPaymentMode();
                break;

            case R.id.applyCouponLinear:
                onClickApplyCoupon();
                break;

            case R.id.bookingConfirmation:
                onClickBookingConfirm();
                break;
            case R.id.fareBreakkup:
                onClickFareBreakup();
                break;
        }
    }

    private void onClickFareBreakup() {
        Dialog dialog = new Dialog(ConfirmOutstation.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.farebreakup_popup);

        final TextView categoryFareBreakup,totalFareFareBrkUp,perKmFareBrkUp,totalDistance,numDays,noteCharge,afterDate;

        categoryFareBreakup= dialog.findViewById(R.id.categoryFareBreakup);
        totalFareFareBrkUp= dialog.findViewById(R.id.totalFareFareBrkUp);
        perKmFareBrkUp= dialog.findViewById(R.id.perKmFareBrkUp);
        totalDistance= dialog.findViewById(R.id.totalDistance);
        numDays = dialog.findViewById(R.id.numDays);
        noteCharge = dialog.findViewById(R.id.noteCharge);
        afterDate = dialog.findViewById(R.id.afterDate);


        categoryFareBreakup.setText(cabType.getText().toString());
        totalFareFareBrkUp.setText(totalEstimatedFare.getText().toString());
        numDays.setText(nod);

        afterDate.setText("After - "+travel_Date);
        int totalKm=Integer.parseInt(nod)*250;
        noteCharge.setText("Minimum "+totalKm+"KMs will be charged for "+nod+" days");
        if(trip_Type.equals("1")) {
            perKmFareBrkUp.setText( getString(R.string.rupee_symbol) + "0" );
        } else {
            perKmFareBrkUp.setText( nod + "days x " + getString(R.string.rupee_symbol) + driverCharge );
        }
        totalDistance.setText( totalKm+" Km x " + getString( R.string.rupee_symbol )+perKmCharge+" /Km" );

        dialog.show();
    }

    private void onClickBookingConfirm() {
        double estFare=Double.parseDouble(estimatedFare);
        double walletAmt=Double.parseDouble(sessionManager.getWalletBalance());
        if( walletAmt > estFare/4 ) {
            confirmBooking();
        } else {
            int est = (int) estFare / 4;
//            Toast.makeText(this, "Wallet balance should be greater than " + ++est , Toast.LENGTH_SHORT).show();
            showRatingDialog(String.valueOf(++est));
        }
    }


    private void getWalletDetails() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ResponceWalletBalance> call = request.getWalletBalance(sessionManager.getAuthToken()
                ,"api/newwalletapi/getuser_walllet/format/json/" ,new RequestWalletBalance(new RequestFieldsWallet(sessionManager.getUserId())));
        call.enqueue(new Callback<ResponceWalletBalance>() {

            @Override
            public void onResponse(Call<ResponceWalletBalance> call, Response<ResponceWalletBalance> response) {
                Log.d("aaaa",response.toString());
                if(response.body() != null ) {
                    if ( response.body().getResponseCode() == 410 ) {
                        Toast.makeText(ConfirmOutstation.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if ( response.body().getResponseCode() == 400 ) {
                        if (response.body().getDataArray() != null) {
                            WalletDataArray dataArray = response.body().getDataArray();
                            sessionManager.saveWalletBalance(dataArray.getWalletamt());
                        }
                    }
                } else {
                    Toast.makeText(ConfirmOutstation.this, "Something went wrong please try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponceWalletBalance> call, Throwable t) {

            }
        });
    }

    private void showRatingDialog(final String amt) {

        dialog = new Dialog(ConfirmOutstation.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_wallet);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        dialog.findViewById(R.id.id_network_ok).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent(ConfirmOutstation.this, CCAvenue.class);
                        intent.putExtra("amount", amt);
                        startActivity(intent);
                    }
                });

        dialog.show();
    }


    private void confirmBooking() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface request = retrofit.create( RequestInterface.class );

        Call<ConfirmIntercityResponceFields> call = request.confirmIntercityRequest(sessionManager.getAuthToken()
                ,"api/intercityapi/bookingconfirm/format/json/"
                ,new ConfirmIntercityRequestFields(new ConfirmIntercityRequestData(sessionManager.getUserId()
                ,pickUp
                ,toCity.getText().toString()
                ,estimatedFare
                ,""
                ,CommonMethods.convertDateUnixToStringFormat(Long.parseLong(pickUp_Time),"yyyy-MM-dd HH:mm")
                ,paymentMode.getText().toString().toLowerCase()
                ,couponId
                ,""
                ,sessionManager.getUserMob()
                ,sessionManager.getUserName()
                ,pickupCityId
                ,dropCityId
                ,trip_Type
                ,sessionManager.getPickupMarkerLat()
                ,sessionManager.getPickupMarkerLng()
                ,categoryType
                ))
        );

        call.enqueue(new Callback<ConfirmIntercityResponceFields>() {

            @Override
            public void onResponse(Call<ConfirmIntercityResponceFields> call, Response<ConfirmIntercityResponceFields> response) {
                Log.d("aaaa", response.toString());

                if ( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(ConfirmOutstation.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if ( response.body().getResponseCode() == 400 ) {
                        String rideId=response.body().getDataArray().getRideid();
                        Intent i=new Intent(ConfirmOutstation.this,WaitingScreenIntercity.class);
                        i.putExtra("RIDEID",rideId);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfirmIntercityResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
            }
        });
    }

    private void onClickApplyCoupon() {
        Intent pickCoupon = new Intent(ConfirmOutstation.this, CouponActivity.class);
        pickCoupon.putExtra(Constants.TRIPTYPE,"1");
        pickCoupon.putExtra(Constants.VEHICLE_TYPE,categoryType);
        pickCoupon.putExtra(Constants.COUPON_SOURCE,"outstation");
        startActivityForResult ( pickCoupon, Constants.PICK_COUPONS);
    }

    private void onClickPaymentMode() {
        Intent payment = new Intent(ConfirmOutstation.this, PaymentActivity.class);
        startActivityForResult ( payment, Constants.PAYMENT_CODE );
    }

    private void getPickupAddress() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setCountry("IN")
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);


        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                confirmPickUpLocationText.setText(place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Toast.makeText(this, "No place found please try again", Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == Constants.PAYMENT_CODE) {
            // Make sure the request was successful
            String paymentType=data.getStringExtra(Constants.PAYMENT_TYPE);
            paymentMode.setText(paymentType);
        } else if (resultCode == Constants.PICK_COUPONS) {
            // Make sure the request was successful
            applyCoupon.setText(data.getStringExtra(Constants.COUPON_NAME));
            data.getStringExtra(Constants.COUPON_VALUE);
            couponId=data.getStringExtra(Constants.COUPON_ID);

        }
    }
}