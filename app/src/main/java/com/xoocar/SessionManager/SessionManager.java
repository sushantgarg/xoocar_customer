package com.xoocar.SessionManager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sushant on 8/7/17.
 */

public class SessionManager {
    private static final String DESTINATIONDISTANCEMETERS = "destinationdistancemeters";
    private static final String DESTINATIONDURATION ="destinationduration" ;
    private static final String INTERNETAVAILABLE ="internetavailable";
    private static final String GEORADIUS = "georadius";
    private static final String TAXRATE = "TAXRATE";

    // Shared Preferences
        private SharedPreferences pref;

        // Editor for Shared preferences
        private SharedPreferences.Editor editor;

    // Constructor
        public SessionManager(Context context) {
            Context _context = context;
            int PRIVATE_MODE = 0;
            String PREF_NAME = "com.xoocar";
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }

    public void setDestinationDistanceMeters(int distance) {
        editor.putInt(DESTINATIONDISTANCEMETERS,distance);
        editor.commit();
    }

    public int getDestinationDistanceMeters(){
        return pref.getInt(DESTINATIONDISTANCEMETERS,0);
    }

    public void setDestinationDurationSeconds(int seconds) {
        editor.putInt(DESTINATIONDURATION,seconds);
        editor.commit();
    }

    public int getDestinationDurationSeconds() {
        return pref.getInt(DESTINATIONDURATION,0);
    }

    public void setInternetAvailable(boolean b) {
        editor.putBoolean(INTERNETAVAILABLE,b);
        editor.commit();
    }

    public boolean isInternetAvailable(){
        return pref.getBoolean(INTERNETAVAILABLE,true);
    }

    public void setCurrentLat(double latitude) {
        editor.putString("CURRENTLAT", String.valueOf(latitude));
        editor.commit();
    }

    public void setCurrentLng(double longitude) {
        editor.putString("CURRENTLNG", String.valueOf(longitude));
        editor.commit();
    }

    public String getCurrentLat() {
        return pref.getString("CURRENTLAT","0");
    }

    public String getCurrentLng() {
        return pref.getString("CURRENTLNG","0");
    }

    public void setPickupAddress(String sourceAddress) {
        editor.putString("CURRENTADDRESS",sourceAddress);
        editor.commit();
    }

    public String getPickupAddress(){
        return pref.getString("CURRENTADDRESS","");
    }

    public void setDestinationAddress(String sourceAddress) {
        editor.putString("DESTINATIONADDRESS",sourceAddress);
        editor.commit();
    }

    public String getDSestinationAddress(){
        return pref.getString("DESTINATIONADDRESS","");
    }

    public void setFcmToken(String refreshedToken) {
        editor.putString("FCMTOKEN",refreshedToken);
        editor.commit();
    }

    public String getFcmToken(){
        return pref.getString("FCMTOKEN","");
    }

    public void setDestLat(double latitude) {
        editor.putString("DEST_LAT", String.valueOf(latitude));
        editor.commit();
    }

    public void setDestLng(double longitude) {
        editor.putString("DEST_LNG", String.valueOf(longitude));
        editor.commit();
    }

    public String getDestLat() {
        return pref.getString("DEST_LAT","0");
    }

    public String getDestLng() {
        return pref.getString("DEST_LNG","0");
    }

    public String getUserId() {
        return pref.getString("USERID","8628");
    }

    public void saveUserId(String userId) {
        editor.putString("USERID",userId);
        editor.commit();
    }

    public void saveAuthToken(String authtoken) {
        editor.putString("AUTHTOKEN",authtoken);
        editor.commit();
    }

    public String getAuthToken(){
        return pref.getString("AUTHTOKEN","");
    }

    public void setIsLogedIn(boolean b) {
        editor.putBoolean("ISLOGGEDIN",b);
        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean("ISLOGGEDIN",false);
    }

    public String getRideId() {
        return pref.getString("RIDEID","0");
    }

    public void setRideId(String rideId) {
        editor.putString("RIDEID",rideId);
        editor.commit();
    }

    public void logout() {
        String token=getFcmToken();
        editor.clear();
        editor.commit();
        setFcmToken(token);
    }

    public void saveUserNAme(String firstName) {
        editor.putString("USERNAME",firstName);
        editor.commit();
    }

    public String getUserName(){
        return pref.getString("USERNAME","");
    }

    public void setFrndName(String name) {
        editor.putString("FRNDNAME",name);
        editor.commit();
    }

    public String getFrndName(){
        return pref.getString("FRNDNAME","");
    }

    public void setFrndNumber(String num) {
        editor.putString("FRNDNUMBER",num);
        editor.commit();
    }

    public String getFrndNumber(){
        return pref.getString("FRNDNUMBER","");
    }

    public void setIsValidPickupLocation(boolean b) {
        editor.putBoolean("ISVALIDPICKUP",b);
        editor.commit();
    }

    public boolean isValidPickup(){
        return pref.getBoolean("ISVALIDPICKUP",true);
    }

    public void setIsValidDestination(boolean b) {
        editor.putBoolean("ISVALIDDESTINATION",b);
        editor.commit();
    }

    public boolean isValidDestination(){
        return pref.getBoolean("ISVALIDDESTINATION",true);
    }

    public void saveUserNumber(String mobNum) {
        editor.putString("USERMOBILENUM",mobNum);
        editor.commit();
    }

    public String getUserMob(){
        return pref.getString("USERMOBILENUM","");
    }

    public void setPickupMarkerLat(double latitude) {
        editor.putString("PICKUPMARKERLAT", String.valueOf(latitude));
        editor.commit();
    }

    public String getPickupMarkerLat(){
        return pref.getString("PICKUPMARKERLAT","0");
    }

    public void setPickupMarkerLng(double longitude) {
        editor.putString("PICKUPMARKERLNG", String.valueOf(longitude));
        editor.commit();
    }

    public String getPickupMarkerLng(){
        return pref.getString("PICKUPMARKERLNG","0");
    }

    public void saveWalletBalance(String i) {
        editor.putString("WALLET_BALANCE_NEW",i);
        editor.commit();
    }

    public String getWalletBalance() {
        return pref.getString("WALLET_BALANCE_NEW","0");
    }

    public String getCityId() {
        return pref.getString("CITYID","");
    }

    public void setCityId(String city){
        editor.putString("CITYID",city);
        editor.commit();
    }

    public boolean showOffer() {
        return pref.getBoolean("ISFIRST",true);
    }

    public void setShowOffer(boolean value ) {
        editor.putBoolean("ISFIRST",value);
        editor.commit();
    }

    public double getGeoRadius() {
        return Double.parseDouble(pref.getString(GEORADIUS,"2.5"));
    }

    public void setGeoradius(String radius){
        editor.putString(GEORADIUS,radius);
        editor.commit();
    }

    public int getBannerId() {
        return pref.getInt("BANNER_ID",0);
    }
    public void setBannerId(int id){
        editor.putInt("BANNER_ID",id);
        editor.commit();
    }

    public float getTaxRate() {
        return pref.getFloat(TAXRATE, 5f);
    }

    public void setTaxRate(float taxRate) {
        editor.putFloat(TAXRATE, taxRate);
        editor.commit();
    }

    public void setCancellationFee(int charge){
        editor.putInt("CANCELLATION_FEE",charge);
        editor.commit();
    }

    public int getCancellationCharge() {
        return pref.getInt("CANCELLATION_FEE",60);
    }
}
