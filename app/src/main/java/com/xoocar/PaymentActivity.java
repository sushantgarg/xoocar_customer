package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by sushant on 10/8/17.
 */

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        LinearLayout linearCash = (LinearLayout) findViewById(R.id.paymentLinearLayout1);
        LinearLayout linearWallet = (LinearLayout) findViewById(R.id.paymentLinearLayout2);
        linearCash.setOnClickListener(this);
        linearWallet.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.paymentLinearLayout1:
                Intent i=new Intent();
                i.putExtra(Constants.PAYMENT_TYPE,"Cash");
                setResult(Constants.PAYMENT_CODE,i);
                finish();
                break;
            case R.id.paymentLinearLayout2:
                Intent intent =new Intent();
                intent.putExtra(Constants.PAYMENT_TYPE,"Wallet");
                setResult(Constants.PAYMENT_CODE,intent);
                finish();
                break;


        }

    }

}
