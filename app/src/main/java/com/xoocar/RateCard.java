package com.xoocar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Requests.GetCategoyRateCard.GetCategoryRateCardRequestFields;
import com.xoocar.Requests.GetCategoyRateCard.GetCategoryRateCardResponceData;
import com.xoocar.Requests.GetCategoyRateCard.GetCategoryRateCardResponceFields;
import com.xoocar.Requests.GetCityId.GetCityIdRequestFields;
import com.xoocar.Requests.GetCityId.GetCityIdResponceData;
import com.xoocar.Requests.GetCityId.GetCityIdResponceFields;
import com.xoocar.Requests.GetRateCard.GetRateCardRequestData;
import com.xoocar.Requests.GetRateCard.GetRateCardRequestFields;
import com.xoocar.Requests.GetRateCard.GetRateCardResponceData;
import com.xoocar.Requests.GetRateCard.GetRateCardResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 14/8/17.
 */

public class RateCard extends AppCompatActivity {
    private TextView textViewBaseFare,firstRangeCharge,secRangeCharge,perMinCharge;

    private SessionManager sessionManager;
    private Spinner citySpinner,categorySpinner;
    private TextView category,city,firstRange,secRange;
    private List<GetCityIdResponceData> cities;
    private List<GetCategoryRateCardResponceData> categories;
    private int cityPos=0,catPos=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_card);
        sessionManager=new SessionManager(this);

        textViewBaseFare=(TextView) findViewById(R.id.textViewBaseFare);
        firstRangeCharge=(TextView) findViewById(R.id.firstRangeCharge);
        secRangeCharge=(TextView)findViewById(R.id.secRangeCharge) ;
        perMinCharge=(TextView) findViewById(R.id.perMinCharge);

        citySpinner = (Spinner) findViewById(R.id.rate_card_city_sp);
        categorySpinner = (Spinner) findViewById(R.id.rate_card_city_sp2);

        category= (TextView) findViewById(R.id.textViewCategory);
        city= (TextView) findViewById(R.id.cityTextView);

        firstRange= (TextView) findViewById(R.id.firstRange);
        secRange= (TextView) findViewById(R.id.secondRange);

        getCities();
        getCategory();

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityPos=i;

                if(categories != null && cities != null ) {
                    getRateCard();
                    city.setText(cities.get(i).gettCityName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                catPos=i;

                if(categories != null && cities != null ) {
                    getRateCard();
                    category.setText(categories.get(i).getName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getRateCard() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<GetRateCardResponceFields> call = request.getRateCard(sessionManager.getAuthToken(),"api/processapi/ratelist/format/json/"
                ,new GetRateCardRequestFields(new GetRateCardRequestData("ratelist"
                        ,String.valueOf(cities.get(cityPos).getaCityId())
                        ,String.valueOf(categories.get(catPos).getId())
                        ,"2"
                )));
        call.enqueue(new Callback<GetRateCardResponceFields>() {

            @Override
            public void onResponse(Call<GetRateCardResponceFields> call, Response<GetRateCardResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RateCard.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        GetRateCardResponceData categoryPrice=response.body().getDataArray().get(0);

                        String[] range1=categoryPrice.getRangefrom().split(",");
                        String[] range2=categoryPrice.getRangeto().split(",");
                        String[] price=categoryPrice.getCharge().split(",");
                        int rangeValue1=0;
                        int rangeValue2=0;

                        if( range1.length > 1) {
                            rangeValue1 = Integer.parseInt(range1[1]);
                        }
                        if(range2.length > 1) {
                            rangeValue2 = Integer.parseInt(range2[1]);
                        }

                        int charge1= Integer.parseInt(price[0]);
                        int charge2=0;

                        if( price.length > 1 ) {
                         charge2 = Integer.parseInt(price[1]);
                         secRangeCharge.setText(""+charge2);
                        }

                        textViewBaseFare.setText(categoryPrice.getPcBasefare());
                        firstRangeCharge.setText(""+charge1);
                        perMinCharge.setText(categoryPrice.getPcRidechargepermin());

                        firstRange.setText("0-"+rangeValue1+" Km");
                        secRange.setText(rangeValue1+"-"+rangeValue2+" Km");

                    } else {
                        Toast.makeText(RateCard.this, "Price list not available ", Toast.LENGTH_SHORT).show();
                        textViewBaseFare.setText("");
                        firstRangeCharge.setText("");
                        secRangeCharge.setText("");
                        perMinCharge.setText("");
                    }
                } else {
                    Toast.makeText(RateCard.this, "Price list not available ", Toast.LENGTH_SHORT).show();
                    textViewBaseFare.setText("");
                    firstRangeCharge.setText("");
                    secRangeCharge.setText("");
                    perMinCharge.setText("");
                }
            }

            @Override
            public void onFailure(Call<GetRateCardResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    private void getCities() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<GetCityIdResponceFields> call = request.getCities(sessionManager.getAuthToken(),"api/processapi/getallcity/format/json/"
                ,new GetCityIdRequestFields());
        call.enqueue(new Callback<GetCityIdResponceFields>() {

            @Override
            public void onResponse(Call<GetCityIdResponceFields> call, Response<GetCityIdResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RateCard.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {

                        List<String> cityString=new ArrayList<String>();
                        cities=response.body().getDataArray();
                        for (GetCityIdResponceData city:cities) {
                            cityString.add(city.gettCityName());
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>( RateCard.this
                                , android.R.layout.simple_spinner_item
                                , cityString );

                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        citySpinner.setAdapter(dataAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<GetCityIdResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    private void getCategory() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<GetCategoryRateCardResponceFields> call = request.getCategoryRateCard(sessionManager.getAuthToken(),"api/processapi/getcabcat/format/json/"
                ,new GetCategoryRateCardRequestFields());
        call.enqueue(new Callback<GetCategoryRateCardResponceFields>() {

            @Override
            public void onResponse(Call<GetCategoryRateCardResponceFields> call, Response<GetCategoryRateCardResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(RateCard.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {

                        List<String> cityString=new ArrayList<String>();
                        categories=response.body().getDataArray();
                        for (GetCategoryRateCardResponceData city:categories) {
                            cityString.add(city.getName());
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>( RateCard.this
                                , android.R.layout.simple_spinner_item
                                , cityString );

                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        category.setText(categories.get(0).getName());
                        categorySpinner.setAdapter(dataAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<GetCategoryRateCardResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

}
