package com.xoocar;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Adapter.RideHistoryPagerAdapter;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.RideHistory.RideHIstoryRequestFields;
import com.xoocar.Requests.RideHistory.RideHIstoryResponceFields;
import com.xoocar.Requests.RideHistory.RideHistoryRequestData;
import com.xoocar.Requests.RideHistory.RideHistoryResponceData;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 9/8/17.
 */

public class MyRidesActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;
    private TextView rideNum,rideTag;
    private boolean isAlive;
    private LinearLayout runningLayout,scheduleLayout,cancelLayout,completeLayout,pendingLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ride_completed);
        sessionManager=new SessionManager(this);

        fragmentManager = getSupportFragmentManager();

        mViewPager = findViewById(R.id.completepager);
        rideNum = findViewById(R.id.rideNumRideHistory);
        rideTag = findViewById(R.id.rideTagRideHistory);

        runningLayout= findViewById(R.id.runningLayout);
        scheduleLayout=  findViewById(R.id.scheduleLayout);
        cancelLayout= findViewById(R.id.cancelLayout);
        completeLayout= findViewById(R.id.completedLayout);
        pendingLayout= findViewById(R.id.pendingLayout);

        RideHistoryPagerAdapter adapter = new RideHistoryPagerAdapter(fragmentManager,5);
        mViewPager.setAdapter(adapter);
        isAlive=true;

        completeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });

        runningLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });

        scheduleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(2);
            }
        });

        cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(3);
            }
        });

        pendingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(4);
            }
        });

        mViewPager.setOffscreenPageLimit(4);

        getRideHistory();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                tabLayout.setScrollPosition(position,positionOffset,true);
                switch (position) {
                    case 0:
                        rideTag.setText("Rides Completed");
                        completeLayout.setBackgroundColor(Color.parseColor("#878787"));

                        runningLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        cancelLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        scheduleLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        pendingLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));

                        rideNum.setText(""+getRideNum("Completed"));
                        break;

                    case 1:
                        rideTag.setText("Rides Running");
                        runningLayout.setBackgroundColor(Color.parseColor("#878787"));

                        scheduleLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        cancelLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        completeLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        pendingLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        rideNum.setText(""+getRideNum("Running"));
                        break;

                    case 2:
                        rideTag.setText("Rides Scheduled");
                        scheduleLayout.setBackgroundColor(Color.parseColor("#878787"));

                        runningLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        cancelLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        completeLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        pendingLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        rideNum.setText(""+getRideNum("Schedule"));
                        break;

                    case 3:
                        rideTag.setText("Rides Cancelled");
                        cancelLayout.setBackgroundColor(Color.parseColor("#878787"));

                        runningLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        scheduleLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        completeLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        pendingLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        rideNum.setText(""+getRideNum("Cancelled"));

                        break;


                    case 4:
                        rideTag.setText("Pending Rides");
                        pendingLayout.setBackgroundColor(Color.parseColor("#878787"));

                        runningLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        scheduleLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        completeLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        cancelLayout.setBackgroundColor(Color.parseColor("#2e2e2e"));
                        rideNum.setText(""+getRideNum("Pending"));

                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private int getRideNum(String status) {
        return  RealmManager.getRideHistoryByCategory(status).size();
    }

    private void getRideHistory() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<RideHIstoryResponceFields> call = request.rideHistory(sessionManager.getAuthToken()
                ,"api/newcustomersapi/customerridehistory/format/json/"
                , new RideHIstoryRequestFields(new RideHistoryRequestData("customerridehistory"
                        ,sessionManager.getUserId())));

        call.enqueue(new Callback<RideHIstoryResponceFields>() {
            @Override
            public void onResponse(Call<RideHIstoryResponceFields> call, Response<RideHIstoryResponceFields> response) {
                if( response.body() != null && isAlive && !isFinishing()) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(MyRidesActivity.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body()!= null ) {
                            List<RideHistoryResponceData> RideLIst=response.body().getDataArray();
                            RealmManager.addRideHistory(RideLIst);
                            RideHistoryPagerAdapter adapter = new RideHistoryPagerAdapter(fragmentManager,5);
                            if( mViewPager != null ) {
                                mViewPager.setAdapter(adapter);
                            }
                            Log.d("aaaa",response.body().toString());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RideHIstoryResponceFields> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAlive=false;
    }
}
