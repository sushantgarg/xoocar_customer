package com.xoocar;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by sushant on 9/23/17.
 */

public class MarkerHash {
    String userId;
    Marker marker;

    public MarkerHash(String userId, Marker marker) {
        this.userId = userId;
        this.marker = marker;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
