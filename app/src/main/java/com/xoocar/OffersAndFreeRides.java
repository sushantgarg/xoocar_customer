package com.xoocar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.xoocar.Adapter.OffersAdapter;
import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.Requests.GetCoupons.CouponResponceFields;
import com.xoocar.Requests.GetCoupons.GetCouponRequest;
import com.xoocar.Requests.GetCoupons.GetCoupons;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 14/8/17.
 */

public class OffersAndFreeRides extends AppCompatActivity implements AdapterToMain {

    private Context ctx;
    private RecyclerView couponList;
    private OffersAdapter adapter;
    private ArrayList<ResponseDataArray> couponsList;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_rides);

        sessionManager=new SessionManager(this);
        ctx=OffersAndFreeRides.this;

        getCoupons();

        couponList = findViewById(R.id.offers);
        couponList.setLayoutManager(new LinearLayoutManager(ctx));
    }


    public void getCoupons() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.base)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CouponResponceFields> call = request.getCoupons(sessionManager.getAuthToken(),"api/newloginapi/sharecopon/format/json/"
                ,new GetCoupons(new GetCouponRequest("sharecopon",sessionManager.getUserId(),sessionManager.getCityId(),"")));
        call.enqueue(new Callback<CouponResponceFields>() {

            @Override
            public void onResponse(Call<CouponResponceFields> call, Response<CouponResponceFields> response) {
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(OffersAndFreeRides.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body().getDataArray() != null && response.body().getDataArray().size() > 0) {
                            couponsList = (ArrayList<ResponseDataArray>) response.body().getDataArray();

                            ArrayList<ResponseDataArray> todaysCoupon=new ArrayList<ResponseDataArray>();
                            for (ResponseDataArray coupon:couponsList
                                    ) {
                                if(coupon.getTag().equals("all")){
                                    todaysCoupon.add(coupon);
                                }
                            }
                            adapter = new OffersAdapter(todaysCoupon, ctx);
                            couponList.setAdapter(adapter);
                        }
                    }
                } else {
                    Toast.makeText(OffersAndFreeRides.this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CouponResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    @Override
    public void addCoupon(ResponseDataArray coupon) {
        Intent i =new Intent( OffersAndFreeRides.this,WebViewClass.class );
        i.putExtra(Constants.WEB_VIEW_CODE,"4");
        startActivity( i);
    }

    @Override
    public void rideDetail(CabDetailHopNGo cab) {

    }
}
