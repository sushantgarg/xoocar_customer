package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.xoocar.SessionManager.SessionManager;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sushant on 10/11/17.
 */

public class WaitingScreenIntercity extends AppCompatActivity {

    private CountDownTimer timercontDown;
    private String rideId;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_intercity);
        sessionManager=new SessionManager(this);
        Intent i=getIntent();
        rideId=i.getStringExtra("RIDEID");
        TextView tv=findViewById(R.id.bookingConfirmText);
        String text="Your booking is confirmed with us. Ride details will be shared 1 hour before pickup time.";
        tv.setText(text);
        timeShow();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        timercontDown.cancel();

        Intent i=new Intent(WaitingScreenIntercity.this,MainActivity.class);
        startActivity(i);
    }

    private void timeShow() {
        try {
            timercontDown = new CountDownTimer(5*1000, 1000) {

                public void onTick( long millisUntilFinished ) {
                }
                public void onFinish() {
                    Intent i=new Intent(WaitingScreenIntercity.this,MainActivity.class);
                    startActivity(i);
                }
            }.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
