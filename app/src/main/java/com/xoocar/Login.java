package com.xoocar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.xoocar.Requests.CheckUserExist.CheckUserExistsRequestData;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsRequestFields;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsResponceData;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/8/17.
 */

public class Login extends AppCompatActivity {
    private EditText phoneNumLogin;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    ProgressDialog pd;
    LinearLayout loginGo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkPermissions();

        phoneNumLogin = (EditText) findViewById(R.id.phoneNumLogin);
        loginGo = (LinearLayout) findViewById(R.id.loginImg);

        if (new SessionManager(Login.this).isLoggedIn()) {
                                Intent i = new Intent(Login.this, MainActivity.class);
                                startActivity(i);
                            } else {
                                loginGo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        callLogin();
                                    }
                                });

                                phoneNumLogin.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                        if(charSequence.length() == 10){
                                            callLogin();
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable editable) {
                                    }
                                });
                            }
    }

    private void callLogin() {
        if( CommonMethods.isValidPhone(phoneNumLogin.getText().toString()) ) {
            pd = new ProgressDialog(Login.this);
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
            sendOtp(phoneNumLogin.getText().toString());
        } else {
            Toast.makeText(Login.this, "Not Valid Number", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendOtp(String num) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CheckUserExistsResponceFields> call = request.isUserExist("api/newloginapi/check_customerotp/format/json/"
                ,new CheckUserExistsRequestFields(new CheckUserExistsRequestData(num)));

        call.enqueue(new Callback<CheckUserExistsResponceFields>() {
            @Override
            public void onResponse(Call<CheckUserExistsResponceFields> call, Response<CheckUserExistsResponceFields> response) {

                if( response.body() != null  ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( Login.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            CheckUserExistsResponceData userStatus= response.body().getDataArray();

                            Intent i=new Intent(Login.this,OTP.class);
                            i.putExtra(Constants.OTP,userStatus.getOtp());
                            i.putExtra(Constants.IS_USER_EXISTS,userStatus.getStatus());
                            i.putExtra((Constants.MOB_NUM),phoneNumLogin.getText().toString());
                            startActivity(i);
                        }
                    } else {
                        Toast.makeText(Login.this, "Some error occured please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CheckUserExistsResponceFields> call, Throwable t) {
                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }
            }
        });
    }

    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(Login.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(Login.this,
                        new String[]{android.Manifest.permission.CALL_PHONE
                                , android.Manifest.permission.READ_CONTACTS
                                , android.Manifest.permission.READ_SMS
                                , Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
//                   Todo: enable gps

                } else {
                    Toast.makeText(this, "XooCar will not be able to fetch your pickup location", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lianes to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
