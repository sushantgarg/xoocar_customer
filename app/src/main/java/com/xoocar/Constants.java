package com.xoocar;

/**
 * Created by sushant on 8/3/17.
 */

public class Constants {
    public static final String HOPNGO_CAB_DETAIL = "hopngo_cab_detail";
    public static final String COUPON_ID = "coupon_id";
    public static final String PAYMENT_MODE = "payment_mode";
    public static final String CAB_BY_ID = "cab_by_id";
    public static final String PAYMENT_TYPE ="payment_type" ;
    public static final int PAYMENT_CODE = 8;
    public static final String CC_AVENUE_URL = "http://www.xoocar.com/webapp/ccpayment/paymentgateway?";
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String RIDE_DETAIL = "ride_detail";
    public static final String RIDE_ESTIMATE = "ride_estimate";
    public static final String ETA = "eta";
    public static final String RIDE_ID = "rideId";
    public static final String CAB_TYPE = "cab_type";
    public static final String TOTALESTIMATEDFARE = "totalestimatedfare";
    public static final String FROMCITY = "fromCity";
    public static final String TOCITY = "toCity";
    public static final String PICKUPLOCATIONIMG = "pickUpLocationImg";
    public static final String TRIPTYPE = "tripType";
    public static final String TRAVELDATE = "travelDate";
    public static final String PICKUPTIME = "pickUpTime";
    static final int PICK_RIDE_HOPNGO = 6;
    static final String ACCEPTED_BY_DRIVER = "3";
    static final String REJECTED_BY_DRIVER = "15";
    static final String REJECTED_AFTER_SCHEDULE = "31";
    static final String OTP = "otp";
    static final String IS_USER_EXISTS ="is_user_exista" ;
    static final String MOB_NUM ="mob_num" ;
    static final String RIDE_COMPLETED = "6";
    static final String WEB_VIEW_CODE = "web_view_code";
    static final String SHARE_LINK = "webapp/webapp/mapdraw/";
    static final String RUNNING = "5";
    static final int PICK_CONTACT = 14;
    static final String GENERAL_NOTIFICATION = "35";
    public static final String CREATED_ON = "created_on";
    public static final String DRIVER_IDS = "driver_ids";
    public static final String DISTANCE ="distance" ;
    public static final String PER_KM_CHARGE = "per_km_charge";
    public static final String CATEGORY_TYPE ="category_type" ;
    public static final String DRIVER_PER_DAY_CHARGE ="driver_per_day_charge" ;
    public static final String NUM_OF_DAYS ="num_of_days" ;
    public static final String PICKUP_CITY_ID ="pickup_city_id" ;
    public static final String DROP_CITY_ID ="drop_city_id" ;
    public static final String COUPON_SOURCE = "coupon_source";
    public static String base = "http://velenesa.com/vele/index.php/";
    public static int PICK_COUPONS = 4;
    static String pathJsonDelhi ="{\"coordinates\": [[76.629638671875,28.899992721104496],[ 76.959228515625,28.26084411480482],[  77.8436279296875 , 28.393816571287864 ] , [ 77.67333984375 , 28.97931203672246 ] ,[ 76.629638671875 , 28.899992721104496 ] ] }";
    static String pathJsonMumbai="{\"coordinates\":[[72.7130126953125,20.135891626114574],[72.80914306640625,20.1333128972237]\n" +
            "            ,[72.90252685546875,20.210656234489853]\n" +
            "            ,[73.06732177734375,20.050771131126798]\n" +
            "            ,[73.597412109375,19.665866618043196]\n" +
            "            ,[73.795166015625,19.331878440818787]\n" +
            "            ,[73.883056640625,18.81791748264768]\n" +
            "            ,[73.5589599609375,18.505656663040547]\n" +
            "            ,[73.707275390625,18.205871127330347]\n" +
            "            ,[73.62762451171875,17.628317078494]\n" +
            "            ,[73.17718505859375,17.57857571874489]\n" +
            "            ,[72.82012939453125,18.596792049367412]\n" +
            "            ,[72.66357421875,19.691728302992512]\n" +
            "            ,[72.630615234375,19.862310448664577]\n" +
            "            ,[72.70477294921874,20.125576455270572]] }";
    static String COUPON_NAME="coupon_name";
    static String COUPON_VALUE="coupon_value";

}
