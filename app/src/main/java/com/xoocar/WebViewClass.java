package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import java.net.URL;

/**
 * Created by sushant on 14/8/17.
 */

public class WebViewClass extends AppCompatActivity {

    private WebView webView;
    URL url;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        Intent i=getIntent();
        String source=i.getStringExtra(Constants.WEB_VIEW_CODE);

        webView=(WebView) findViewById(R.id.webView1);

        switch (source) {
            case "1":
                webView.loadUrl("http://velenesa.com/vele/index.php/webapp/webapp/aboutus");
                break;
            case "2":
                webView.loadUrl("http://velenesa.com/vele/index.php/webapp/webapp/policies");
                break;
            case "3":
                webView.loadUrl("http://velenesa.com/vele/index.php/webapp/webapp/terms");
                break;
            case "4":
                webView.loadUrl("http://velenesa.com/vele/index.php/webapp/webapp/offerdeals");
                break;

        }
    }
}
