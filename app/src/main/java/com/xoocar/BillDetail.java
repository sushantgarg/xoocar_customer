package com.xoocar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Requests.BillDetail.BillDetailRequestFields;
import com.xoocar.Requests.BillDetail.BillDetailsRequestData;
import com.xoocar.Requests.BillDetail.BillDetailsResponceData;
import com.xoocar.Requests.BillDetail.BillDetailsResponceFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingRequestData;
import com.xoocar.Requests.SubmitRating.SubmitRatingRequetFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingResponce;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 16/8/17.
 */

public class BillDetail extends AppCompatActivity {
    private SessionManager sessionManager;
    private TextView totalFare,toll,coupon,netPayment,cashPaid,walletPaid,pickupAddress,dropAddress,totalAmt;
    private Dialog dialog;
    private BillDetailsResponceData billData;
    private ProgressDialog pd;
    private String rideId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail);
        sessionManager=new SessionManager(this);

        Intent i=getIntent();
        rideId=i.getStringExtra(Constants.RIDE_ID);

        getBillDetail();

        totalFare= findViewById(R.id.totalfare);
        toll=  findViewById(R.id.toll);
        coupon=  findViewById(R.id.couponDiscount);
        netPayment= findViewById(R.id.netPayment);
        cashPaid= findViewById(R.id.cashPaid);
        walletPaid=  findViewById(R.id.walletPaid);
        pickupAddress= findViewById(R.id.pickupAddress);
        dropAddress= findViewById(R.id.dropAddress);
        totalAmt=  findViewById(R.id.rideAmtBill);
        Button done = findViewById(R.id.doneBtn);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( billData != null) {
                    showRatingDialog();
                }else{
                    getBillDetail();
                }
            }
        });
    }

    private void showRatingDialog() {
        dialog = new Dialog(BillDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rating_screen);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        final EditText comment;
        final RatingBar ratingBar;
        final TextView driverName,vehicleName,amount,pickup,destination;

        comment=dialog.findViewById(R.id.comment);
        ratingBar=dialog.findViewById(R.id.ratingBar);

        driverName=dialog.findViewById(R.id.driverNameRating);
        vehicleName=dialog.findViewById(R.id.vehicleNumberRating);
        amount=dialog.findViewById(R.id.amountPaidrating);
        pickup=dialog.findViewById(R.id.source_address_rating);
        destination=dialog.findViewById(R.id.destination_address_rating);

        driverName.setText(billData.getFirst_name());
        vehicleName.setText(billData.getV_licence_no());
        amount.setText(getString(R.string.rupee_symbol)+" "+ billData.getTotal());
        pickup.setText(billData.getSource());
        destination.setText(billData.getDestination());


        dialog.findViewById(R.id.submitRating).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        submitRating(ratingBar.getRating(),comment.getText().toString());

                    }
                });

        dialog.show();
    }

    private void submitRating(final float rating,final String s) {
        dialog.dismiss();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<SubmitRatingResponce> call = request.submitrating(sessionManager.getAuthToken()
                ,"api/processapi/insertrating/format/json/"
                ,new SubmitRatingRequetFields(new SubmitRatingRequestData(rideId
                        ,String.valueOf(rating),s)));
        call.enqueue(new Callback<SubmitRatingResponce>() {

            @Override
            public void onResponse(Call<SubmitRatingResponce> call, Response<SubmitRatingResponce> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(BillDetail.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        sessionManager.setFrndName("");
                        sessionManager.setFrndNumber("");
                        sessionManager.saveWalletBalance(response.body().getDataArray().getWalletinfo());

                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitRatingResponce> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });

    }

    private void getBillDetail() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        pd=new ProgressDialog(this);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Loading...");
        pd.show();

        Call<BillDetailsResponceFields> call = request.getBillDetail(sessionManager.getAuthToken()
                ,"api/processapi/tripsummry/format/json/"
                ,new BillDetailRequestFields(new BillDetailsRequestData("tripsummry",sessionManager.getUserId(),rideId)));
        call.enqueue(new Callback<BillDetailsResponceFields>() {

            @Override
            public void onResponse(Call<BillDetailsResponceFields> call, Response<BillDetailsResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( pd != null && pd.isShowing() ) {
                    pd.dismiss();
                }
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(BillDetail.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        billData=response.body().getDataArray();
                        double total=Double.parseDouble(billData.getNet()) + Double.parseDouble(billData.getTaxcharge());
                        totalFare.setText(getString(R.string.rupee_symbol)+" " + total );
                        toll.setText(getString(R.string.rupee_symbol)+" "+billData.getTolltax());
                        coupon.setText(getString(R.string.rupee_symbol)+" "+billData.getCoupondiscount());
                        netPayment.setText(getString(R.string.rupee_symbol)+" "+billData.getTotal());

                        String cash=billData.getCashPaid();
                            if( cash.equals("") ){
                                cash="0";
                            }
                        cashPaid.setText(getString(R.string.rupee_symbol)+" "+ cash);

                        String wallet=billData.getWalletPaid();
                        if( wallet.equals("") ){
                            wallet="0";
                        }
                        walletPaid.setText(getString(R.string.rupee_symbol)+" "+ wallet);

                        pickupAddress.setText(billData.getSource());
                        dropAddress.setText(billData.getDestination());

                        totalAmt.setText(getString(R.string.rupee_symbol)+" "+ billData.getTotal());
                    }
                }
            }

            @Override
            public void onFailure(Call<BillDetailsResponceFields> call, Throwable t) {
                if( pd != null && pd.isShowing() ) {
                    pd.dismiss();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent( BillDetail.this, MainActivity.class );
        startActivity(i);
    }
}
