package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.CCAvenue.CCAvenue;
import com.xoocar.Requests.WalletBalanceRequest.RequestFieldsWallet;
import com.xoocar.Requests.WalletBalanceRequest.RequestWalletBalance;
import com.xoocar.Requests.WalletBalanceRequest.ResponceWalletBalance;
import com.xoocar.Requests.WalletBalanceRequest.WalletDataArray;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 11/8/17.
 */

public class MyWallet extends AppCompatActivity implements View.OnClickListener {
    private TextView xooCashPromo;
    private TextView walletBalance;
    private TextView totalBalance;
    private TextView usageRuleDesc;
    private EditText enterAmount;
    private WalletDataArray dataArray;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mywallet);

        sessionManager=new SessionManager(this);
        xooCashPromo = findViewById(R.id.xooCashPromo);
        walletBalance = findViewById(R.id.walletBalance);
        totalBalance =  findViewById(R.id.totalBalance);
        enterAmount = findViewById(R.id.enterAmount);
        TextView inputAmount1 = findViewById(R.id.inputAmount1);
        TextView inputAmount2 = findViewById(R.id.inputAmount2);
        TextView inputAmount3 = findViewById(R.id.inputAmount3);
        usageRuleDesc=findViewById(R.id.usageRuleDesc);
        Button addMoneyButton = findViewById(R.id.addMoneyButton);

        getWalletDetails();

        addMoneyButton.setOnClickListener(this);
        inputAmount1.setOnClickListener(this);
        inputAmount2.setOnClickListener(this);
        inputAmount3.setOnClickListener(this);


    }

    private void getWalletDetails() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ResponceWalletBalance> call = request.getWalletBalance(sessionManager.getAuthToken()
                ,"api/newwalletapi/getuser_walllet/format/json/" ,new RequestWalletBalance(new RequestFieldsWallet(sessionManager.getUserId())));
        call.enqueue(new Callback<ResponceWalletBalance>() {

            @Override
            public void onResponse(Call<ResponceWalletBalance> call, Response<ResponceWalletBalance> response) {
                Log.d("aaaa",response.toString());
                if(response.body() != null ) {
                    if ( response.body().getResponseCode() == 410 ) {
                        Toast.makeText(MyWallet.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if ( response.body().getResponseCode() == 400 ) {
                        if (response.body().getDataArray() != null) {
                            dataArray=response.body().getDataArray();
                            sessionManager.saveWalletBalance(dataArray.getWalletamt());
                            setDetails(dataArray);
                        }
                    }
                } else {
                    Toast.makeText(MyWallet.this, "Something went wrong please try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponceWalletBalance> call, Throwable t) {

            }
        });
    }

    private void setDetails(WalletDataArray dataArray) {
        double total=Double.parseDouble(dataArray.getPromowallet())+Double.parseDouble(dataArray.getWalletamt());
        xooCashPromo.setText(dataArray.getPromowallet());
        walletBalance.setText(dataArray.getWalletamt());
        totalBalance.setText("" + total);
        usageRuleDesc.setText(dataArray.getRules());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addMoneyButton:
                String amt=enterAmount.getText().toString().trim();
                if( amt.equals("") ) {
                    Toast.makeText(this, "Please enter valid amount", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MyWallet.this, CCAvenue.class);
                    intent.putExtra("amount", amt);
                    startActivity(intent);
                }
                break;

            case R.id.inputAmount1:
              enterAmount.setText("250");
                break;

            case R.id.inputAmount2:
                enterAmount.setText("500");
                break;

            case R.id.inputAmount3:
                enterAmount.setText("1000");
                break;
        }
    }
}
