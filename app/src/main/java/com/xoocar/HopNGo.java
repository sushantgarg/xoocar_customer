package com.xoocar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xoocar.Adapter.HopNGoAdapter;
import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;
import com.xoocar.Requests.HopNGo.RequestFieldsHopnGo;
import com.xoocar.Requests.HopNGo.RequestModelHopnGo;
import com.xoocar.Requests.HopNGo.ResponceHopnGo;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/8/17.
 */

public class HopNGo extends AppCompatActivity implements AdapterToMain{
    private RecyclerView hopnGoList;
    private LinearLayout noCabsImageHopActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hopngo);

        hopnGoList =  (RecyclerView) findViewById(R.id.hopngoList);
        hopnGoList.setLayoutManager(new LinearLayoutManager(HopNGo.this));
        noCabsImageHopActivity=findViewById(R.id.noCabsImageHopActivity);

        getCabsFromRealm();
    }

    private void getCabsFromRealm() {

        List<CabDetailHopNGo> cabList=new ArrayList<>();

        for (CabsRealm cab:
        RealmManager.getAllCabs()) {
            if( !cab.getCabType().equals("13")) {
                CabDetailHopNGo cabs = new CabDetailHopNGo(cab);
                cabList.add(cabs);
            }
        }

        if( cabList.size() > 0 ) {
            HopNGoAdapter adapter = new HopNGoAdapter(cabList, HopNGo.this);
            hopnGoList.setAdapter(adapter);
        } else {
            noCabsImageHopActivity.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void addCoupon(ResponseDataArray coupon) {
    }

    @Override
    public void rideDetail(CabDetailHopNGo cab) {
        Intent i=new Intent();
        i.putExtra(Constants.HOPNGO_CAB_DETAIL,new Gson().toJson(cab));
        setResult(Constants.PICK_RIDE_HOPNGO,i);
        finish();
    }
}
