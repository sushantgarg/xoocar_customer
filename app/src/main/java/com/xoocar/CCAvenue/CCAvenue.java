package com.xoocar.CCAvenue;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.xoocar.Constants;
import com.xoocar.R;
import com.xoocar.SessionManager.SessionManager;

/**
 * Created by sushant on 18/8/17.
 */

public class CCAvenue extends Activity {
    private WebView webView;
    private String id, amount;
    private ProgressDialog progressDialog;
    private SessionManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cc_avenue);
        getFromIntent();
        initView();
        listenerView();
//        TripSummary.trip_flag = true;
    }

    private void getFromIntent() {
        amount = getIntent().getStringExtra("amount");
        Log.d("dkp ccavenue amount", "" + amount);
    }

    public void initView() {
        prefManager = new SessionManager(CCAvenue.this);
        id = prefManager.getUserId();
//        id = "2";
        Log.d("userId", "" + id);

        webView = (WebView) findViewById(R.id.webView);

        progressDialog=new ProgressDialog(CCAvenue.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void listenerView() {
        String url = Constants.CC_AVENUE_URL + "id=" + id + "&amount=" + amount;
        Log.d("ccavenue url", "" + url);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.addJavascriptInterface(new CCAvenueJavaScriptInterface(), "cc");
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        webView.clearHistory();
        webView.clearFormData();
        webView.clearCache(true);

        /*@SuppressWarnings("unused")
        WebSettings settings = webView.getSettings();
        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }*/
        Log.d("ccavenue url", "" + url);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if( progressDialog!= null && progressDialog.isShowing() ) {
                    progressDialog.dismiss();
                }

                if (url.contains("paymentsuccess")) {
                    Log.d("Success", "successsss" + url);
                    Toast.makeText(CCAvenue.this, "Payment successfuly done", Toast.LENGTH_SHORT).show();
                    webView.destroy();
                    finish();

                } else if (url.contains("paymentfailure")) {
                    Log.d("failure", "failureeeee" + url);
                    Toast.makeText(CCAvenue.this, "Payment failed", Toast.LENGTH_SHORT).show();
                    webView.destroy();
                    finish();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    private final class CCAvenueJavaScriptInterface {
        CCAvenueJavaScriptInterface() {
        }

        @JavascriptInterface
        public void success(long id, final String paymentId) {
            CCAvenue.this.runOnUiThread(new Runnable() {
                public void run() {
                    //success
                    Log.e("Error", "aaaaaaaaaa");
                }
            });
        }

        @JavascriptInterface
        public void failure(long id, final String paymentId) {
            CCAvenue.this.runOnUiThread(new Runnable() {
                public void run() {
                    //Failed
                    Log.e("Error", "bbbbbbbbb");
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}