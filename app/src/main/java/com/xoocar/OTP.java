package com.xoocar;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Requests.CheckUserExist.CheckUserExistsRequestData;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsRequestFields;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsResponceData;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsResponceFields;
import com.xoocar.Requests.Login.LoginRequestData;
import com.xoocar.Requests.Login.LoginRequestFields;
import com.xoocar.Requests.Login.LoginResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION;

/**
 * Created by sushant on 10/8/17.
 */

public class OTP extends AppCompatActivity implements View.OnClickListener {
    private EditText otp1Edt,otp2Edt,otp3Edt,otp4Edt;
   private Button otpButton;
    private String otp,isUserExists;
    private String otp1,otp2,otp3,otp4,mobNum;
    private SessionManager sessionManager;
    ProgressDialog pd;
    private Test_Receiver smsReceiver;
    private TextView resendOtp;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        otp1Edt = (EditText) findViewById(R.id.editTextOtp1);
        otp2Edt = (EditText) findViewById(R.id.editTextOtp2);
        otp3Edt = (EditText) findViewById(R.id.editTextOtp3);
        otp4Edt = (EditText) findViewById(R.id.editTextOtp4);

        otpButton = (Button) findViewById(R.id.otpButton);

        resendOtp= (TextView) findViewById(R.id.resendOtp);

        otpButton.setOnClickListener(this);
        resendOtp.setOnClickListener(this);

        sessionManager=new SessionManager(this);

        Intent i=getIntent();
        otp = i.getStringExtra(Constants.OTP);
        isUserExists=i.getStringExtra(Constants.IS_USER_EXISTS);
        mobNum=i.getStringExtra(Constants.MOB_NUM);
        edit_Text_Focus_Listner();

        smsReceiver = new Test_Receiver();
// Generate a filter object
        IntentFilter intentFilter = new IntentFilter();
// Add filter an action
        intentFilter.addAction(SMS_RECEIVED_ACTION);
        registerReceiver(smsReceiver, intentFilter);

    }

    @Override
    public void onClick(View view) {

        if(pd == null){
            pd=new ProgressDialog(OTP.this);
            pd.setMessage("Verifying details...");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }


        switch ( view.getId() ) {
            case R.id.otpButton:
                getTextFromEditText();
                String otpRec = otp1 + otp2 + otp3 + otp4;
                Log.d("otp",otp);
                if( otp.equals( otpRec ) ){
                    if( isUserExists.equals("1") ) {
//                        call login api
                        login();
                    } else {
//                        call signup
                        Intent i=new Intent(getApplicationContext(),SignUp.class);
                        i.putExtra(Constants.MOB_NUM,mobNum);
                        startActivity(i);
                    }
                } else {
                    Toast.makeText(this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.resendOtp:
                sendOtp(mobNum);
                break;
        }
        if(pd!= null && pd.isShowing()){
            pd.dismiss();
        }
    }

    private void login() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<LoginResponceFields> call = request.login("api/newloginapi/customerlogin/format/json/"
                ,new LoginRequestFields(new LoginRequestData(mobNum,"Android",sessionManager.getFcmToken())));

        call.enqueue(new Callback<LoginResponceFields>() {
            @Override
            public void onResponse(Call<LoginResponceFields> call, Response<LoginResponceFields> response) {

                if( response.body() != null  ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( OTP.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            sessionManager.saveUserId(response.body().getDataArray().getUid());
                            sessionManager.saveAuthToken(response.body().getDataArray().getAuthtoken());
                            sessionManager.setIsLogedIn(true);
                            sessionManager.saveUserNAme(response.body().getDataArray().getFirstName());
                            sessionManager.saveUserNumber(mobNum);
                            sessionManager.saveWalletBalance(response.body().getDataArray().getWalletinfo());
                            Intent i=new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(i);
                        }
                    } else {
                        Toast.makeText(OTP.this, "Some error occured please try again", Toast.LENGTH_SHORT).show();
                    }
                }
                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginResponceFields> call, Throwable t) {
                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    public void edit_Text_Focus_Listner() {

        otp1Edt.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (otp1Edt.getText().toString().length() == 1) {
                    otp2Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
        });

        otp2Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (otp2Edt.getText().toString().length() == 1) {
                    otp3Edt.requestFocus();
                } else if (otp2Edt.getText().toString().length() == 0) {
                    otp1Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        otp3Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (otp3Edt.getText().toString().length() == 1) {
                    otp4Edt.requestFocus();
                } else if (otp3Edt.getText().toString().length() == 0) {
                    otp2Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });


        otp4Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (otp4Edt.getText().toString().length() == 0) {
                    otp3Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });
    }

    public void getTextFromEditText() {
        otp1 = otp1Edt.getText().toString();
        otp2 = otp2Edt.getText().toString();
        otp3 = otp3Edt.getText().toString();
        otp4 = otp4Edt.getText().toString();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        LocalBroadcastManager.getInstance(OTP.this).unregisterReceiver(receiver);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(smsReceiver);
    }

    public class Test_Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "OnReceive");
            if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {
                Bundle extras = intent.getExtras();

                String strMessage = "";

                if (extras != null) {
                    Object[] smsextras = (Object[]) extras.get("pdus");

                    assert smsextras != null;
                    for (Object smsextra : smsextras) {
                        SmsMessage smsmsg = SmsMessage.createFromPdu((byte[]) smsextra);

                        String strMsgBody = smsmsg.getMessageBody();
                        strMessage += strMsgBody;
                    }

                    strMessage = strMessage.toLowerCase();
                    if (strMessage.contains("xoocar login")) {

                        String[] separated = strMessage.split(" ");

                        String otpRec=separated[5];
                        if (otpRec.equals(otp)) {
                            Toast.makeText(OTP.this, "OTP successfully verified", Toast.LENGTH_LONG).show();
                            if( isUserExists.equals("1") ) {
//                        call login api
                                login();
                            } else {
//                        call signup
                                Intent i=new Intent(getApplicationContext(),SignUp.class);
                                i.putExtra(Constants.MOB_NUM,mobNum);
                                startActivity(i);
                            }
                        }
                    }
                }
            }
        }
    }


    private void sendOtp(String num) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CheckUserExistsResponceFields> call = request.isUserExist("api/newloginapi/check_customerotp/format/json/"
                ,new CheckUserExistsRequestFields(new CheckUserExistsRequestData(num)));

        call.enqueue(new Callback<CheckUserExistsResponceFields>() {
            @Override
            public void onResponse(Call<CheckUserExistsResponceFields> call, Response<CheckUserExistsResponceFields> response) {

                if( response.body() != null  ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText( OTP.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if( response.body() != null ) {
                            CheckUserExistsResponceData userStatus= response.body().getDataArray();
                            otp=userStatus.getOtp();
                            Toast.makeText(OTP.this, "OTP resend successfully", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(OTP.this, "Some error occured please try again", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckUserExistsResponceFields> call, Throwable t) {

            }
        });
    }
}
