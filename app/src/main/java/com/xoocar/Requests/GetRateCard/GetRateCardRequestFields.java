package com.xoocar.Requests.GetRateCard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 22/8/17.
 */

public class GetRateCardRequestFields {

    public GetRateCardRequestFields(GetRateCardRequestData fields) {
        this.fields = fields;
    }

    public GetRateCardRequestData getFields() {

        return fields;
    }

    public void setFields(GetRateCardRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private GetRateCardRequestData fields;
}
