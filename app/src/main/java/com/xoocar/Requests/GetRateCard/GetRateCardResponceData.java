package com.xoocar.Requests.GetRateCard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 22/8/17.
 */

public class GetRateCardResponceData {

    @SerializedName("pc_id")
    @Expose
    private String pcId;
    @SerializedName("pc_catid")
    @Expose
    private String pcCatid;
    @SerializedName("pc_cityid")
    @Expose
    private String pcCityid;
    @SerializedName("pc_stateid")
    @Expose
    private String pcStateid;
    @SerializedName("pc_countryid")
    @Expose
    private String pcCountryid;
    @SerializedName("pc_basefare")
    @Expose
    private String pcBasefare;
    @SerializedName("pc_basefirstkm")
    @Expose
    private String pcBasefirstkm;
    @SerializedName("pc_ridechargepermin")
    @Expose
    private String pcRidechargepermin;
    @SerializedName("pc_conveniencecharge")
    @Expose
    private String pcConveniencecharge;
    @SerializedName("tollcharge")
    @Expose
    private String tollcharge;
    @SerializedName("pc_status")
    @Expose
    private String pcStatus;
    @SerializedName("pc_createdon")
    @Expose
    private String pcCreatedon;
    @SerializedName("pc_modifiedon")
    @Expose
    private String pcModifiedon;

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getPcCatid() {
        return pcCatid;
    }

    public void setPcCatid(String pcCatid) {
        this.pcCatid = pcCatid;
    }

    public String getPcCityid() {
        return pcCityid;
    }

    public void setPcCityid(String pcCityid) {
        this.pcCityid = pcCityid;
    }

    public String getPcStateid() {
        return pcStateid;
    }

    public void setPcStateid(String pcStateid) {
        this.pcStateid = pcStateid;
    }

    public String getPcCountryid() {
        return pcCountryid;
    }

    public void setPcCountryid(String pcCountryid) {
        this.pcCountryid = pcCountryid;
    }

    public String getPcBasefare() {
        return pcBasefare;
    }

    public void setPcBasefare(String pcBasefare) {
        this.pcBasefare = pcBasefare;
    }

    public String getPcBasefirstkm() {
        return pcBasefirstkm;
    }

    public void setPcBasefirstkm(String pcBasefirstkm) {
        this.pcBasefirstkm = pcBasefirstkm;
    }

    public String getPcRidechargepermin() {
        return pcRidechargepermin;
    }

    public void setPcRidechargepermin(String pcRidechargepermin) {
        this.pcRidechargepermin = pcRidechargepermin;
    }

    public String getPcConveniencecharge() {
        return pcConveniencecharge;
    }

    public void setPcConveniencecharge(String pcConveniencecharge) {
        this.pcConveniencecharge = pcConveniencecharge;
    }

    public String getTollcharge() {
        return tollcharge;
    }

    public void setTollcharge(String tollcharge) {
        this.tollcharge = tollcharge;
    }

    public String getPcStatus() {
        return pcStatus;
    }

    public void setPcStatus(String pcStatus) {
        this.pcStatus = pcStatus;
    }

    public String getPcCreatedon() {
        return pcCreatedon;
    }

    public void setPcCreatedon(String pcCreatedon) {
        this.pcCreatedon = pcCreatedon;
    }

    public String getPcModifiedon() {
        return pcModifiedon;
    }

    public void setPcModifiedon(String pcModifiedon) {
        this.pcModifiedon = pcModifiedon;
    }

    public String getPcType() {
        return pcType;
    }

    public void setPcType(String pcType) {
        this.pcType = pcType;
    }

    public String getIntercitytype() {
        return intercitytype;
    }

    public void setIntercitytype(String intercitytype) {
        this.intercitytype = intercitytype;
    }

    public String getTaxname() {
        return taxname;
    }

    public void setTaxname(String taxname) {
        this.taxname = taxname;
    }

    public String getTaxvalue() {
        return taxvalue;
    }

    public void setTaxvalue(String taxvalue) {
        this.taxvalue = taxvalue;
    }

    public String getRangefrom() {
        return rangefrom;
    }

    public void setRangefrom(String rangefrom) {
        this.rangefrom = rangefrom;
    }

    public String getRangeto() {
        return rangeto;
    }

    public void setRangeto(String rangeto) {
        this.rangeto = rangeto;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getPeakfrom() {
        return peakfrom;
    }

    public void setPeakfrom(String peakfrom) {
        this.peakfrom = peakfrom;
    }

    public String getPeakto() {
        return peakto;
    }

    public void setPeakto(String peakto) {
        this.peakto = peakto;
    }

    public String getPeakmulticharge() {
        return peakmulticharge;
    }

    public void setPeakmulticharge(String peakmulticharge) {
        this.peakmulticharge = peakmulticharge;
    }

    @SerializedName("pc_type")
    @Expose
    private String pcType;
    @SerializedName("intercitytype")
    @Expose
    private String intercitytype;
    @SerializedName("taxname")
    @Expose
    private String taxname;
    @SerializedName("taxvalue")
    @Expose
    private String taxvalue;
    @SerializedName("rangefrom")
    @Expose
    private String rangefrom;
    @SerializedName("rangeto")
    @Expose
    private String rangeto;
    @SerializedName("charge")
    @Expose
    private String charge;
    @SerializedName("peakfrom")
    @Expose
    private String peakfrom;
    @SerializedName("peakto")
    @Expose
    private String peakto;
    @SerializedName("peakmulticharge")
    @Expose
    private String peakmulticharge;
}
