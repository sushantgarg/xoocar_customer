package com.xoocar.Requests.GetRateCard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 22/8/17.
 */

public class GetRateCardRequestData {

    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("cityid")
    @Expose
    private String cityid;
    @SerializedName("vehicle_type_id")
    @Expose
    private String vehicleTypeId;
    @SerializedName("ride_type")
    @Expose
    private String rideType;

    public GetRateCardRequestData(String actMode, String cityid, String vehicleTypeId, String rideType) {
        this.actMode = actMode;
        this.cityid = cityid;
        this.vehicleTypeId = vehicleTypeId;
        this.rideType = rideType;
    }

    public String getActMode() {

        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getRideType() {
        return rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }
}
