package com.xoocar.Requests.GetDriverPosition;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class GetDriverRequestFields {
    public GetDriverRequestFields(GetDriverPositionRequestData fields) {
        this.fields = fields;
    }

    public GetDriverPositionRequestData getFields() {

        return fields;
    }

    public void setFields(GetDriverPositionRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private GetDriverPositionRequestData fields;
}
