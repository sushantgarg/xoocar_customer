package com.xoocar.Requests.GetDriverPosition;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class GetDriverPositionRequestData {
    @SerializedName("rideid")
    @Expose
    private String rideid;
    @SerializedName("customerid")
    @Expose
    private String customerid;

    public GetDriverPositionRequestData(String rideid, String customerid) {
        this.rideid = rideid;
        this.customerid = customerid;
    }

    public String getRideid() {

        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }
}
