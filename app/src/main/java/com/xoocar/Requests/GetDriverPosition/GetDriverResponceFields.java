package com.xoocar.Requests.GetDriverPosition;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class GetDriverResponceFields {
    @SerializedName("oid")
    @Expose
    private String oid;

    private String pay_mode,coupon_id;

    public String getPay_mode() {
        return pay_mode;
    }

    public void setPay_mode(String pay_mode) {
        this.pay_mode = pay_mode;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

//    public GetDriverResponceFields(String oid, String driverId, String customerId, String bookingStatus
//            , String dLatt, String dLong, String dCurrentStatus ,String course) {
//        this.oid = oid;
//        this.driverId = driverId;
//        this.customerId = customerId;
//        this.bookingStatus = bookingStatus;
//        this.dLatt = dLatt;
//        this.dLong = dLong;
//        this.dCurrentStatus = dCurrentStatus;
//        this.course=course;
//    }

    public String getOid() {

        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getdLatt() {
        return dLatt;
    }

    public void setdLatt(String dLatt) {
        this.dLatt = dLatt;
    }

    public String getdLong() {
        return dLong;
    }

    public void setdLong(String dLong) {
        this.dLong = dLong;
    }

    public String getdCurrentStatus() {
        return dCurrentStatus;
    }

    public void setdCurrentStatus(String dCurrentStatus) {
        this.dCurrentStatus = dCurrentStatus;
    }

    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("d_latt")
    @Expose
    private String dLatt;
    @SerializedName("d_long")
    @Expose
    private String dLong;
    @SerializedName("d_current_status")
    @Expose
    private String dCurrentStatus;
    private String course;
    private String contact;
    private String v_vehiclename;
    private String profile_pic;
    private String ridetype;
    private int cat_id;

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getV_vehiclename() {
        return v_vehiclename;
    }

    public void setV_vehiclename(String v_vehiclename) {
        this.v_vehiclename = v_vehiclename;
    }

    public String getRidetype() {
        return ridetype;
    }

    public void setRidetype(String ridetype) {
        this.ridetype = ridetype;
    }

    public String getV_licence_no() {
        return v_licence_no;
    }

    public void setV_licence_no(String v_licence_no) {
        this.v_licence_no = v_licence_no;
    }

    private String v_licence_no;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getModal() {
        return v_vehiclename;
    }

    public void setModal(String modal) {
        this.v_vehiclename = modal;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

}
