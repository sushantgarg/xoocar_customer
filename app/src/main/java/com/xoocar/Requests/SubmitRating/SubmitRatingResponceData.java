package com.xoocar.Requests.SubmitRating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/24/17.
 */

public class SubmitRatingResponceData {

    public String getWalletinfo() {
        return walletinfo;
    }

    public void setWalletinfo(String walletinfo) {
        this.walletinfo = walletinfo;
    }

    @SerializedName("walletinfo")
    @Expose
    private String walletinfo;
}
