package com.xoocar.Requests.SubmitRating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.ResendBooking.CabResponceData;
import com.xoocar.Requests.SubmitQuerry.SubmitQuerryResponceData;

import java.util.List;

/**
 * Created by sushant on 16/8/17.
 */

public class SubmitRatingResponce {
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    @SerializedName("mesagges")
    @Expose

    private String mesagges;

    public SubmitRatingResponceData getDataArray() {
        return dataArray;
    }

    public void setDataArray(SubmitRatingResponceData dataArray) {
        this.dataArray = dataArray;
    }

    @SerializedName("data_array")
    @Expose
    private SubmitRatingResponceData dataArray;

}
