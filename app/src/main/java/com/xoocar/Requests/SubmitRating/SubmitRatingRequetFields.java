package com.xoocar.Requests.SubmitRating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 16/8/17.
 */

public class SubmitRatingRequetFields {

    public SubmitRatingRequetFields(SubmitRatingRequestData fields) {
        this.fields = fields;
    }

    public SubmitRatingRequestData getFields() {

        return fields;
    }

    public void setFields(SubmitRatingRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private SubmitRatingRequestData fields;

}
