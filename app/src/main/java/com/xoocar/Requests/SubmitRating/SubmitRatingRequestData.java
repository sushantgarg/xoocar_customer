package com.xoocar.Requests.SubmitRating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 16/8/17.
 */

public class SubmitRatingRequestData {
    public SubmitRatingRequestData(String rideId, String rating, String desc) {
        this.rideId = rideId;
        this.rating = rating;
        this.desc = desc;
    }

    public String getRideId() {

        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("desc")
    @Expose
    private String desc;
}
