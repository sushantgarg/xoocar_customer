package com.xoocar.Requests.CancelRide;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/13/17.
 */

public class CancelRideFields {
    @SerializedName("oid")
    @Expose
    private String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }
}
