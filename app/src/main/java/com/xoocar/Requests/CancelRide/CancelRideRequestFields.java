package com.xoocar.Requests.CancelRide;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/13/17.
 */

public class CancelRideRequestFields {
    public CancelRideRequestFields(CancelRideRequestData fields) {
        this.fields = fields;
    }

    public CancelRideRequestData getFields() {

        return fields;
    }

    public void setFields(CancelRideRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private CancelRideRequestData fields;
}
