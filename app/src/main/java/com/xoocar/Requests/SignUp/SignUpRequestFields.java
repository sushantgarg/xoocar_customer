package com.xoocar.Requests.SignUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 15/8/17.
 */

public class SignUpRequestFields {

    public SignUpRequestFields(SignUpRequestData fields) {
        this.fields = fields;
    }

    public SignUpRequestData getFields() {

        return fields;
    }

    public void setFields(SignUpRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose

    private SignUpRequestData fields;

}
