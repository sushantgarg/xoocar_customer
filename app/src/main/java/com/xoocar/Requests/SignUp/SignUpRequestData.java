package com.xoocar.Requests.SignUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 15/8/17.
 */

public class SignUpRequestData {
    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    private String aadhar;

    public SignUpRequestData(String contactno, String userEmail, String fname, String deviceType,String aadhar, String deviceToken) {
        this.contactno = contactno;
        this.userEmail = userEmail;
        this.fname = fname;
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
        this.aadhar=aadhar;
    }

    public String getContactno() {

        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
