package com.xoocar.Requests.ConfirmBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/9/17.
 */

public class RequestBookingRequest {
    @SerializedName("latt")
    @Expose
    private String latt;
    @SerializedName("long")
    @Expose
    private String _long;

    public RequestBookingRequest(String latt, String _long, String vehicalId, String customerId
            , String source, String destination, String estimatedCost, String estimatedTime
            , String paymode, String couponId, String couponStatus, String mobileNo, String name
            , String cabnumber, String cityid, String destLat, String destLng,String driver_id,String hopngo) {
        this.latt = latt;
        this._long = _long;
        this.vehicalId = vehicalId;
        this.customerId = customerId;
        this.source = source;
        this.destination = destination;
        this.estimatedCost = estimatedCost;
        this.estimatedTime = estimatedTime;
        this.paymode = paymode;
        this.couponId = couponId;
        this.couponStatus = couponStatus;
        this.mobileNo = mobileNo;
        this.name = name;
        this.cabnumber = cabnumber;
        this.cityid = cityid;
        c_dest_lat=destLat;
        c_dest_lng=destLng;
        this.driver_id = driver_id;
        this.hopngo = hopngo;
    }

    @SerializedName("vehical_id")
    @Expose
    private String vehicalId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("estimated_cost")
    @Expose
    private String estimatedCost;
    @SerializedName("estimated_time")
    @Expose
    private String estimatedTime;
    @SerializedName("paymode")
    @Expose
    private String paymode;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("coupon_status")
    @Expose
    private String couponStatus;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cabnumber")
    @Expose
    private String cabnumber;
    @SerializedName("cityid")
    @Expose
    private String cityid;

    private String c_dest_lat,c_dest_lng;

    private String driver_id;

    private String hopngo;

    public String getC_dest_lat() {
        return c_dest_lat;
    }

    public void setC_dest_lat(String c_dest_lat) {
        this.c_dest_lat = c_dest_lat;
    }

    public String getC_dest_lng() {
        return c_dest_lng;
    }

    public void setC_dest_lng(String c_dest_lng) {
        this.c_dest_lng = c_dest_lng;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPaymode() {
        return paymode;
    }

    public void setPaymode(String paymode) {
        this.paymode = paymode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(String couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCabnumber() {
        return cabnumber;
    }

    public void setCabnumber(String cabnumber) {
        this.cabnumber = cabnumber;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

}
