package com.xoocar.Requests.ConfirmBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/10/17.
 */

public class CabDetailsResponce {

    @Expose
    private String rideid;

    public String getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }

    private String isvalid;

    public String getRideid() {
        return rideid;
    }
}
