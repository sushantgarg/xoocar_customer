package com.xoocar.Requests.ConfirmBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/9/17.
 */

public class FieldsGetBooking {
    @SerializedName("fields")
    @Expose
    private RequestBookingRequest fields;

    public FieldsGetBooking(RequestBookingRequest fields) {
        this.fields = fields;
    }

    public RequestBookingRequest getFields() {
        return fields;
    }

    public void setFields(RequestBookingRequest fields) {
        this.fields = fields;
    }
}
