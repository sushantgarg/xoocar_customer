package com.xoocar.Requests.HopNGo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sushant on 8/8/17.
 */

public class ResponceHopnGo {
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;
    @SerializedName("data_array")
    @Expose
    private List<CabDetailHopNGo> dataArray = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    public List<CabDetailHopNGo> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<CabDetailHopNGo> dataArray) {
        this.dataArray = dataArray;
    }

}
