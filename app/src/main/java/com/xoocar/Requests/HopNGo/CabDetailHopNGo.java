package com.xoocar.Requests.HopNGo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Realm.CabsRealm;

/**
 * Created by sushant on 8/8/17.
 */

public class CabDetailHopNGo {
    /*
    * here uid is driver id
    *
    * */

    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;
    @SerializedName("cabid")
    @Expose
    private String cabid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("d_current_status")
    @Expose
    private String dCurrentStatus;
    @SerializedName("caticon")
    @Expose
    private String caticon;
    @SerializedName("v_vehiclename")
    @Expose
    private String vVehiclename;
    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("v_noofseat")
    @Expose
    private String vNoofSeat;

    private String couponId;
    private String paymenyMode;
    private String driverId;

    public String getvLicenceNo() {
        return vLicenceNo;
    }

    public void setvLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getdCurrentStatus() {
        return dCurrentStatus;
    }

    public void setdCurrentStatus(String dCurrentStatus) {
        this.dCurrentStatus = dCurrentStatus;
    }

    public String getvVehiclename() {
        return vVehiclename;
    }

    public void setvVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public CabDetailHopNGo(CabsRealm cab) {
        this.vLicenceNo = cab.getOriginAddresses();
        this.cabid = cab.getUid();
        this.name = cab.getFirstName();
        this.id = cab.getCabType();
        this.vVehiclename = cab.getDestinationAddresses();
        this.driverId=cab.getUid();
        this.distance= String.valueOf(cab.getDurationValue());
    }

    public String getPaymenyMode() {
        return paymenyMode;
    }

    public void setPaymenyMode(String paymenyMode) {
        this.paymenyMode = paymenyMode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getvNoofSeat() {
        return vNoofSeat;
    }

    public void setvNoofSeat(String vNoofSeat) {
        this.vNoofSeat = vNoofSeat;
    }

    public String getVLicenceNo() {
        return vLicenceNo;
    }

    public void setVLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getCabid() {
        return cabid;
    }

    public void setCabid(String cabid) {
        this.cabid = cabid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDCurrentStatus() {
        return dCurrentStatus;
    }

    public void setDCurrentStatus(String dCurrentStatus) {
        this.dCurrentStatus = dCurrentStatus;
    }

    public String getCaticon() {
        return caticon;
    }

    public void setCaticon(String caticon) {
        this.caticon = caticon;
    }

    public String getVVehiclename() {
        return vVehiclename;
    }

    public void setVVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
