package com.xoocar.Requests.HopNGo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/8/17.
 */

public class RequestModelHopnGo {
    @SerializedName("act_mode")
    @Expose
    private String actMode;

    public RequestModelHopnGo(String actMode, String keyword, String latt, String _long) {
        this.actMode = actMode;
        this.keyword = keyword;
        this.latt = latt;
        this._long = _long;
    }

    @SerializedName("keyword")
    @Expose

    private String keyword;
    @SerializedName("latt")
    @Expose
    private String latt;
    @SerializedName("long")
    @Expose
    private String _long;

    public String getActMode() {
        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }
}
