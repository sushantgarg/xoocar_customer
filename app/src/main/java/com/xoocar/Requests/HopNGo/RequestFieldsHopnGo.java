package com.xoocar.Requests.HopNGo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/8/17.
 */

public class RequestFieldsHopnGo {
    @SerializedName("fields")
    @Expose
    private RequestModelHopnGo fields;

    public RequestFieldsHopnGo(RequestModelHopnGo fields) {
        this.fields = fields;
    }

    public RequestModelHopnGo getFields() {
        return fields;
    }

    public void setFields(RequestModelHopnGo fields) {
        this.fields = fields;
    }
}
