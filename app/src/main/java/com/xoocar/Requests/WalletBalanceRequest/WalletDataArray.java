package com.xoocar.Requests.WalletBalanceRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 11/8/17.
 */

public class WalletDataArray {


    @SerializedName("walletamt")
    @Expose
    private String walletamt;
    @SerializedName("promowallet")
    @Expose
    private String promowallet;
    @SerializedName("rules")
    @Expose
    private String rules;

    public String getWalletamt() {
        return walletamt;
    }

    public void setWalletamt(String walletamt) {
        this.walletamt = walletamt;
    }

    public String getPromowallet() {
        return promowallet;
    }

    public void setPromowallet(String promowallet) {
        this.promowallet = promowallet;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }
}
