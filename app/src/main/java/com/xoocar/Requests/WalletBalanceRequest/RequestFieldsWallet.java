package com.xoocar.Requests.WalletBalanceRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 11/8/17.
 */

public class RequestFieldsWallet {

    @SerializedName("userid")
    @Expose
    private String userid;

    public RequestFieldsWallet(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;

    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
