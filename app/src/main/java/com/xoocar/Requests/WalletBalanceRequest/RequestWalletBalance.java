package com.xoocar.Requests.WalletBalanceRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 11/8/17.
 */

public class RequestWalletBalance {
    @SerializedName("fields")
    @Expose
    private RequestFieldsWallet fields;

    public RequestFieldsWallet getFields() {
        return fields;
    }

    public RequestWalletBalance(RequestFieldsWallet fields) {
        this.fields = fields;
    }

    public void setFields(RequestFieldsWallet fields) {
        this.fields = fields;
    }
}
