package com.xoocar.Requests.WalletBalanceRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 11/8/17.
 */

public class ResponceWalletBalance {

    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;
    @SerializedName("data_array")
    @Expose
    private WalletDataArray dataArray;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    public WalletDataArray getDataArray() {
        return dataArray;
    }

    public void setDataArray(WalletDataArray dataArray) {
        this.dataArray = dataArray;
    }
}
