package com.xoocar.Requests.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 15/8/17.
 */

public class LoginRequestData {

    public LoginRequestData(String contactno, String deviceType, String deviceToken) {
        this.contactno = contactno;
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
    }

    public String getContactno() {

        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
}
