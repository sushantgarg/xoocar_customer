package com.xoocar.Requests.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 15/8/17.
 */

public class LoginResponceData {

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("o_id")
    @Expose
    private String oId;
    @SerializedName("agency_id")
    @Expose
    private String agencyId;
    @SerializedName("o_type")
    @Expose
    private String oType;
    @SerializedName("o_code")
    @Expose
    private String oCode;
    @SerializedName("firm_name")
    @Expose
    private Object firmName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private Object lastName;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("age")
    @Expose
    private Object age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("reg_password")
    @Expose
    private String regPassword;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("sociallogin_id")
    @Expose
    private String socialloginId;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("loginuser")
    @Expose
    private String loginuser;
    @SerializedName("attachemp")
    @Expose
    private String attachemp;
    @SerializedName("devicetype")
    @Expose
    private String devicetype;
    @SerializedName("devicetoken")
    @Expose
    private String devicetoken;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("modify_on")
    @Expose
    private String modifyOn;
    @SerializedName("t_status")
    @Expose
    private String tStatus;
    @SerializedName("approve_by_admin")
    @Expose
    private String approveByAdmin;
    @SerializedName("work_address")
    @Expose
    private String workAddress;
    @SerializedName("home_address")
    @Expose
    private String homeAddress;
    @SerializedName("authtoken")
    @Expose
    private String authtoken;

    public String getWalletinfo() {
        return walletinfo;
    }

    public void setWalletinfo(String walletinfo) {
        this.walletinfo = walletinfo;
    }

    private String walletinfo;

    public LoginResponceData(String uid, String userType, String oId, String agencyId, String oType, String oCode, Object firmName, String firstName, Object lastName, String userEmail, String contact, Object age, String gender, String dob, String profilePic, String regPassword, String address, String state, String city, String socialloginId, String loginType, String loginuser, String attachemp, String devicetype, String devicetoken, String createdOn, String modifyOn, String tStatus, String approveByAdmin, String workAddress, String homeAddress, String authtoken) {
        this.uid = uid;
        this.userType = userType;
        this.oId = oId;
        this.agencyId = agencyId;
        this.oType = oType;
        this.oCode = oCode;
        this.firmName = firmName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userEmail = userEmail;
        this.contact = contact;
        this.age = age;
        this.gender = gender;
        this.dob = dob;
        this.profilePic = profilePic;
        this.regPassword = regPassword;
        this.address = address;
        this.state = state;
        this.city = city;
        this.socialloginId = socialloginId;
        this.loginType = loginType;
        this.loginuser = loginuser;
        this.attachemp = attachemp;
        this.devicetype = devicetype;
        this.devicetoken = devicetoken;
        this.createdOn = createdOn;
        this.modifyOn = modifyOn;
        this.tStatus = tStatus;
        this.approveByAdmin = approveByAdmin;
        this.workAddress = workAddress;
        this.homeAddress = homeAddress;
        this.authtoken = authtoken;
    }

    public String getUid() {

        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getoId() {
        return oId;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public String getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    public String getoType() {
        return oType;
    }

    public void setoType(String oType) {
        this.oType = oType;
    }

    public String getoCode() {
        return oCode;
    }

    public void setoCode(String oCode) {
        this.oCode = oCode;
    }

    public Object getFirmName() {
        return firmName;
    }

    public void setFirmName(Object firmName) {
        this.firmName = firmName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getRegPassword() {
        return regPassword;
    }

    public void setRegPassword(String regPassword) {
        this.regPassword = regPassword;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSocialloginId() {
        return socialloginId;
    }

    public void setSocialloginId(String socialloginId) {
        this.socialloginId = socialloginId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginuser() {
        return loginuser;
    }

    public void setLoginuser(String loginuser) {
        this.loginuser = loginuser;
    }

    public String getAttachemp() {
        return attachemp;
    }

    public void setAttachemp(String attachemp) {
        this.attachemp = attachemp;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String gettStatus() {
        return tStatus;
    }

    public void settStatus(String tStatus) {
        this.tStatus = tStatus;
    }

    public String getApproveByAdmin() {
        return approveByAdmin;
    }

    public void setApproveByAdmin(String approveByAdmin) {
        this.approveByAdmin = approveByAdmin;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }
}
