package com.xoocar.Requests.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 15/8/17.
 */

public class LoginRequestFields {
    @SerializedName("fields")
    @Expose
    private LoginRequestData fields;

    public LoginRequestFields(LoginRequestData fields) {
        this.fields = fields;
    }

    public LoginRequestData getFields() {
        return fields;
    }

    public void setFields(LoginRequestData fields) {
        this.fields = fields;
    }
}
