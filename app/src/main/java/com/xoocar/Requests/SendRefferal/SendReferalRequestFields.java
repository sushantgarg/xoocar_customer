package com.xoocar.Requests.SendRefferal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 18/8/17.
 */

public class SendReferalRequestFields {
    public SendReferalRequestFields(SendReferalRequestData fields) {
        this.fields = fields;
    }

    public SendReferalRequestData getFields() {

        return fields;
    }

    public void setFields(SendReferalRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private SendReferalRequestData fields;
}
