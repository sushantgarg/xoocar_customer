package com.xoocar.Requests.SendRefferal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 18/8/17.
 */

public class SendReferalRequestData {
    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("reffer_id")
    @Expose
    private String refferId;

    public SendReferalRequestData(String contactno, String refferId) {
        this.contactno = contactno;
        this.refferId = refferId;
    }

    public String getContactno() {

        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getRefferId() {
        return refferId;
    }

    public void setRefferId(String refferId) {
        this.refferId = refferId;
    }
}
