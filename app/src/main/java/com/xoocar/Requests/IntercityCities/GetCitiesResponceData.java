package com.xoocar.Requests.IntercityCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/10/17.
 */

public class GetCitiesResponceData {
    public String getaCityId() {
        return aCityId;
    }

    public void setaCityId(String aCityId) {
        this.aCityId = aCityId;
    }

    public String gettCityName() {
        return tCityName;
    }

    public void settCityName(String tCityName) {
        this.tCityName = tCityName;
    }

    @SerializedName("a_CityId")
    @Expose
    private String aCityId;
    @SerializedName("t_CityName")
    @Expose
    private String tCityName;
}
