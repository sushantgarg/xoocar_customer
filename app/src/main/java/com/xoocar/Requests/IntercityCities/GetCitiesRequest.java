package com.xoocar.Requests.IntercityCities;

/**
 * Created by sushant on 10/10/17.
 */

public class GetCitiesRequest {
    String fields;

    public GetCitiesRequest(String fields) {
        this.fields = fields;
    }

    public String getFields() {

        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}
