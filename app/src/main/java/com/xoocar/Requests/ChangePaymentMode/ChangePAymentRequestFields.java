package com.xoocar.Requests.ChangePaymentMode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class ChangePAymentRequestFields {
    public ChangePAymentRequestFields(ChangePaymentRequestData fields) {
        this.fields = fields;
    }

    public ChangePaymentRequestData getFields() {

        return fields;
    }

    public void setFields(ChangePaymentRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private ChangePaymentRequestData fields;
}
