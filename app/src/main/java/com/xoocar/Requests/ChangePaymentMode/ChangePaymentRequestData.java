package com.xoocar.Requests.ChangePaymentMode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class ChangePaymentRequestData {
    public ChangePaymentRequestData(String oid, String paymode) {
        this.oid = oid;
        this.paymode = paymode;
    }

    public String getOid() {

        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPaymode() {
        return paymode;
    }

    public void setPaymode(String paymode) {
        this.paymode = paymode;
    }

    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("paymode")
    @Expose
    private String paymode;
}
