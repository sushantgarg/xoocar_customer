package com.xoocar.Requests.BillDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 16/8/17.
 */

public class BillDetailRequestFields {
    public BillDetailRequestFields(BillDetailsRequestData fields) {
        this.fields = fields;
    }

    public BillDetailsRequestData getFields() {
        return fields;
    }

    public void setFields(BillDetailsRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private BillDetailsRequestData fields;
}
