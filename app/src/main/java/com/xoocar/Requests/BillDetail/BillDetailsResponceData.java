package com.xoocar.Requests.BillDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 16/8/17.
 */

public class BillDetailsResponceData {

    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("actual_cost")
    @Expose
    private String actualCost;
    @SerializedName("actual_time")
    @Expose
    private String actualTime;
    @SerializedName("actual_kms")
    @Expose
    private String actualKms;
    @SerializedName("pay_mode")
    @Expose
    private String payMode;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("cash_paid")
    @Expose
    private String cashPaid;
    @SerializedName("wallet_paid")
    @Expose
    private String walletPaid;
    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("ride_summary")
    @Expose
    private String rideSummary;
    @SerializedName("coupondiscount")
    @Expose
    private String coupondiscount;
    @SerializedName("net")
    @Expose
    private String net;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("tolltax")
    @Expose
    private Integer tolltax;

    private String taxcharge;

    private String first_name,profile_pic,v_licence_no;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getV_licence_no() {
        return v_licence_no;
    }

    public void setV_licence_no(String v_licence_no) {
        this.v_licence_no = v_licence_no;
    }

    public String getTaxcharge() {
        return taxcharge;
    }

    public void setTaxcharge(String taxcharge) {
        this.taxcharge = taxcharge;
    }

    public BillDetailsResponceData(String source, String destination, String actualCost, String actualTime
            , String actualKms, String payMode, String couponId, String cashPaid, String walletPaid
            , String oid, String customerId, String couponCode, String rideSummary, String coupondiscount
            , String net, String total, Integer tolltax,String taxcharge,String first_name,String profile_pic,String v_licence_no) {
        this.source = source;
        this.destination = destination;
        this.actualCost = actualCost;
        this.actualTime = actualTime;
        this.actualKms = actualKms;
        this.payMode = payMode;
        this.couponId = couponId;
        this.cashPaid = cashPaid;
        this.walletPaid = walletPaid;
        this.oid = oid;
        this.customerId = customerId;
        this.couponCode = couponCode;
        this.rideSummary = rideSummary;
        this.coupondiscount = coupondiscount;
        this.net = net;
        this.total = total;
        this.tolltax = tolltax;
        this.taxcharge=taxcharge;
        this.first_name=first_name;
        this.profile_pic=profile_pic;
        this.v_licence_no=v_licence_no;
    }

    public String getSource() {

        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCashPaid() {
        return cashPaid;
    }

    public void setCashPaid(String cashPaid) {
        this.cashPaid = cashPaid;
    }

    public String getWalletPaid() {
        return walletPaid;
    }

    public void setWalletPaid(String walletPaid) {
        this.walletPaid = walletPaid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getRideSummary() {
        return rideSummary;
    }

    public void setRideSummary(String rideSummary) {
        this.rideSummary = rideSummary;
    }

    public String getCoupondiscount() {
        return coupondiscount;
    }

    public void setCoupondiscount(String coupondiscount) {
        this.coupondiscount = coupondiscount;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Integer getTolltax() {
        return tolltax;
    }

    public void setTolltax(Integer tolltax) {
        this.tolltax = tolltax;
    }
}
