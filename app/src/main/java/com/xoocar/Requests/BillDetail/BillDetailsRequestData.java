package com.xoocar.Requests.BillDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 16/8/17.
 */

public class BillDetailsRequestData {


    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("rideid")
    @Expose
    private String rideid;

    public String getActMode() {
        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public BillDetailsRequestData(String actMode, String customerid, String rideid) {
        this.actMode = actMode;
        this.customerid = customerid;
        this.rideid = rideid;
    }
}
