package com.xoocar.Requests.ConfirmIntercityBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/18/17.
 */

public class ConfirmIntercityRequestFields {
    public ConfirmIntercityRequestData getFields() {
        return fields;
    }

    public void setFields(ConfirmIntercityRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private ConfirmIntercityRequestData fields;

    public ConfirmIntercityRequestFields(ConfirmIntercityRequestData fields) {
        this.fields = fields;
    }
}
