package com.xoocar.Requests.ConfirmIntercityBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/18/17.
 */

public class ConfirmIntercityResponceData {


    @SerializedName("rideid")
    @Expose
    private String rideid;

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }
}
