package com.xoocar.Requests.ConfirmIntercityBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/18/17.
 */

public class ConfirmIntercityRequestData {

    @SerializedName("customer_id")
    @Expose
    private String customerId;

    public ConfirmIntercityRequestData( String customerId, String source, String destination
            , String estimatedCost, String estimatedTime, String pickupTime, String paymode
            , String couponId, String couponStatus, String mobileNo, String name
            , String fromcityid, String tocityid, String intercitytype, String latt
            , String _long,String vehical_id ) {
        this.customerId = customerId;
        this.source = source;
        this.destination = destination;
        this.estimatedCost = estimatedCost;
        this.estimatedTime = estimatedTime;
        this.pickupTime = pickupTime;
        this.paymode = paymode;
        this.couponId = couponId;
        this.couponStatus = couponStatus;
        this.mobileNo = mobileNo;
        this.name = name;
        this.fromcityid = fromcityid;
        this.tocityid = tocityid;
        this.intercitytype = intercitytype;
        this.latt = latt;
        this._long = _long;
        this.vehical_id=vehical_id;
    }

    public String getCustomerId() {
        return customerId;

    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPaymode() {
        return paymode;
    }

    public void setPaymode(String paymode) {
        this.paymode = paymode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(String couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromcityid() {
        return fromcityid;
    }

    public void setFromcityid(String fromcityid) {
        this.fromcityid = fromcityid;
    }

    public String getTocityid() {
        return tocityid;
    }

    public void setTocityid(String tocityid) {
        this.tocityid = tocityid;
    }

    public String getIntercitytype() {
        return intercitytype;
    }

    public void setIntercitytype(String intercitytype) {
        this.intercitytype = intercitytype;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String get_long() {
        return _long;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    @SerializedName("source")

    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("estimated_cost")
    @Expose
    private String estimatedCost;
    @SerializedName("estimated_time")
    @Expose
    private String estimatedTime;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("paymode")
    @Expose
    private String paymode;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("coupon_status")
    @Expose
    private String couponStatus;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fromcityid")
    @Expose
    private String fromcityid;
    @SerializedName("tocityid")
    @Expose
    private String tocityid;
    @SerializedName("intercitytype")
    @Expose
    private String intercitytype;
    @SerializedName("latt")
    @Expose
    private String latt;
    @SerializedName("long")
    @Expose
    private String _long;

    public String getVehical_id() {
        return vehical_id;
    }

    public void setVehical_id(String vehical_id) {
        this.vehical_id = vehical_id;
    }

    private String vehical_id;

}
