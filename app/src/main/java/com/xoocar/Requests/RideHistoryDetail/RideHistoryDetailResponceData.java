package com.xoocar.Requests.RideHistoryDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 21/8/17.
 */

public class RideHistoryDetailResponceData {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("gross_amt")
    @Expose
    private String grossAmt;
    @SerializedName("base_fare")
    @Expose
    private String baseFare;
    @SerializedName("ride_charge")
    @Expose
    private String rideCharge;
    @SerializedName("wait_charge")
    @Expose
    private String waitCharge;
    @SerializedName("tax_charge")
    @Expose
    private String taxCharge;
    @SerializedName("coupon_price")
    @Expose
    private String couponPrice;
    @SerializedName("convenience_charge")
    @Expose
    private String convenienceCharge;
    @SerializedName("perminute_charge_accordingly")
    @Expose
    private String perminuteChargeAccordingly;
    @SerializedName("tolltax")
    @Expose
    private String tolltax;
    @SerializedName("pay_mode")
    @Expose
    private String payMode;
    @SerializedName("net_amt")
    @Expose
    private String netAmt;
    @SerializedName("summery_created_on")
    @Expose
    private String summeryCreatedOn;
    @SerializedName("summery_modified_on")
    @Expose
    private String summeryModifiedOn;
    @SerializedName("v_vehiclename")
    @Expose
    private String vVehiclename;
    @SerializedName("v_exteriorcolor")
    @Expose
    private String vExteriorcolor;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("dname")
    @Expose
    private String dname;
    @SerializedName("driver_pic")
    @Expose
    private String driverPic;
    @SerializedName("demail")
    @Expose
    private String demail;
    @SerializedName("dcontact")
    @Expose
    private String dcontact;
    @SerializedName("v_category")
    @Expose
    private String vCategory;
    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;
    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driversvehical_id")
    @Expose
    private String driversvehicalId;
    @SerializedName("vehical_id")
    @Expose
    private String vehicalId;
    @SerializedName("price_city_id")
    @Expose
    private String priceCityId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("c_contacno")
    @Expose
    private String cContacno;
    @SerializedName("tempcust_name")
    @Expose
    private String tempcustName;
    @SerializedName("crn_no")
    @Expose
    private String crnNo;
    @SerializedName("hopngo")
    @Expose
    private String hopngo;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("estimated_cost")
    @Expose
    private String estimatedCost;
    @SerializedName("estimated_time")
    @Expose
    private String estimatedTime;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("diliver_time")
    @Expose
    private String diliverTime;
    @SerializedName("r_pickupdate")
    @Expose
    private String rPickupdate;
    @SerializedName("r_delivereddate")
    @Expose
    private String rDelivereddate;
    @SerializedName("actual_cost")
    @Expose
    private String actualCost;
    @SerializedName("gross_amount")
    @Expose
    private String grossAmount;
    @SerializedName("cash_paid")
    @Expose
    private String cashPaid;
    @SerializedName("wallet_paid")
    @Expose
    private String walletPaid;
    @SerializedName("actual_time")
    @Expose
    private String actualTime;
    @SerializedName("actual_kms")
    @Expose
    private String actualKms;
    @SerializedName("isCoupon")
    @Expose
    private String isCoupon;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("d_CreatedOn")
    @Expose
    private String dCreatedOn;
    @SerializedName("n_CreatedBy")
    @Expose
    private Object nCreatedBy;
    @SerializedName("d_ModifiedOn")
    @Expose
    private Object dModifiedOn;
    @SerializedName("n_ModifiedBy")
    @Expose
    private Object nModifiedBy;
    @SerializedName("b_Deleted")
    @Expose
    private String bDeleted;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("pay_success")
    @Expose
    private String paySuccess;
    @SerializedName("alter_driver")
    @Expose
    private String alterDriver;
    @SerializedName("ridetype")
    @Expose
    private String ridetype;
    @SerializedName("way_bill")
    @Expose
    private String wayBill;
    @SerializedName("b_cancelled_by")
    @Expose
    private String bCancelledBy;
    @SerializedName("cancelled_why")
    @Expose
    private String cancelledWhy;
    @SerializedName("n_AdminType")
    @Expose
    private Object nAdminType;
    @SerializedName("fromcityid")
    @Expose
    private String fromcityid;
    @SerializedName("tocityid")
    @Expose
    private String tocityid;
    @SerializedName("intercitytype")
    @Expose
    private String intercitytype;
    @SerializedName("d_alter1")
    @Expose
    private Object dAlter1;
    @SerializedName("d_reason1")
    @Expose
    private Object dReason1;
    @SerializedName("d_alter2")
    @Expose
    private Object dAlter2;
    @SerializedName("d_reason2")
    @Expose
    private Object dReason2;
    @SerializedName("c_latt")
    @Expose
    private String cLatt;
    @SerializedName("c_long")
    @Expose
    private String cLong;
    @SerializedName("c_dest_lat")
    @Expose
    private String cDestLat;
    @SerializedName("c_dest_lng")
    @Expose
    private String cDestLng;

    private String poly;

    public String getPoly() {
        return poly;
    }

    public void setPoly(String poly) {
        this.poly = poly;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getGrossAmt() {
        return grossAmt;
    }

    public void setGrossAmt(String grossAmt) {
        this.grossAmt = grossAmt;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getRideCharge() {
        return rideCharge;
    }

    public void setRideCharge(String rideCharge) {
        this.rideCharge = rideCharge;
    }

    public String getWaitCharge() {
        return waitCharge;
    }

    public void setWaitCharge(String waitCharge) {
        this.waitCharge = waitCharge;
    }

    public String getTaxCharge() {
        return taxCharge;
    }

    public void setTaxCharge(String taxCharge) {
        this.taxCharge = taxCharge;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(String convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public String getPerminuteChargeAccordingly() {
        return perminuteChargeAccordingly;
    }

    public void setPerminuteChargeAccordingly(String perminuteChargeAccordingly) {
        this.perminuteChargeAccordingly = perminuteChargeAccordingly;
    }

    public String getTolltax() {
        return tolltax;
    }

    public void setTolltax(String tolltax) {
        this.tolltax = tolltax;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(String netAmt) {
        this.netAmt = netAmt;
    }

    public String getSummeryCreatedOn() {
        return summeryCreatedOn;
    }

    public void setSummeryCreatedOn(String summeryCreatedOn) {
        this.summeryCreatedOn = summeryCreatedOn;
    }

    public String getSummeryModifiedOn() {
        return summeryModifiedOn;
    }

    public void setSummeryModifiedOn(String summeryModifiedOn) {
        this.summeryModifiedOn = summeryModifiedOn;
    }

    public String getvVehiclename() {
        return vVehiclename;
    }

    public void setvVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

    public String getvExteriorcolor() {
        return vExteriorcolor;
    }

    public void setvExteriorcolor(String vExteriorcolor) {
        this.vExteriorcolor = vExteriorcolor;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDriverPic() {
        return driverPic;
    }

    public void setDriverPic(String driverPic) {
        this.driverPic = driverPic;
    }

    public String getDemail() {
        return demail;
    }

    public void setDemail(String demail) {
        this.demail = demail;
    }

    public String getDcontact() {
        return dcontact;
    }

    public void setDcontact(String dcontact) {
        this.dcontact = dcontact;
    }

    public String getvCategory() {
        return vCategory;
    }

    public void setvCategory(String vCategory) {
        this.vCategory = vCategory;
    }

    public String getvLicenceNo() {
        return vLicenceNo;
    }

    public void setvLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriversvehicalId() {
        return driversvehicalId;
    }

    public void setDriversvehicalId(String driversvehicalId) {
        this.driversvehicalId = driversvehicalId;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

    public String getPriceCityId() {
        return priceCityId;
    }

    public void setPriceCityId(String priceCityId) {
        this.priceCityId = priceCityId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getcContacno() {
        return cContacno;
    }

    public void setcContacno(String cContacno) {
        this.cContacno = cContacno;
    }

    public String getTempcustName() {
        return tempcustName;
    }

    public void setTempcustName(String tempcustName) {
        this.tempcustName = tempcustName;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getHopngo() {
        return hopngo;
    }

    public void setHopngo(String hopngo) {
        this.hopngo = hopngo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDiliverTime() {
        return diliverTime;
    }

    public void setDiliverTime(String diliverTime) {
        this.diliverTime = diliverTime;
    }

    public String getrPickupdate() {
        return rPickupdate;
    }

    public void setrPickupdate(String rPickupdate) {
        this.rPickupdate = rPickupdate;
    }

    public String getrDelivereddate() {
        return rDelivereddate;
    }

    public void setrDelivereddate(String rDelivereddate) {
        this.rDelivereddate = rDelivereddate;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getCashPaid() {
        return cashPaid;
    }

    public void setCashPaid(String cashPaid) {
        this.cashPaid = cashPaid;
    }

    public String getWalletPaid() {
        return walletPaid;
    }

    public void setWalletPaid(String walletPaid) {
        this.walletPaid = walletPaid;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(String isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getdCreatedOn() {
        return dCreatedOn;
    }

    public void setdCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public Object getnCreatedBy() {
        return nCreatedBy;
    }

    public void setnCreatedBy(Object nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public Object getdModifiedOn() {
        return dModifiedOn;
    }

    public void setdModifiedOn(Object dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public Object getnModifiedBy() {
        return nModifiedBy;
    }

    public void setnModifiedBy(Object nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public String getbDeleted() {
        return bDeleted;
    }

    public void setbDeleted(String bDeleted) {
        this.bDeleted = bDeleted;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPaySuccess() {
        return paySuccess;
    }

    public void setPaySuccess(String paySuccess) {
        this.paySuccess = paySuccess;
    }

    public String getAlterDriver() {
        return alterDriver;
    }

    public void setAlterDriver(String alterDriver) {
        this.alterDriver = alterDriver;
    }

    public String getRidetype() {
        return ridetype;
    }

    public void setRidetype(String ridetype) {
        this.ridetype = ridetype;
    }

    public String getWayBill() {
        return wayBill;
    }

    public void setWayBill(String wayBill) {
        this.wayBill = wayBill;
    }

    public String getbCancelledBy() {
        return bCancelledBy;
    }

    public void setbCancelledBy(String bCancelledBy) {
        this.bCancelledBy = bCancelledBy;
    }

    public String getCancelledWhy() {
        return cancelledWhy;
    }

    public void setCancelledWhy(String cancelledWhy) {
        this.cancelledWhy = cancelledWhy;
    }

    public Object getnAdminType() {
        return nAdminType;
    }

    public void setnAdminType(Object nAdminType) {
        this.nAdminType = nAdminType;
    }

    public String getFromcityid() {
        return fromcityid;
    }

    public void setFromcityid(String fromcityid) {
        this.fromcityid = fromcityid;
    }

    public String getTocityid() {
        return tocityid;
    }

    public void setTocityid(String tocityid) {
        this.tocityid = tocityid;
    }

    public String getIntercitytype() {
        return intercitytype;
    }

    public void setIntercitytype(String intercitytype) {
        this.intercitytype = intercitytype;
    }

    public Object getdAlter1() {
        return dAlter1;
    }

    public void setdAlter1(Object dAlter1) {
        this.dAlter1 = dAlter1;
    }

    public Object getdReason1() {
        return dReason1;
    }

    public void setdReason1(Object dReason1) {
        this.dReason1 = dReason1;
    }

    public Object getdAlter2() {
        return dAlter2;
    }

    public void setdAlter2(Object dAlter2) {
        this.dAlter2 = dAlter2;
    }

    public Object getdReason2() {
        return dReason2;
    }

    public void setdReason2(Object dReason2) {
        this.dReason2 = dReason2;
    }

    public String getcLatt() {
        return cLatt;
    }

    public void setcLatt(String cLatt) {
        this.cLatt = cLatt;
    }

    public String getcLong() {
        return cLong;
    }

    public void setcLong(String cLong) {
        this.cLong = cLong;
    }

    public String getcDestLat() {
        return cDestLat;
    }

    public void setcDestLat(String cDestLat) {
        this.cDestLat = cDestLat;
    }

    public String getcDestLng() {
        return cDestLng;
    }

    public void setcDestLng(String cDestLng) {
        this.cDestLng = cDestLng;
    }
}
