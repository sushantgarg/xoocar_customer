package com.xoocar.Requests.RideHistoryDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 21/8/17.
 */

public class RideHistoryDetailRequestField {
    public RideHistoryDetailRequestField(RideHistoryDetailRequestData fields) {
        this.fields = fields;
    }

    public RideHistoryDetailRequestData getFields() {

        return fields;
    }

    public void setFields(RideHistoryDetailRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private RideHistoryDetailRequestData fields;
}
