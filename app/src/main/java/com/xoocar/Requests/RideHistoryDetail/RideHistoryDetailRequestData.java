package com.xoocar.Requests.RideHistoryDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 21/8/17.
 */

public class RideHistoryDetailRequestData {

    @SerializedName("ride_id")
    @Expose
    private String rideId;

    public RideHistoryDetailRequestData(String rideId) {
        this.rideId = rideId;
    }

    public String getRideId() {

        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }
}
