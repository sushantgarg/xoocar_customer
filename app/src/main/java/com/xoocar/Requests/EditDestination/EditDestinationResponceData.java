package com.xoocar.Requests.EditDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 18/8/17.
 */

public class EditDestinationResponceData {

    @SerializedName("rideid")
    @Expose
    private String rideid;
    @SerializedName("devicetype")
    @Expose
    private String devicetype;
    @SerializedName("devicetoken")
    @Expose
    private String devicetoken;
    @SerializedName("uid")
    @Expose
    private String uid;

    public EditDestinationResponceData(String rideid, String devicetype, String devicetoken, String uid) {
        this.rideid = rideid;
        this.devicetype = devicetype;
        this.devicetoken = devicetoken;
        this.uid = uid;
    }

    public String getRideid() {

        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
