package com.xoocar.Requests.EditDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 18/8/17.
 */

public class EditDestinationrequestFields {
    @SerializedName("fields")
    @Expose
    private EditDestinationRequestData fields;

    public EditDestinationrequestFields(EditDestinationRequestData fields) {
        this.fields = fields;
    }

    public EditDestinationRequestData getFields() {

        return fields;
    }

    public void setFields(EditDestinationRequestData fields) {
        this.fields = fields;
    }
}
