package com.xoocar.Requests.EditDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 18/8/17.
 */

public class EditDestinationRequestData {
    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("destination")
    @Expose
    private String destination;

    private String d_lat,d_lng;

    public EditDestinationRequestData(String oid, String destination, String d_lat, String d_lng) {
        this.oid = oid;
        this.destination = destination;
        this.d_lat = d_lat;
        this.d_lng = d_lng;
    }

    public String getD_lat() {

        return d_lat;
    }

    public void setD_lat(String d_lat) {
        this.d_lat = d_lat;
    }

    public String getD_lng() {
        return d_lng;
    }

    public void setD_lng(String d_lng) {
        this.d_lng = d_lng;
    }

    public String getOid() {

        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
