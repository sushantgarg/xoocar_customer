package com.xoocar.Requests.GetRideById;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sushant on 8/11/17.
 */

public class RequestFieldsGetCabById {
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;
    @SerializedName("data_array")
    @Expose
    private List<RequestData> dataArray = null;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    public List<RequestData> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<RequestData> dataArray) {
        this.dataArray = dataArray;
    }
}
