package com.xoocar.Requests.GetRideById;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/11/17.
 */

public class FieldsRequestGetCabById {
    @SerializedName("rideid")
    @Expose
    private String rideid;

    public FieldsRequestGetCabById(String rideid) {
        this.rideid = rideid;
    }

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }
}
