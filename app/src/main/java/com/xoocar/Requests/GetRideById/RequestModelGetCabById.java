package com.xoocar.Requests.GetRideById;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/11/17.
 */

public class RequestModelGetCabById {
    @SerializedName("fields")
    @Expose
    private FieldsRequestGetCabById fields;

    public RequestModelGetCabById(FieldsRequestGetCabById fields) {
        this.fields = fields;
    }

    public FieldsRequestGetCabById getFields() {
        return fields;
    }

    public void setFields(FieldsRequestGetCabById fields) {
        this.fields = fields;
    }
}
