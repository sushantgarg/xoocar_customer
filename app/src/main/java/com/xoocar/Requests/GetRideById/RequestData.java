package com.xoocar.Requests.GetRideById;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/11/17.
 */

public class RequestData {

    @SerializedName("rideid")
    @Expose
    private String rideid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("crn_no")
    @Expose
    private String crnNo;
    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;
    @SerializedName("v_exteriorcolor")
    @Expose
    private String vExteriorcolor;
    @SerializedName("v_category")
    @Expose
    private String vCategory;
    @SerializedName("modal")
    @Expose
    private String modal;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    private String rating;

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    private String booking_status;

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getVLicenceNo() {
        return vLicenceNo;
    }

    public void setVLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getVExteriorcolor() {
        return vExteriorcolor;
    }

    public void setVExteriorcolor(String vExteriorcolor) {
        this.vExteriorcolor = vExteriorcolor;
    }

    public String getVCategory() {
        return vCategory;
    }

    public void setVCategory(String vCategory) {
        this.vCategory = vCategory;
    }

    public String getModal() {
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }
}
