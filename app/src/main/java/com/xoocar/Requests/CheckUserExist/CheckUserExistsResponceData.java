package com.xoocar.Requests.CheckUserExist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 14/8/17.
 */

public class CheckUserExistsResponceData {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("otp")
    @Expose
    private String otp;

    public CheckUserExistsResponceData(String status, String otp) {
        this.status = status;
        this.otp = otp;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
