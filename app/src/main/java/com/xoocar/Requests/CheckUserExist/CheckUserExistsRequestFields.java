package com.xoocar.Requests.CheckUserExist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 14/8/17.
 */

public class CheckUserExistsRequestFields {
    public CheckUserExistsRequestFields(CheckUserExistsRequestData fields) {
        this.fields = fields;
    }

    public CheckUserExistsRequestData getFields() {

        return fields;
    }

    public void setFields(CheckUserExistsRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private CheckUserExistsRequestData fields;

}
