package com.xoocar.Requests.CheckUserExist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 14/8/17.
 */

public class CheckUserExistsRequestData {
    @SerializedName("contactno")
    @Expose
    private String contactno;

    public CheckUserExistsRequestData(String contactno) {
        this.contactno = contactno;
    }

    public String getContactno() {

        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }
}
