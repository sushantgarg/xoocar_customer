package com.xoocar.Requests.IntercityRideRstimate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/11/17.
 */

public class GetRideEstimateRequestFields {

    public GetRideEstimateRequestFields(GetRideEstimateRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private GetRideEstimateRequestData fields;
}
