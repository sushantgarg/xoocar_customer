package com.xoocar.Requests.IntercityRideRstimate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/11/17.
 */

public class GetRideEstimateRequestData {

    public String getNumdays() {
        return numdays;
    }

    private final String numdays;
    @SerializedName("city_from")
    @Expose
    private String cityFrom;
    @SerializedName("city_to")
    @Expose
    private String cityTo;

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public GetRideEstimateRequestData(String cityFrom, String cityTo, String trip,String numDays) {

        this.cityFrom = cityFrom;
        this.cityTo = cityTo;
        this.trip = trip;
        this.numdays=numDays;
    }

    @SerializedName("trip")
    @Expose
    private String trip;
}
