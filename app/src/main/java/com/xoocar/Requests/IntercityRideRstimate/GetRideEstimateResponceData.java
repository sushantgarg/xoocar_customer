package com.xoocar.Requests.IntercityRideRstimate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/11/17.
 */

public class GetRideEstimateResponceData {

    @SerializedName("rangefrom")
    @Expose
    private String rangefrom;
    @SerializedName("rangeto")
    @Expose
    private String rangeto;
    @SerializedName("charge")
    @Expose
    private String charge;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("pc_id")
    @Expose
    private String pcId;
    @SerializedName("pc_catid")
    @Expose
    private String pcCatid;
    @SerializedName("pc_cityid")
    @Expose
    private String pcCityid;
    @SerializedName("pc_basefare")
    @Expose
    private String pcBasefare;
    @SerializedName("pc_basefirstkm")
    @Expose
    private String pcBasefirstkm;
    @SerializedName("estimatedprice")
    @Expose
    private String estimatedprice;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("additionkms")
    @Expose
    private String additionkms;
    @SerializedName("additionkmscharge")
    @Expose
    private String additionkmscharge;
    @SerializedName("estinmatedtime")
    @Expose
    private String estinmatedtime;

    private String DA;

    public String getDA() {
        return DA;
    }

    public void setDA(String DA) {
        this.DA = DA;
    }

    public String getRangefrom() {
        return rangefrom;
    }

    public void setRangefrom(String rangefrom) {
        this.rangefrom = rangefrom;
    }

    public String getRangeto() {
        return rangeto;
    }

    public void setRangeto(String rangeto) {
        this.rangeto = rangeto;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getPcCatid() {
        return pcCatid;
    }

    public void setPcCatid(String pcCatid) {
        this.pcCatid = pcCatid;
    }

    public String getPcCityid() {
        return pcCityid;
    }

    public void setPcCityid(String pcCityid) {
        this.pcCityid = pcCityid;
    }

    public String getPcBasefare() {
        return pcBasefare;
    }

    public void setPcBasefare(String pcBasefare) {
        this.pcBasefare = pcBasefare;
    }

    public String getPcBasefirstkm() {
        return pcBasefirstkm;
    }

    public void setPcBasefirstkm(String pcBasefirstkm) {
        this.pcBasefirstkm = pcBasefirstkm;
    }

    public String getEstimatedprice() {
        return estimatedprice;
    }

    public void setEstimatedprice(String estimatedprice) {
        this.estimatedprice = estimatedprice;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getAdditionkms() {
        return additionkms;
    }

    public void setAdditionkms(String additionkms) {
        this.additionkms = additionkms;
    }

    public String getAdditionkmscharge() {
        return additionkmscharge;
    }

    public void setAdditionkmscharge(String additionkmscharge) {
        this.additionkmscharge = additionkmscharge;
    }

    public String getEstinmatedtime() {
        return estinmatedtime;
    }

    public void setEstinmatedtime(String estinmatedtime) {
        this.estinmatedtime = estinmatedtime;
    }
}
