package com.xoocar.Requests.SubmitQuerry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/21/17.
 */

public class SubmitQuerryRequestFields {
    @SerializedName("fields")
    @Expose
    private SubmitQuerryRequestData fields;

    public SubmitQuerryRequestFields(SubmitQuerryRequestData fields) {
        this.fields = fields;
    }

    public SubmitQuerryRequestData getFields() {

        return fields;
    }

    public void setFields(SubmitQuerryRequestData fields) {
        this.fields = fields;
    }
}
