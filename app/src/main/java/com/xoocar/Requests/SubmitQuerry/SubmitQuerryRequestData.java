package com.xoocar.Requests.SubmitQuerry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/21/17.
 */

public class SubmitQuerryRequestData {
    public SubmitQuerryRequestData(String uid, String query) {
        this.uid = uid;
        this.query = query;
    }

    public String getUid() {

        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("query")
    @Expose
    private String query;
}
