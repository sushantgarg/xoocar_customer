package com.xoocar.Requests.SubmitQuerry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/21/17.
 */

public class SubmitQuerryResponceData {
    @SerializedName("token")
    @Expose
    private Integer token;

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }
}
