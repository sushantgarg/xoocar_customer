package com.xoocar.Requests.IntercityDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/10/17.
 */

public class GetIntercityDestsRequestData {

    @SerializedName("city_from")
    @Expose
    private String cityFrom;

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public GetIntercityDestsRequestData(String cityFrom) {

        this.cityFrom = cityFrom;
    }
}
