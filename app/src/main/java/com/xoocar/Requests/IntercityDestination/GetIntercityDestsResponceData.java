package com.xoocar.Requests.IntercityDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/10/17.
 */

public class GetIntercityDestsResponceData {
    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String gettCityName() {
        return tCityName;
    }

    public void settCityName(String tCityName) {
        this.tCityName = tCityName;
    }

    @SerializedName("city_to")
    @Expose
    private String cityTo;
    @SerializedName("t_CityName")
    @Expose
    private String tCityName;

    public String getIs_oneway() {
        return is_oneway;
    }

    public void setIs_oneway(String is_oneway) {
        this.is_oneway = is_oneway;
    }

    private String is_oneway;

}
