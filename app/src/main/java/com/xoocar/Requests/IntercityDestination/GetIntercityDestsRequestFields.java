package com.xoocar.Requests.IntercityDestination;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 10/10/17.
 */

public class GetIntercityDestsRequestFields {

    public GetIntercityDestsRequestFields(GetIntercityDestsRequestData fields) {
        this.fields = fields;
    }

    public GetIntercityDestsRequestData getFields() {

        return fields;
    }

    public void setFields(GetIntercityDestsRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private GetIntercityDestsRequestData fields;

}
