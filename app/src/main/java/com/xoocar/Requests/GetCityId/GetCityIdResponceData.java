package com.xoocar.Requests.GetCityId;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/21/17.
 */

public class GetCityIdResponceData {

    @SerializedName("a_CityId")
    @Expose
    private String aCityId;
    @SerializedName("n_StateId")
    @Expose
    private String nStateId;
    @SerializedName("t_CityName")
    @Expose
    private String tCityName;

    public String getaCityId() {
        return aCityId;
    }

    public void setaCityId(String aCityId) {
        this.aCityId = aCityId;
    }

    public String getnStateId() {
        return nStateId;
    }

    public void setnStateId(String nStateId) {
        this.nStateId = nStateId;
    }

    public String gettCityName() {
        return tCityName;
    }

    public void settCityName(String tCityName) {
        this.tCityName = tCityName;
    }
}
