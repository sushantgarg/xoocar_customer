package com.xoocar.Requests.GetCityId;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.EditDestination.EditDestinationRequestData;

/**
 * Created by sushant on 8/21/17.
 */

public class GetCityIdRequestFields {
    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private String fields;
}
