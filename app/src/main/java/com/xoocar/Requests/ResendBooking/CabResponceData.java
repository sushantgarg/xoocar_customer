package com.xoocar.Requests.ResendBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/10/17.
 */

public class CabResponceData {

    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("driver_id")
    @Expose
    private String driverId;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
