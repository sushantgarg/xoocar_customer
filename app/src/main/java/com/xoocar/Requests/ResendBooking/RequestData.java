package com.xoocar.Requests.ResendBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/10/17.
 */

public class RequestData {

    public RequestData(String rideid, String driverid) {
        this.rideid = rideid;
        this.driverid = driverid;
    }

    @SerializedName("rideid")
    @Expose
    private String rideid;
    @SerializedName("driverid")
    @Expose
    private String driverid;

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }
}
