package com.xoocar.Requests.ResendBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/10/17.
 */

public class RequestFields {
    @SerializedName("fields")
    @Expose
    private RequestData fields;

    public RequestData getFields() {
        return fields;
    }

    public RequestFields(RequestData fields) {
        this.fields = fields;
    }

    public void setFields(RequestData fields) {
        this.fields = fields;
    }
}
