package com.xoocar.Requests.CancelScheduledRide;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.BillDetail.BillDetailsResponceData;

/**
 * Created by sushant on 8/17/17.
 */

public class CancelRideResponceFields {

    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }
}
