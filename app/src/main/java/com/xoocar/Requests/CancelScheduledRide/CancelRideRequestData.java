package com.xoocar.Requests.CancelScheduledRide;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class CancelRideRequestData {

    @SerializedName("rideid")
    @Expose
    private String rideid;

    public CancelRideRequestData(String rideid, String cancelledwhy) {
        this.rideid = rideid;
        this.cancelledwhy = cancelledwhy;
    }

    public String getRideid() {

        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getCancelledwhy() {
        return cancelledwhy;
    }

    public void setCancelledwhy(String cancelledwhy) {
        this.cancelledwhy = cancelledwhy;
    }

    @SerializedName("cancelledwhy")
    @Expose
    private String cancelledwhy;
}
