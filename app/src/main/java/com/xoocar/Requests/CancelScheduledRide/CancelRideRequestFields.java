package com.xoocar.Requests.CancelScheduledRide;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class CancelRideRequestFields {

    public CancelRideRequestData getFields() {

        return fields;
    }

    public void setFields(CancelRideRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private CancelRideRequestData fields;
}
