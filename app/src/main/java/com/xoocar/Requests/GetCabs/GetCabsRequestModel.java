package com.xoocar.Requests.GetCabs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/3/17.
 */

public class GetCabsRequestModel {


    public String getCityid() {
        return cityid;
    }

    private String cityid;

    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("latt")
    @Expose
    private String latt;
    @SerializedName("long")
    @Expose
    private String _long;

    public GetCabsRequestModel( String actMode, String latt, String _long, String cityId) {
        this.actMode = actMode;
        this.latt = latt;
        this._long = _long;
        this.cityid=cityId;
    }

    public String getActMode() {
        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getLatt() {
        return latt;
    }

    public void setLatt(String latt) {
        this.latt = latt;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

}
