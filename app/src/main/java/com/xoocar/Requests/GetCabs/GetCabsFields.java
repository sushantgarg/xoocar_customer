package com.xoocar.Requests.GetCabs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/3/17.
 */

public class GetCabsFields {

    @SerializedName("fields")
    @Expose
    private GetCabsRequestModel fields;

    public GetCabsFields(GetCabsRequestModel fields) {
        this.fields = fields;
    }

    public GetCabsRequestModel getFields() {
        return fields;
    }

    public void setFields(GetCabsRequestModel fields) {
        this.fields = fields;
    }

}
