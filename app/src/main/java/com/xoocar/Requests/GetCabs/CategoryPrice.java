package com.xoocar.Requests.GetCabs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/8/17.
 */

public class CategoryPrice {

    @SerializedName("pc_catid")
    @Expose
    private String pcCatid;
    @SerializedName("pc_cityid")
    @Expose
    private String pcCityid;
    @SerializedName("pc_basefare")
    @Expose
    private String pcBasefare;
    @SerializedName("pc_basefirstkm")
    @Expose
    private String pcBasefirstkm;
    @SerializedName("pc_ridechargepermin")
    @Expose
    private String pcRidechargepermin;
    @SerializedName("pc_conveniencecharge")
    @Expose
    private String pcConveniencecharge;
    @SerializedName("pc_type")
    @Expose
    private String pcType;
    @SerializedName("catname")
    @Expose
    private String catname;
    @SerializedName("cabs")
    @Expose
    private String cabs;
    @SerializedName("rangefrom")
    @Expose
    private String rangefrom;
    @SerializedName("rangeto")
    @Expose
    private String rangeto;
    @SerializedName("charge")
    @Expose
    private String charge;


    public String getPcCatid() {
        return pcCatid;
    }

    public void setPcCatid(String pcCatid) {
        this.pcCatid = pcCatid;
    }

    public String getPcCityid() {
        return pcCityid;
    }

    public void setPcCityid(String pcCityid) {
        this.pcCityid = pcCityid;
    }

    public String getPcBasefare() {
        return pcBasefare;
    }

    public void setPcBasefare(String pcBasefare) {
        this.pcBasefare = pcBasefare;
    }

    public String getPcBasefirstkm() {
        return pcBasefirstkm;
    }

    public void setPcBasefirstkm(String pcBasefirstkm) {
        this.pcBasefirstkm = pcBasefirstkm;
    }

    public String getPcRidechargepermin() {
        return pcRidechargepermin;
    }

    public void setPcRidechargepermin(String pcRidechargepermin) {
        this.pcRidechargepermin = pcRidechargepermin;
    }

    public String getPcConveniencecharge() {
        return pcConveniencecharge;
    }

    public void setPcConveniencecharge(String pcConveniencecharge) {
        this.pcConveniencecharge = pcConveniencecharge;
    }

    public String getPcType() {
        return pcType;
    }

    public void setPcType(String pcType) {
        this.pcType = pcType;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCabs() {
        return cabs;
    }

    public void setCabs(String cabs) {
        this.cabs = cabs;
    }

    public String getRangefrom() {
        return rangefrom;
    }

    public void setRangefrom(String rangefrom) {
        this.rangefrom = rangefrom;
    }

    public String getRangeto() {
        return rangeto;
    }

    public void setRangeto(String rangeto) {
        this.rangeto = rangeto;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }
}
