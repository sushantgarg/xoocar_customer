package com.xoocar.Requests.GetCabs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sushant on 8/3/17.
 */

public class DataArrayGetCabs {


    @SerializedName("drivers_data")
    @Expose
    private List<Driver> driversData = null;
    @SerializedName("category_price")
    @Expose
    private List<CategoryPrice> categoryPrice = null;

    public List<Driver> getDriversData() {
        return driversData;
    }

    public void setDriversData(List<Driver> driversData) {
        this.driversData = driversData;
    }

    public List<CategoryPrice> getCategoryPrice() {
        return categoryPrice;
    }

    public void setCategoryPrice(List<CategoryPrice> categoryPrice) {
        this.categoryPrice = categoryPrice;
    }}
