package com.xoocar.Requests.IsRatingDone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class IsRatingDoneRequestData {
    @SerializedName("customerid")
    @Expose
    private String customerid;

    public IsRatingDoneRequestData(String customerid) {
        this.customerid = customerid;
    }

    public String getCustomerid() {

        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }
}
