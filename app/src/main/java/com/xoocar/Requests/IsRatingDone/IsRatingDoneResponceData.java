package com.xoocar.Requests.IsRatingDone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class IsRatingDoneResponceData {

    @SerializedName("actual_cost")
    @Expose
    private String actualCost;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;

    private String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public IsRatingDoneResponceData(String actualCost, String source, String destination, String firstName, String contact, String profilePic, String vLicenceNo) {
        this.actualCost = actualCost;
        this.source = source;
        this.destination = destination;
        this.firstName = firstName;
        this.contact = contact;
        this.profilePic = profilePic;
        this.vLicenceNo = vLicenceNo;
    }

    public String getActualCost() {

        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getvLicenceNo() {
        return vLicenceNo;
    }

    public void setvLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }
}
