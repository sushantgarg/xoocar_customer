package com.xoocar.Requests.IsRatingDone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class IsRatingDoneRequestFields {
    public IsRatingDoneRequestData getFields() {
        return fields;
    }

    public void setFields(IsRatingDoneRequestData fields) {
        this.fields = fields;
    }

    public IsRatingDoneRequestFields(IsRatingDoneRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose


    private IsRatingDoneRequestData fields;
}
