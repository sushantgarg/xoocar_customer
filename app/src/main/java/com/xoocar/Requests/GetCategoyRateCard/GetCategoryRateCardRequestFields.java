package com.xoocar.Requests.GetCategoyRateCard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.GetCabs.GetCabsRequestModel;

/**
 * Created by sushant on 22/8/17.
 */

public class GetCategoryRateCardRequestFields {
    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private String fields;
}
