package com.xoocar.Requests.AddCouponAfterScheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class AddCouponLaterRequestData {
    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;

    public AddCouponLaterRequestData(String oid, String couponId) {
        this.oid = oid;
        this.couponId = couponId;
    }

    public String getOid() {

        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }
}
