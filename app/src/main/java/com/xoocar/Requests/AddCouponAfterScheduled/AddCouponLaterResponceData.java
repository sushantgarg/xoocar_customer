package com.xoocar.Requests.AddCouponAfterScheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class AddCouponLaterResponceData {

    @SerializedName("rideid")
    @Expose
    private String rideid;

    public AddCouponLaterResponceData(String rideid) {
        this.rideid = rideid;
    }

    public String getRideid() {

        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }
}
