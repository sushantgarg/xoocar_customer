package com.xoocar.Requests.AddCouponAfterScheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/17/17.
 */

public class AddCouponLaterRequestFields {
    public AddCouponLaterRequestFields(AddCouponLaterRequestData fields) {
        this.fields = fields;
    }

    public AddCouponLaterRequestData getFields() {

        return fields;
    }

    public void setFields(AddCouponLaterRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private AddCouponLaterRequestData fields;
}
