package com.xoocar.Requests.GetCoupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/5/17.
 */

public class GetCouponRequest {

    public String getTripType() {
        return ridetype;
    }

    public void setTripType(String tripType) {
        this.ridetype = tripType;
    }

    private  String ridetype;
    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("u_id")
    @Expose
    private String uId;

    private String city_id;

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public GetCouponRequest(String actMode, String uId ,String cityId,String tripType) {
        this.actMode = actMode;
        this.uId = uId;
        city_id=cityId;
        this.ridetype=tripType;
    }

    public String getActMode() {
        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }
}
