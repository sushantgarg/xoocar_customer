package com.xoocar.Requests.GetCoupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/5/17.
 */

public class GetCoupons {
    @SerializedName("fields")
    @Expose
    private GetCouponRequest fields;

    public GetCoupons(GetCouponRequest fields) {
        this.fields=fields;
    }

    public GetCouponRequest getFields() {
        return fields;
    }

    public void setFields(GetCouponRequest fields) {
        this.fields = fields;
    }

}
