package com.xoocar.Requests.GetCoupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/5/17.
 */

public class ResponseDataArray {
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("coupon_name")
    @Expose
    private String couponName;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("coupon_type")
    @Expose
    private String couponType;
    @SerializedName("coupon_value")
    @Expose
    private String couponValue;
    @SerializedName("valid_from")
    @Expose
    private String validFrom;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("is_reusable")
    @Expose
    private String isReusable;
    @SerializedName("reusable_limit")
    @Expose
    private String reusableLimit;

    public int getCat_type() {
        return cat_type;
    }

    public void setCat_type(int cat_type) {
        this.cat_type = cat_type;
    }

    @SerializedName("multiple_limit")

    @Expose
    private String multipleLimit;
    @SerializedName("coupon_status")
    @Expose
    private String couponStatus;
    @SerializedName("coupon_createdon")
    @Expose
    private String couponCreatedon;
    @SerializedName("coupon_modifiedon")
    @Expose
    private Object couponModifiedon;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cnt")
    @Expose
    private String cnt;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("c_desc")
    @Expose
    private String desc;

    // categeory id for which coupon is applicable
    private int cat_type;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getIsReusable() {
        return isReusable;
    }

    public void setIsReusable(String isReusable) {
        this.isReusable = isReusable;
    }

    public String getReusableLimit() {
        return reusableLimit;
    }

    public void setReusableLimit(String reusableLimit) {
        this.reusableLimit = reusableLimit;
    }

    public String getMultipleLimit() {
        return multipleLimit;
    }

    public void setMultipleLimit(String multipleLimit) {
        this.multipleLimit = multipleLimit;
    }

    public String getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(String couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getCouponCreatedon() {
        return couponCreatedon;
    }

    public void setCouponCreatedon(String couponCreatedon) {
        this.couponCreatedon = couponCreatedon;
    }

    public Object getCouponModifiedon() {
        return couponModifiedon;
    }

    public void setCouponModifiedon(Object couponModifiedon) {
        this.couponModifiedon = couponModifiedon;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
