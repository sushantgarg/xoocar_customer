package com.xoocar.Requests.RideHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/20/17.
 */

public class RideHistoryRequestData {

    public RideHistoryRequestData(String actMode, String customerId) {
        this.actMode = actMode;
        this.customerId = customerId;
    }

    public String getActMode() {

        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
}
