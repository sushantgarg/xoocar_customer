package com.xoocar.Requests.RideHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/20/17.
 */

public class RideHIstoryRequestFields {
    public RideHIstoryRequestFields(RideHistoryRequestData fields) {
        this.fields = fields;
    }

    public RideHistoryRequestData getFields() {

        return fields;
    }

    public void setFields(RideHistoryRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private RideHistoryRequestData fields;
}
