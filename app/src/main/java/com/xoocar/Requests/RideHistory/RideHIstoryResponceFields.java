package com.xoocar.Requests.RideHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xoocar.Requests.ResendBooking.CabResponceData;

import java.lang.reflect.GenericDeclaration;
import java.util.List;

/**
 * Created by sushant on 8/20/17.
 */

public class RideHIstoryResponceFields {

    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;

    public RideHIstoryResponceFields(Integer responseCode, Integer status, String mesagges) {
        this.responseCode = responseCode;
        this.status = status;
        this.mesagges = mesagges;
    }

    public Integer getResponseCode() {

        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    @SerializedName("mesagges")
    @Expose
    private String mesagges;

    public List<RideHistoryResponceData> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<RideHistoryResponceData> dataArray) {
        this.dataArray = dataArray;
    }

    @SerializedName("data_array")
    @Expose
    private List<RideHistoryResponceData> dataArray = null;
}
