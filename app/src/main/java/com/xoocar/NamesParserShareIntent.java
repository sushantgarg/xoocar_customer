package com.xoocar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sushant on 8/17/17.
 */

public class NamesParserShareIntent {
    private List<ShareIntentItems> listShareIntents;

    public List<ShareIntentItems> getData(Activity activity) {
        try {
            ShareIntentItems itemShareIntents;
            Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
            sendIntent.setType("image/*");
            List<ResolveInfo> activities = activity.getPackageManager()
                    .queryIntentActivities(sendIntent, 0);

            listShareIntents = new ArrayList<>();

            boolean flag = false;

            for (int i = 0; i < activities.size(); i++) {

                itemShareIntents = new ShareIntentItems();
                ResolveInfo info = activities.get(i);

                final PackageManager pm = activity.getApplicationContext()
                        .getPackageManager();
                ApplicationInfo ai;
                try {
                    ai = pm.getApplicationInfo(
                            info.activityInfo.applicationInfo.packageName, 0);
                } catch (final PackageManager.NameNotFoundException e) {
                    ai = null;
                }
                final String applicationName = (String) (ai != null ? pm
                        .getApplicationLabel(ai) : "(unknown)");
                System.out.println(applicationName);

                itemShareIntents.setImageName(info.activityInfo.applicationInfo
                        .loadIcon(activity.getPackageManager()));

                itemShareIntents.setName(pm.getApplicationLabel(ai).toString());
                itemShareIntents.setPackageName(info.activityInfo.packageName);
                itemShareIntents.setClassName(info.activityInfo.name);

                listShareIntents.add(itemShareIntents);
                if (applicationName.equalsIgnoreCase("facebook")) {
                    flag = true;
                }

            }

            if (!flag) {
                itemShareIntents = new ShareIntentItems();
                itemShareIntents.setImageName(activity.getResources()
                        .getDrawable(R.drawable.ic_plusone_medium_off_client));
                itemShareIntents.setName("facebook");
                itemShareIntents.setPackageName("");
                itemShareIntents.setClassName("");
                listShareIntents.add(itemShareIntents);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return listShareIntents;
    }

    public static String getApplicationName(Context context) {
        int stringId = context.getApplicationInfo().labelRes;
        return context.getString(stringId);
    }

}

