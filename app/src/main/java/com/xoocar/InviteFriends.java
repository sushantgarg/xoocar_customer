package com.xoocar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.xoocar.Requests.SendRefferal.SendReferalRequestData;
import com.xoocar.Requests.SendRefferal.SendReferalRequestFields;
import com.xoocar.Requests.SendRefferal.SendReferalResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 14/8/17.
 */

public class InviteFriends extends AppCompatActivity implements View.OnClickListener {
    private EditText mobileNumber;
    private SessionManager sessionManager;
    private ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitefriends);

        mobileNumber = findViewById(R.id.mobileNumberInviteFrnds);
        ImageView addContactInviteFriends = findViewById(R.id.addContactInviteFriends);
        Button inviteButton = findViewById(R.id.InviteButton);
        Button inviteShare = findViewById(R.id.InviteShare);

        addContactInviteFriends.setOnClickListener(this);
        inviteButton.setOnClickListener(this);
        inviteShare.setOnClickListener(this);

        sessionManager=new SessionManager(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.addContactInviteFriends:
                try {
                    Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(contactPickerIntent, Constants.PICK_CONTACT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.InviteButton:
                if( ! CommonMethods.isValidPhone(mobileNumber.getText().toString()) ) {
                    Toast.makeText(this, "Enter a valid number", Toast.LENGTH_SHORT).show();
                } else {
//                    sendReferal(mobileNumber.getText().toString());
                }
                break;
            case R.id.InviteShare:
                    inviteFriends();

                break;
        }
    }

    private void inviteFriends( ) {

        String shareText="Hurrey! Your friend " + sessionManager.getUserName()+ " has referred you to " +
                "Logo. Get Rs.250 download now." + " https://www.google.com";

        CommonMethods.showShareIntentDialog(shareText, InviteFriends.this);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.d("dkp contact", " reqCode :" + reqCode);
        switch (reqCode) {
            case (Constants.PICK_CONTACT):
                contactPicked(data);
                }
        }


    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            phoneNo=phoneNo.replace(" ","");
            mobileNumber.setText(phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendReferal(String phoneNo) {

        pd=new ProgressDialog(InviteFriends.this);
        pd.setMessage("Verifying details...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<SendReferalResponceFields> call = request.sendReferal(sessionManager.getAuthToken(),"api/processapi/sendreffer/format/json/" ,new SendReferalRequestFields(new SendReferalRequestData(phoneNo,sessionManager.getUserId())));
        call.enqueue(new Callback<SendReferalResponceFields>() {

            @Override
            public void onResponse(Call<SendReferalResponceFields> call, Response<SendReferalResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(InviteFriends.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        mobileNumber.setText("");
                        Toast.makeText(InviteFriends.this, "Referral send successfully", Toast.LENGTH_SHORT).show();
                    }
                }
//
                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SendReferalResponceFields> call, Throwable t) {

                if(pd!= null && pd.isShowing()){
                    pd.dismiss();
                }

            }
        });
    }
}
