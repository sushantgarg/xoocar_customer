package com.xoocar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xoocar.Requests.SubmitQuerry.SubmitQuerryRequestData;
import com.xoocar.Requests.SubmitQuerry.SubmitQuerryRequestFields;
import com.xoocar.Requests.SubmitQuerry.SubmitQuerryResponceFields;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 14/8/17.
 */

public class Support extends AppCompatActivity {

    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        sessionManager=new SessionManager(this);

        final EditText commentBox= (EditText) findViewById(R.id.commentText);
        Button submit= (Button) findViewById(R.id.submitBtnSupport);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitQuerry(commentBox.getText().toString());
            }
        });
    }

    private void submitQuerry(String comment) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<SubmitQuerryResponceFields> call = request.submitQuerry(sessionManager.getAuthToken(),"api/processapi/usersupport/format/json/"
                        ,new SubmitQuerryRequestFields(new SubmitQuerryRequestData(sessionManager.getUserId(),comment)));
        call.enqueue(new Callback<SubmitQuerryResponceFields>() {

            @Override
            public void onResponse(Call<SubmitQuerryResponceFields> call, Response<SubmitQuerryResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(Support.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        confirmDialog("Ticket raised successfully","Your ticket id is: "+response.body().getDataArray().getToken());
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitQuerryResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    public void confirmDialog(String title, String msg) {

        AlertDialog.Builder alert = new AlertDialog.Builder(Support.this);
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setCancelable(false);


        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
                dialog.dismiss();
            }
        });

        alert.show();
    }
}
