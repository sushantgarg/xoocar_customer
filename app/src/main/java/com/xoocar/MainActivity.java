package com.xoocar;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.maps.android.PolyUtil;
import com.xoocar.AsyncTasks.GetGeoCodeAsync;
import com.xoocar.AsyncTasks.GetPathDistance;
import com.xoocar.AsyncTasks.GetpathAsync;
import com.xoocar.Delegates.GeoCodeDelegate;
import com.xoocar.Delegates.GetPathDelegate;
import com.xoocar.EventBusModels.Config;
import com.xoocar.EventBusModels.CouponDetail;
import com.xoocar.EventBusModels.InternetAvailable;
import com.xoocar.EventBusModels.Message;
import com.xoocar.EventBusModels.PaymentType;
import com.xoocar.EventBusModels.RemoveMarker;
import com.xoocar.FireBase.FirebaseManager;
import com.xoocar.NetworkDetector.ConnectivityManager;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneRequestData;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneRequestFields;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneResponceData;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneResponceFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingRequestData;
import com.xoocar.Requests.SubmitRating.SubmitRatingRequetFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingResponce;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.fragments.HomeFragment;
import com.xoocar.fragments.HopNGoFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 2;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION = 3;
    private LinearLayout rideChoicesLayout;
    private ImageView myLocBtn;
    private TextView sourceAddressTextview;
    private TextView destinationAddressTextView;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap gMap;
    private Marker pickupMarker,destinationMarker;
    private Polyline navigationPathPoliline;
    private SessionManager sessionManager;
    private FragmentManager fragmentManager;
    List<LatLng> pointsDelhi = new ArrayList<>();
    List<LatLng> pointsMumbai = new ArrayList<>();
    ConnectivityManager connectivityManager;
    ProgressDialog pd;
    private Dialog dialog;
    private DrawerLayout navigationDrawer;
    private EditText mobileNumberPopUpFriendsNFamily, namePopUpFriendsNFamily;
    private long mBackPressed;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private String currentVersion=null;
    String displayAddress = "";
    private ArrayList<MarkerHash> follMarkersHash;
    private boolean isAlive;
    private ImageButton outStation;
    private TextView outStationText;
    private String selectedCabType="-1";
    private ProgressDialog pd2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        chatAnonymousAuth();
        requestPermissions();
        addPoints();
        buildGoogleApiClient();

        sessionManager=new SessionManager(MainActivity.this);
        IntentFilter filter = new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION);

        follMarkersHash = new ArrayList<>();

        connectivityManager=new ConnectivityManager();
        this.registerReceiver(connectivityManager, filter);

        new GetVersionCode().execute();

        fragmentManager=getSupportFragmentManager();
        ImageView menuIcon =  findViewById(R.id.menuIcon);
        rideChoicesLayout = findViewById(R.id.rideChoices);
        ImageButton rideNowBtn =  findViewById(R.id.rideNow);
        ImageButton bookForFriendBtn = findViewById(R.id.bookForFriend);
        ImageButton hopNGoBtn = findViewById(R.id.hopNGo);

        outStationText= findViewById(R.id.outStationText);

        myLocBtn= findViewById(R.id.myLocBtn);

        LinearLayout inviteFriendsLayout =findViewById(R.id.inviteFriendsLayout);
        LinearLayout rateCard =  findViewById(R.id.rateCardLayout);
        LinearLayout myRides =  findViewById(R.id.myRidesLayout);
        LinearLayout myWalletLayout = findViewById(R.id.myWalletLayout);
        LinearLayout notificationsLayout = findViewById(R.id.notificationsLayout);
        LinearLayout emergencyContactsLayout = findViewById(R.id.emergencyContactsLayout);
        LinearLayout termsAndConditionslayout = findViewById(R.id.termsAndConditionslayout);
        LinearLayout supportLayout =  findViewById(R.id.supportLayout);
        LinearLayout policiesLayout =findViewById(R.id.policiesLayout);
        LinearLayout aboutUsLayout =  findViewById(R.id.aboutUsLayout);
        LinearLayout logOutLayout = findViewById(R.id.logOutLayout);
        LinearLayout offersandFreeRidesLayout = findViewById(R.id.offersandFreeRidesLayout);

        RelativeLayout sourceAddressLayout = findViewById(R.id.source_address_layout);
        RelativeLayout destinationAddressLayout =  findViewById(R.id.destination_address_layout);
        sourceAddressTextview =  findViewById(R.id.source_address_textview);
        TextView username = findViewById(R.id.username);
        destinationAddressTextView = findViewById(R.id.destination_address_textview);
        outStation = findViewById(R.id.outStation);

        navigationDrawer =findViewById(R.id.navigationDrawer);

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.mapFragment);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.mapFragment, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        menuIcon.setOnClickListener(this);
        navigationDrawer.setOnClickListener(this);

        sourceAddressLayout.setOnClickListener(this);
        destinationAddressLayout.setOnClickListener(this);
        rideNowBtn.setOnClickListener(this);
        hopNGoBtn.setOnClickListener(this);
        outStation.setOnClickListener(this);

        inviteFriendsLayout.setOnClickListener(this);
        rateCard.setOnClickListener(this);
        myRides.setOnClickListener(this);
        myWalletLayout.setOnClickListener(this);
        notificationsLayout.setOnClickListener(this);
        emergencyContactsLayout.setOnClickListener(this);
        termsAndConditionslayout.setOnClickListener(this);
        supportLayout.setOnClickListener(this);
        policiesLayout.setOnClickListener(this);
        aboutUsLayout.setOnClickListener(this);
        logOutLayout.setOnClickListener(this);
        offersandFreeRidesLayout.setOnClickListener(this);
        bookForFriendBtn.setOnClickListener(this);
        myLocBtn.setOnClickListener(this);

        username.setText(sessionManager.getUserName());
        getRatingDetail();
        FirebaseManager.getInstance().getConfig();

        Answers.getInstance().logCustom(new CustomEvent("App Open")
                .putCustomAttribute("userId", sessionManager.getUserId())
        );

    }

    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putLong("param", 10000);
        super.onSaveInstanceState(bundle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null ) {
            PackageManager packageManager = MainActivity.this.getPackageManager();
            Intent intent = packageManager.getLaunchIntentForPackage(MainActivity.this.getPackageName());
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
            MainActivity.this.startActivity(mainIntent);
            System.exit(0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAlive=true;
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        assert locationManager != null;
        if ( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER )) {
            showDialogToSettings();
        }
        if( ! EventBus.getDefault().isRegistered(this) )
            EventBus.getDefault().register(MainActivity.this);

        if( pickupMarker != null ) {
            if( checkMarkerDelhi( pickupMarker.getPosition() ) ){
//            getCabsDetail("80",lat,lng);
                FirebaseManager.getInstance().getGeoFireUsers(pickupMarker.getPosition().latitude
                        ,pickupMarker.getPosition().longitude
                        ,MainActivity.this);
            } else if( checkMarkerMumbai(pickupMarker.getPosition()) ){
//            getCabsDetail("71",lat,lng);
                FirebaseManager.getInstance().getGeoFireUsers(pickupMarker.getPosition().latitude
                        ,pickupMarker.getPosition().longitude
                        ,MainActivity.this);
            }
        }
    }

    public void showDialogToSettings() {
        // Build the alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Location not available");
        builder.setMessage("Please enable GPS for accurate path");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user
                // acknowledges the alert dialog
                enableLoc();
            }
        });
        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Show location settings when the user
                        // acknowledges the alert dialog
                        dialogInterface.dismiss();
                    }
                });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    private void enableLoc() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainActivity.this, 101);
                        } catch (IntentSender.SendIntentException ignored) {
                        }
                        break;
                }
            }
        });
    }

    private void getRatingDetail() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<IsRatingDoneResponceFields> call = request.isRatingDone(sessionManager.getAuthToken()
                ,"api/processapi/checkrating/format/json/"
                ,new IsRatingDoneRequestFields(new IsRatingDoneRequestData(sessionManager.getUserId())));
        call.enqueue(new Callback<IsRatingDoneResponceFields>() {

            @Override
            public void onResponse(Call<IsRatingDoneResponceFields> call, Response<IsRatingDoneResponceFields> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(MainActivity.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        showRatingDialog(response.body().getDataArray());
                    }
                }
            }

            @Override
            public void onFailure(Call<IsRatingDoneResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    private void addPoints() {
                    try {
                        JSONArray jsonArray2=new JSONObject(Constants.pathJsonDelhi).getJSONArray("coordinates") ;
                        for ( int i=0;i<jsonArray2.length();i++ ) {
                            JSONArray jArray1= (JSONArray) jsonArray2.get(i);

                            double lat= Double.parseDouble( String.valueOf(jArray1.get(1)));
                            double lng= Double.parseDouble( String.valueOf(jArray1.get(0)));
//                            Log.d("aaaa",jsonArray2.toString());

                            pointsDelhi.add(new LatLng(lat,lng));
                        }
                        JSONArray jsonArray3=new JSONObject(Constants.pathJsonMumbai).getJSONArray("coordinates") ;

                        for ( int i=0;i<jsonArray3.length();i++ ) {
                            JSONArray jArray1= (JSONArray) jsonArray3.get(i);

                            double lat= Double.parseDouble( String.valueOf(jArray1.get(1)));
                            double lng= Double.parseDouble( String.valueOf(jArray1.get(0)));
//                            Log.d("aaaa",jsonArray3.toString());

                            pointsMumbai.add(new LatLng(lat,lng));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.clear();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.source_address_layout:
                onClickSourceAddressLayout();
                break;

            case R.id.destination_address_layout :
                onClickDestAddressLayout();
                break;

            case R.id.rideNow :
                onClickRideNow();
                break;

            case R.id.hopNGo:
                 onClickHopNGo();
                break;

            case R.id. myRidesLayout:
                onClickMyRides();
                break;

            case R.id. rateCardLayout:
                 onClickRateCard();
                break;

            case R.id.myWalletLayout :
                onClickMyWallet();
                break;

            case R.id.offersandFreeRidesLayout :
//                onClickOffersANdRides();
                break;

            case R.id.notificationsLayout :
                onClickNotifications();
                break;

            case R.id.emergencyContactsLayout :
               onClickEmergencyContacts();
                break;
            case R.id. inviteFriendsLayout:
                onClickInviteFriends();
                break;

            case R.id. termsAndConditionslayout:
//                onClickTerms();
                break;

            case R.id. supportLayout:
//                onClickSupport();
                break;

            case R.id. policiesLayout:
//                onClickPolicies();
                break;

            case R.id. aboutUsLayout:
//                onClickAboutUs();
                break;

            case R.id. logOutLayout:
                onClickLogout();
                break;

            case R.id. bookForFriend:
                onClickBookForFriends();
                break;

            case R.id. myLocBtn:
                onClickMyLocation();
                break;

            case R.id. menuIcon:
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                navigationDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.outStation:
               onClickOutStation();
                break;
        }
    }

    private void onClickOutStation() {
        ActivityOptionsCompat options=ActivityOptionsCompat.makeSceneTransitionAnimation(this,null);
        Intent i=new Intent(MainActivity.this,OutStation.class);
        startActivity(i,options.toBundle());
    }

    private void onClickSourceAddressLayout() {
        if (sessionManager.isInternetAvailable()) {
            try {
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setCountry("IN")
                        .build();

                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .setFilter(typeFilter)
                                .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);


            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickDestAddressLayout() {
        if (sessionManager.isInternetAvailable()) {

            try {
                if (pickupMarker != null) {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("IN")
                            .build();

                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setFilter(typeFilter)
                                    .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION);
                } else {
                    Toast.makeText(this, "Enter pickup location first", Toast.LENGTH_SHORT).show();
                }
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickRideNow() {

        Answers.getInstance().logCustom(new CustomEvent("Ride Now")
                .putCustomAttribute("userId", sessionManager.getUserId())
                .putCustomAttribute("Current_lat", sessionManager.getPickupMarkerLat()+","+sessionManager.getPickupMarkerLng())
        );

        if ( sessionManager.isInternetAvailable() ) {
            if( destinationMarker != null ) {
                if( validatePickupAndDest() ) {
                    rideChoicesLayout.setVisibility(View.GONE);
                    myLocBtn.setVisibility(View.GONE);
                    outStation.setVisibility(View.GONE);
                    outStationText.setVisibility(View.GONE);

                    android.support.v4.app.FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();
                    HomeFragment homeFragment = (HomeFragment) fragmentManager.findFragmentByTag("home");
                    if (homeFragment == null) {
                        homeFragment = new HomeFragment();
                        fragmentTransaction1.add(R.id.fragment2, homeFragment, "home");
                    }else{
                        homeFragment.selectFirstFragment();
                    }
                    fragmentTransaction1.show(homeFragment);
                    fragmentTransaction1.commitAllowingStateLoss();
                }
            } else {
                Toast.makeText(this, "Enter drop location", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validatePickupAndDest() {
        boolean retVal = false;
        if( checkMarkerDelhi(pickupMarker.getPosition()) ){
            if( checkMarkerDelhi(destinationMarker.getPosition()) ){
//                success
                sessionManager.setCityId("80");
                retVal=true;
            } else {
                Toast.makeText(this, "Invalid destination location", Toast.LENGTH_SHORT).show();
//                destination not in same city
            }
        } else if( checkMarkerMumbai( pickupMarker.getPosition() ) ) {
            if( checkMarkerMumbai( destinationMarker.getPosition() ) ) {
//                success
                sessionManager.setCityId("71");
                retVal=true;

            } else {
//                destination not in same city
                Toast.makeText(this, "Invalid destination location", Toast.LENGTH_SHORT).show();
            }
        } else {
//            not serving in this region
            Toast.makeText(this, "Invalid pickup location", Toast.LENGTH_SHORT).show();
        }
        return  retVal;
    }

    private void onClickHopNGo() {

        Answers.getInstance().logCustom(new CustomEvent("HopNGo")
                .putCustomAttribute("userId", sessionManager.getUserId())
                .putCustomAttribute("Current_lat", sessionManager.getPickupMarkerLat()+","+sessionManager.getPickupMarkerLng())
        );

        if ( sessionManager.isInternetAvailable() ) {

            if( destinationMarker != null ) {
                if( validatePickupAndDest() ) {

                    Intent pickRide = new Intent(MainActivity.this, HopNGo.class);
                    startActivityForResult(pickRide, Constants.PICK_RIDE_HOPNGO);
                }
            } else {
                Toast.makeText(this, "Enter drop location", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickMyRides() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,MyRidesActivity.class );
            startActivity(i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickRateCard() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,RateCard.class );
            startActivity( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickMyWallet() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,MyWallet.class );
            startActivity ( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickOffersANdRides() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,OffersAndFreeRides.class );
            startActivity (i);
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickNotifications() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,NotificationsClass.class );
            startActivity(i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickEmergencyContacts() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,Emergency_contacts.class );
            startActivity(i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickInviteFriends() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,InviteFriends.class );
            startActivity ( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickTerms() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,WebViewClass.class );
            i.putExtra(Constants.WEB_VIEW_CODE,"3");
            startActivity( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickSupport() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,Support.class );
            startActivity ( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickPolicies() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,WebViewClass.class );
            i.putExtra(Constants.WEB_VIEW_CODE,"2");
            startActivity ( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickAboutUs() {
        if ( sessionManager.isInternetAvailable() ) {
            Intent i =new Intent( MainActivity.this,WebViewClass.class );
            i.putExtra(Constants.WEB_VIEW_CODE,"1");
            startActivity ( i);

        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickLogout() {
        RealmManager.logout();
        sessionManager.logout();
        Intent i=new Intent(getApplicationContext(),Login.class);
        startActivity(i);
    }

    private void onClickBookForFriends() {
        if (sessionManager.isInternetAvailable()) {
            if(destinationMarker != null ) {
                if( validatePickupAndDest() ) {
                    showFriendsNFamily();
                }
            } else {
                Toast.makeText(this, "Enter drop location", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickMyLocation() {
        if( pickupMarker != null ) {

            if( ! sessionManager.getCurrentLat().equals("0") ) {
                sourceAddressTextview.setText(getAddress(new LatLng(Double.parseDouble(sessionManager.getCurrentLat())
                        , Double.parseDouble(sessionManager.getCurrentLng())), "pickup"));

                pickupMarker.setPosition(new LatLng(Double.parseDouble(sessionManager.getCurrentLat())
                        , Double.parseDouble(sessionManager.getCurrentLng())));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(Double.parseDouble(sessionManager.getCurrentLat())
                                , Double.parseDouble(sessionManager.getCurrentLng())))
                        .zoom(15)
                        .build();
                gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                getCabs(Double.parseDouble(sessionManager.getCurrentLat())
                        , Double.parseDouble(sessionManager.getCurrentLng())
                );

                if( destinationMarker != null ) {
                    getPath(pickupMarker.getPosition(), destinationMarker.getPosition());
                }

            } else {
                Toast.makeText(this, "Location not available", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Location not available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                try {
                        sessionManager.setIsValidPickupLocation(true);

                        String sourceAddress= (String) place.getAddress();
                        sourceAddressTextview.setText(sourceAddress);
                        sessionManager.setPickupAddress(sourceAddress);
                        sessionManager.setPickupMarkerLat(place.getLatLng().latitude);
                        sessionManager.setPickupMarkerLng(place.getLatLng().longitude);

                        addPickupMarker(place.getLatLng());
                        getCabs(place.getLatLng().latitude,place.getLatLng().longitude);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Toast.makeText(this, "No place found please try again", Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                addDestinationMarker(place);
                //                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
//                Log.i(TAG, status.getStatusMessage());
                Toast.makeText(this, "No place found please try again", Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (resultCode == Constants.PICK_COUPONS) {
            // Make sure the request was successful
            EventBus.getDefault().post(new CouponDetail(data.getStringExtra(Constants.COUPON_NAME)
                    ,data.getStringExtra(Constants.COUPON_VALUE)
                    ,data.getStringExtra(Constants.COUPON_ID)
                    ,data.getStringExtra(Constants.COUPON_SOURCE)
                    ));

        } else if (resultCode == Constants.PICK_RIDE_HOPNGO) {
            // Make sure the request was successful
            android.support.v4.app.FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();

            HopNGoFragment hopNGoFragment=new HopNGoFragment();
            Bundle bundle=new Bundle();
            bundle.putString(Constants.HOPNGO_CAB_DETAIL,data.getStringExtra(Constants.HOPNGO_CAB_DETAIL));
            hopNGoFragment.setArguments(bundle);
            fragmentTransaction1.replace(R.id.fragment2,hopNGoFragment, "hopngo");
            fragmentTransaction1.commitAllowingStateLoss();
            rideChoicesLayout.setVisibility(View.GONE);
            myLocBtn.setVisibility(View.GONE);
            outStationText.setVisibility(View.GONE);
        } else if (resultCode == Constants.PAYMENT_CODE) {
            // Make sure the request was successful
            String paymentType=data.getStringExtra(Constants.PAYMENT_TYPE);
            EventBus.getDefault().post(new PaymentType(paymentType.toLowerCase()));
        } else if (requestCode == Constants.PICK_CONTACT) {
                contactPicked(data);
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            phoneNo=phoneNo.replace(" ","");
            mobileNumberPopUpFriendsNFamily.setText(phoneNo);
            namePopUpFriendsNFamily.setText(name);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onLocationChanged(Location location) {
        sessionManager.setCurrentLat(location.getLatitude());
        sessionManager.setCurrentLng(location.getLongitude());

        try {
            if(sessionManager.isInternetAvailable()) {
                String sourceAddress=getAddress(new LatLng(location.getLatitude(),location.getLongitude()), "pickup");
                sourceAddressTextview.setText(sourceAddress);

                    sessionManager.setPickupAddress(sourceAddress);
                    sessionManager.setPickupMarkerLat(location.getLatitude());
                    sessionManager.setPickupMarkerLng(location.getLongitude());

                    addPickupMarker(new LatLng(location.getLatitude()
                            , location.getLongitude()));
                    sessionManager.setIsValidPickupLocation(true);
                    getCabs(location.getLatitude(),location.getLongitude());

            } else {
                Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            Toast.makeText(this, "Internet connection too slow", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(15)
                    .build();
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private boolean checkMarkerDelhi(LatLng location) {
        return PolyUtil.containsLocation(new LatLng(location.latitude,location.longitude), pointsDelhi, true);
    }

    private boolean checkMarkerMumbai(LatLng position) {
        return PolyUtil.containsLocation(new LatLng(position.latitude,position.longitude), pointsMumbai, true);
    }

    private void getCabs(double lat,double lng) {
        if( checkMarkerDelhi( pickupMarker.getPosition() ) ){
//            getCabsDetail("80",lat,lng);
            FirebaseManager.getInstance().getPrizing("80");
            FirebaseManager.getInstance().getGeoFireUsers(lat,lng,MainActivity.this);
            sessionManager.setCityId("80");
        } else if( checkMarkerMumbai(pickupMarker.getPosition()) ){
//            getCabsDetail("71",lat,lng);
            FirebaseManager.getInstance().getPrizing("71");
            FirebaseManager.getInstance().getGeoFireUsers(lat,lng,MainActivity.this);
            sessionManager.setCityId("71");
        } else {
            Toast.makeText(this, "Currently not serving in this city", Toast.LENGTH_SHORT).show();
        }
    }

    private void addDestinationMarker(Place place) {

            sessionManager.setIsValidDestination(true);
            getPath(pickupMarker.getPosition(), place.getLatLng());

            String destinationAddress= (String) place.getAddress();
            sessionManager.setDestinationAddress(destinationAddress);
            destinationAddressTextView.setText(destinationAddress);

            sessionManager.setDestLat(place.getLatLng().latitude);
            sessionManager.setDestLng(place.getLatLng().longitude);

            if ( destinationMarker != null ) {
                destinationMarker.remove();
                destinationMarker = null;
            }

            Drawable mDrawable = getResources().getDrawable(R.drawable.dropoff_homescreen_svg);
            final Bitmap b= CommonMethods.convertDrawableToBitmap(mDrawable);

            destinationMarker = gMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(b))
                    .position(place.getLatLng())
                    .title("Destination")
            );

            destinationMarker.setDraggable(true);


            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            builder.include(pickupMarker.getPosition());
            builder.include(destinationMarker.getPosition());

            LatLngBounds bounds = builder.build();

            int width = 1250;
            int height = 1920;
            int padding = (int) (width * 0.32); // offset from edges of the map 12% of screen

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds( bounds, width, height, padding );
            gMap.animateCamera(cu);



    }

    private void getPath(LatLng position, LatLng latLng) {

        if( pd != null && pd.isShowing() ){
            pd.dismiss();
            pd=null;
        }

        pd=new ProgressDialog(MainActivity.this);
        pd.setMessage("Building path ...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        getPathDistance( position,latLng );

        new GetpathAsync(position, MainActivity.this ,latLng,new GetPathDelegate() {
            @Override
            public void processFinish(List<LatLng> encodedPath) {

                if( pd != null && pd.isShowing() )
                    pd.dismiss();
                if( encodedPath.size() < 1){
//                    Todo : draw arc
//                    addCurvedPath();
//                    http://maps.googleapis.com/maps/api/distancematrix/json?origins=28.7321,77.55487&destinations=28.14525,77.570993&mode=driving&language=en-EN&sensor=true


                } else {
                    EventBus.getDefault().post(new Message("RefreshCabs"));
                    drawNavigationPath(encodedPath);
                }
            }
        }).execute();
    }

    private void getPathDistance(LatLng position, LatLng latLng) {

        new GetPathDistance( position, MainActivity.this ,latLng)
                .execute();
    }

    private void drawNavigationPath(List<LatLng> encodedPath) {

        if( navigationPathPoliline != null && navigationPathPoliline.getPoints().size() > 0 ) {
            navigationPathPoliline.remove();
        }
        PolylineOptions lineOptions = null;
        lineOptions = new PolylineOptions();

        lineOptions.addAll(encodedPath);
        navigationPathPoliline=gMap.addPolyline(lineOptions);
    }

    private void addPickupMarker(LatLng location) throws IOException {

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.latitude, location.longitude))
                .zoom(15)
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if( pickupMarker != null ){
            pickupMarker.remove();
            pickupMarker = null;
        }

        if( destinationMarker != null ) {
            getPath(location,destinationMarker.getPosition());
        }

        Drawable mDrawable = getResources().getDrawable(R.drawable.pickup_homescreen_svg);
        final Bitmap b= CommonMethods.convertDrawableToBitmap(mDrawable);

        pickupMarker = gMap.addMarker(new MarkerOptions()
                    .position(new LatLng(location.latitude, location.longitude))
                    .icon(BitmapDescriptorFactory.fromBitmap(b))
                    .title("Pickup Location")
            );


        pickupMarker.setDraggable(true);

        gMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

                String address= null;

                    if(marker.getTitle().equals("Destination")) {
                        address=getAddress(marker.getPosition(),"destination");
                        destinationAddressTextView.setText(address);
                        sessionManager.setDestinationAddress(address);
                        getPath( pickupMarker.getPosition(), marker.getPosition() );
                        sessionManager.setDestLat( marker.getPosition().latitude );
                        sessionManager.setDestLng( marker.getPosition().longitude);
                        sessionManager.setIsValidDestination(true);
                    } else {
                        /*
                        PICKUP MARKER
                        */
                        address=getAddress(marker.getPosition(),"pickup");
                        sourceAddressTextview.setText(address);
                        sessionManager.setPickupAddress(address);
                        sessionManager.setPickupMarkerLat(marker.getPosition().latitude);
                        sessionManager.setPickupMarkerLng(marker.getPosition().longitude);
                        getCabs(marker.getPosition().latitude,marker.getPosition().longitude);
                        sessionManager.setIsValidPickupLocation(true);

                        if (destinationMarker != null) {
                            getPath(marker.getPosition(), destinationMarker.getPosition());
                        }
                    }
            }
        });

    }

    private String getAddress(LatLng location, final String destination)  {

        new GetGeoCodeAsync(location, new GeoCodeDelegate() {
            @Override
            public void processFinish(String encodedPath, String shortName) {
//                Log.d("aaaa",encodedPath);
                displayAddress= encodedPath;
                if(destination.equals("pickup")) {
                    sourceAddressTextview.setText(displayAddress);
                    sessionManager.setPickupAddress(displayAddress);
//                    sessionManager.savePickupState(shortName);
                } else {
//                 sessionManager.saveDropState(shortName);
                    sessionManager.setDestinationAddress(displayAddress);
                 destinationAddressTextView.setText(displayAddress);
                }
            }
        }).execute();
        return  displayAddress;
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();
        Fragment homeFragment = fragmentManager.findFragmentByTag("home");
        Fragment hopngoFragment = fragmentManager.findFragmentByTag("hopngo");

        if( homeFragment != null ) {
            if( homeFragment.isHidden() ) {
                if ( hopngoFragment != null ){
//            homefragment null but hop fragent alive
                    if( hopngoFragment.isHidden() ){

                        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            return;
                        } else {
                            Toast.makeText(getBaseContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
                        }
                        mBackPressed = System.currentTimeMillis();

                    } else {
                        fragmentTransaction1.hide(hopngoFragment);
                        fragmentTransaction1.commitAllowingStateLoss();
                        rideChoicesLayout.setVisibility(View.VISIBLE);
                        myLocBtn.setVisibility(View.VISIBLE);
                        outStationText.setVisibility(View.VISIBLE);
                        outStation.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                fragmentTransaction1.hide(homeFragment);
                fragmentTransaction1.commitAllowingStateLoss();
                rideChoicesLayout.setVisibility(View.VISIBLE);
                myLocBtn.setVisibility(View.VISIBLE);
                outStationText.setVisibility(View.VISIBLE);
                outStation.setVisibility(View.VISIBLE);
                sessionManager.setFrndName("");
                sessionManager.setFrndNumber("");

                selectedCabType="-1";
                filterCabMarkers("-1");
            }
        } else if ( hopngoFragment != null ) {
//            homefragment null but hop fragent alive
            if( hopngoFragment.isHidden() ){
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    return;

                } else {
                    Toast.makeText(getBaseContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
                }
                mBackPressed = System.currentTimeMillis();
            } else {
                fragmentTransaction1.hide(hopngoFragment);
                fragmentTransaction1.commitAllowingStateLoss();
                rideChoicesLayout.setVisibility(View.VISIBLE);
                myLocBtn.setVisibility(View.VISIBLE);
                outStation.setVisibility(View.VISIBLE);
                outStationText.setVisibility(View.VISIBLE);
            }
        } else {
//            both fragment null
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return;
            } else {
                Toast.makeText(getBaseContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
            }
            mBackPressed = System.currentTimeMillis();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
       requestLocationUpdates();
    }

    private void requestLocationUpdates() {
        LocationRequest mLocationRequest1 = new LocationRequest();
        mLocationRequest1.setInterval(60*1000); //35 seconds
        mLocationRequest1.setFastestInterval(60*1000); //35 seconds
        mLocationRequest1.setSmallestDisplacement(10);
        mLocationRequest1.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }

        Location loc=LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if( loc != null && loc.getLatitude() != 0 ) {
            sessionManager.setCurrentLat(loc.getLatitude());
            sessionManager.setCurrentLng(loc.getLongitude());

            String sourceAddress= null;
            try {
                sourceAddress = getAddress(new LatLng(loc.getLatitude(),loc.getLongitude()), "pickup");
                sourceAddressTextview.setText(sourceAddress);

                sessionManager.setPickupAddress(sourceAddress);

                sessionManager.setPickupMarkerLat(loc.getLatitude());
                sessionManager.setPickupMarkerLng(loc.getLongitude());

                addPickupMarker(new LatLng(loc.getLatitude()
                        , loc.getLongitude()));
                sessionManager.setIsValidPickupLocation(true);
                getCabs(loc.getLatitude(),loc.getLongitude());

            } catch (IOException e) {
                e.printStackTrace();
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(loc.getLatitude(), loc.getLongitude()))
                    .zoom(15)
                    .build();
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest1,  this);
    }

    private void stopLocationUpdates() {
        if( mGoogleApiClient.isConnected() ) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
//        Log.d("aaaa","connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("aaaa","connection failure");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CouponDetail coupon) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CabsRealm cab) {
        addCab(cab);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Message coupon) {
        /* Do something */
        switch ( coupon.getMessage() ) {
            case "HideHopnGo":
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment hopngoFragment = fragmentManager.findFragmentByTag("hopngo");

                fragmentTransaction.hide(hopngoFragment);
                fragmentTransaction.commitAllowingStateLoss();
                rideChoicesLayout.setVisibility(View.VISIBLE);
                myLocBtn.setVisibility(View.GONE);
                outStation.setVisibility(View.GONE);
                outStationText.setVisibility(View.GONE);
                break;

            case "HideRideNow":
                Fragment homeFragment = fragmentManager.findFragmentByTag("home");
                android.support.v4.app.FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();
//                super.onBackPressed();
                if ( homeFragment != null ) {
//            homefragment null but hop fragent alive
                    if (!homeFragment.isHidden()) {
                        fragmentTransaction1.hide(homeFragment);
                        fragmentTransaction1.commitAllowingStateLoss();
                        rideChoicesLayout.setVisibility(View.VISIBLE);
                        myLocBtn.setVisibility(View.VISIBLE);
                        outStationText.setVisibility(View.VISIBLE);
                        outStation.setVisibility(View.VISIBLE);
                    }
                }

                break;

            case "clearCabMarkers":
                RealmManager.deleteCabs();
                removeCabMarkers();
                showPd();
                break;

            case "hide_pd":
                hidePd();
                break;

            case "0":
                selectedCabType="13";
                filterCabMarkers("13");
                break;

            case "1":
                selectedCabType="1";
                filterCabMarkers("1");
                break;

            case "2":
                selectedCabType="2";
                filterCabMarkers("2");
                break;

            case "3":
                selectedCabType="3";
                filterCabMarkers("3");
                break;

            case "4":
                selectedCabType="6";
                filterCabMarkers("6");
                break;
        }
    }

    private void hidePd() {
        if( pd2 != null && pd2.isShowing() ){
            pd2.dismiss();
            pd2=null;
        }
     }

    private void showPd() {

        hidePd();
        pd2=new ProgressDialog(MainActivity.this);
        pd2.setMessage("Getting cabs ...");
        pd2.setCanceledOnTouchOutside(false);
        pd2.show();
    }

    private void filterCabMarkers(String s) {
        removeCabMarkers();
        if(s.equals("-1")){
            for (CabsRealm cab : RealmManager.getAllCabs()) {
                addCab(cab);
            }
        } else {
            for (CabsRealm cab : RealmManager.getCabByCategory(s)) {
                addCab(cab);
            }
        }
    }

    private void removeCabMarkers() {
        for (MarkerHash marker:
                follMarkersHash) {
            marker.getMarker().remove();
        }
        follMarkersHash.clear();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(InternetAvailable status) {
        /* Do something */
        if(status.isInternetAvailable()){
            if( ! mGoogleApiClient.isConnected() ) {
                mGoogleApiClient.connect();
            }else{
                requestLocationUpdates();
            }
        }else{
            Toast.makeText(this, "Internet connection lost", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemoveMarker markerObj) {
        /* Do something */

        synchronized (MainActivity.class) {
            if( follMarkersHash != null && follMarkersHash.size() > 0 ) {
                for ( MarkerHash marker:
                        follMarkersHash ) {
                    if ( marker.getUserId()!= null && marker.getUserId().equals(markerObj.getDriverId()) ) {
                        marker.getMarker().remove();
                        follMarkersHash.remove(marker);
                    }
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Config config) {
        /* Do something */
      sessionManager.setGeoradius(String.valueOf(config.getRadius()));
      sessionManager.setCancellationFee(config.getCancellation_charge());
//        if( sessionManager.getBannerId() != config.getBannerId() ) {
//            showOfferDialog(config.getAndroidBannerUrl());
//            sessionManager.setBannerId(config.getBannerId());
//        }
    }

    public void addCab( CabsRealm cab ) {

        Bitmap b=CommonMethods.getCabIcon(cab.getCabType(),MainActivity.this);

        if( markerAdded(cab) && isAlive ){
            animateMarler(cab);
        } else {
            if( selectedCabType.equals("-1") || selectedCabType.equals(cab.getCabType())) {
                follMarkersHash.add(new MarkerHash(cab.getUid(), gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(cab.getdLatt()), Double.parseDouble(cab.getdLong())))
                        .icon(BitmapDescriptorFactory.fromBitmap(b))
                        .rotation(Float.parseFloat(cab.getCourse()))
                )));
            }
        }
    }

    private void animateMarler(CabsRealm cab) {
        for (MarkerHash marker :
                follMarkersHash) {
            if (marker.getUserId().equals(cab.getUid())){
                Marker mar=marker.getMarker();
                mar.setRotation(Float.parseFloat(cab.getCourse()));

                CommonMethods.animateMarkerToNewLoaction(mar
                        ,Double.parseDouble(cab.getdLatt())
                        ,Double.parseDouble(cab.getdLong())
                );
            }
        }
    }

    private boolean markerAdded(CabsRealm cab) {

        boolean isAdded=false;

        for (MarkerHash marker:follMarkersHash) {
            if(marker.getUserId().equals(cab.getUid())){
                isAdded=true;
                break;
            }
        }
        return isAdded;
    }

    public void requestPermissions() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
        Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

        // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
                                ,Manifest.permission.INTERNET
                                ,Manifest.permission.CALL_PHONE
                                ,Manifest.permission.READ_CONTACTS
                                ,Manifest.permission.READ_SMS
                                ,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
    }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                        requestLocationUpdates();
                    } else {
                        buildGoogleApiClient();
                    }

                } else {
                    Toast.makeText(this, "XooCar will not be able to fetch your pickup location", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lianes to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectivityManager != null) {
            this.unregisterReceiver( connectivityManager );
        }
        mGoogleApiClient.unregisterConnectionCallbacks(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isAlive=false;

        FirebaseManager.getInstance().removeGeoFireListener();

        if( EventBus.getDefault().isRegistered(this) )
            EventBus.getDefault().unregister(this);

        stopLocationUpdates();
    }

    private void showRatingDialog(final IsRatingDoneResponceData dataArray) {
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rating_screen);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        final EditText comment;
        final RatingBar ratingBar;
        final TextView driverName,vehicleName,amount,pickup,destination;

        comment=dialog.findViewById(R.id.comment);
        ratingBar=dialog.findViewById(R.id.ratingBar);

        driverName=dialog.findViewById(R.id.driverNameRating);
        vehicleName=dialog.findViewById(R.id.vehicleNumberRating);
        amount=dialog.findViewById(R.id.amountPaidrating);
        pickup=dialog.findViewById(R.id.source_address_rating);
        destination=dialog.findViewById(R.id.destination_address_rating);

        driverName.setText(dataArray.getFirstName());
        vehicleName.setText(dataArray.getvLicenceNo());
        amount.setText(getString(R.string.rupee_symbol)+" "+ dataArray.getActualCost());
        pickup.setText(dataArray.getSource());
        destination.setText(dataArray.getDestination());

        dialog.findViewById(R.id.submitRating).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitRating(ratingBar.getRating(),comment.getText().toString(),dataArray.getOid());
                    }
                });

        dialog.show();
    }

    public void showOfferDialog(String url) {

        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_mumbai);

        ImageView image=dialog.findViewById(R.id.imagePopUp);

        Glide.with(MainActivity.this)
                .load(url)
                .into(image);

        dialog.show();
    }

    private void showFriendsNFamily() {
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_friendsnfamily);

        mobileNumberPopUpFriendsNFamily=dialog.findViewById(R.id.mobileNumberPopUpFriendsNFamily);
        namePopUpFriendsNFamily=dialog.findViewById(R.id.namePopUpFriendsNFamily);

        dialog.findViewById(R.id.submitButtonHopUpFriendsNFamily).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                 String name=namePopUpFriendsNFamily.getText().toString();
                 String number=mobileNumberPopUpFriendsNFamily.getText().toString();
                        if(CommonMethods.isValidPhone(number)){
                                if(CommonMethods.validateName(name)){
                                    sessionManager.setFrndName(name);
                                    sessionManager.setFrndNumber(number);

                                    rideChoicesLayout.setVisibility(View.GONE);
                                    myLocBtn.setVisibility(View.GONE);
                                    outStation.setVisibility(View.GONE);
                                    outStationText.setVisibility(View.GONE);

                                    android.support.v4.app.FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();
                                    Fragment homeFragment = fragmentManager.findFragmentByTag("home");
                                    if (homeFragment == null) {
                                        homeFragment = new HomeFragment();
                                        fragmentTransaction1.add(R.id.fragment2, homeFragment, "home");
                                    }
                                    fragmentTransaction1.show(homeFragment);
                                    fragmentTransaction1.commitAllowingStateLoss();
                                }else{
                                    Toast.makeText(MainActivity.this, "Invalid name", Toast.LENGTH_SHORT).show();
                                }
                        }else {
                            Toast.makeText(MainActivity.this, "Invalid number", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();

                    }
                });

        dialog.findViewById(R.id.addContactPopUpFriendsNFamily).setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                 showContacts();
                    }
                });
        dialog.show();

    }

    private void showContacts() {
        try {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, Constants.PICK_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void submitRating(final float rating,final String s,final String rideId) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<SubmitRatingResponce> call = request.submitrating(sessionManager.getAuthToken()
                ,"api/processapi/insertrating/format/json/"
                ,new SubmitRatingRequetFields(new SubmitRatingRequestData(rideId
                        ,String.valueOf(rating),s)));
        call.enqueue(new Callback<SubmitRatingResponce>() {

            @Override
            public void onResponse(Call<SubmitRatingResponce> call, Response<SubmitRatingResponce> response) {
                Log.d("aaaa",response.toString());
                if( response.body() != null ) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(MainActivity.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        Toast.makeText(MainActivity.this, "Rating submitted successfully", Toast.LENGTH_SHORT).show();
                        getRatingDetail();
                    }
                }
            }

            @Override
            public void onFailure(Call<SubmitRatingResponce> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;

            try {
                currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    ShowDialog(MainActivity.this);
                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }

    public void ShowDialog(Activity mMainActivity) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        final Dialog imageDialog = new Dialog(mMainActivity);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setCancelable(false);
        imageDialog.setCanceledOnTouchOutside(false);
        imageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) mMainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.layout_update_dialoge, null);
        TextView mTextView = (TextView) layout.findViewById(R.id.id_network_ok);
        TextView mTextmsg = (TextView) layout.findViewById(R.id.id_network_not);

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.xoocar&hl=en"));
                startActivity(in);
                imageDialog.dismiss();
            }
        });
        layout.setLayoutParams(param);
        imageDialog.setContentView(layout, param);
        imageDialog.show();
    }

    private void chatAnonymousAuth() {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if ( task.isSuccessful() ) {

                        }
                    }
                });
    }

}
