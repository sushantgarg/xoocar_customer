package com.xoocar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.xoocar.Adapter.CouponAdapter;
import com.xoocar.Delegates.AdapterToMain;
import com.xoocar.Requests.GetCoupons.CouponResponceFields;
import com.xoocar.Requests.GetCoupons.GetCouponRequest;
import com.xoocar.Requests.GetCoupons.GetCoupons;
import com.xoocar.Requests.GetCoupons.ResponseDataArray;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;
import com.xoocar.RetroFit.RequestInterface;
import com.xoocar.SessionManager.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 3/8/17.
 */

public class CouponActivity extends AppCompatActivity  implements AdapterToMain {
    private Context ctx;
    private RecyclerView couponList;
    private CouponAdapter adapter;
    private ArrayList<ResponseDataArray> couponsList;
    private SessionManager sessionManager;
    private ImageView noCouponScreen;
    private int categoryId;
    private String couponSource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupan_activity);
        sessionManager=new SessionManager(this);
        ctx=CouponActivity.this;

        Intent i=getIntent();
        String tripType=i.getStringExtra(Constants.TRIPTYPE);
        couponSource=i.getStringExtra(Constants.COUPON_SOURCE);
        categoryId= Integer.parseInt(i.getStringExtra(Constants.VEHICLE_TYPE));


        getCoupons(tripType);

        couponList = findViewById(R.id.couponList);
        couponList.setLayoutManager(new LinearLayoutManager(ctx));
        noCouponScreen=  findViewById(R.id.noCoupon_screen);
    }

    public void getCoupons(String tripType) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.base)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CouponResponceFields> call = request.getCoupons(sessionManager.getAuthToken(),"api/newloginapi/sharecopon/format/json/"
                ,new GetCoupons(new GetCouponRequest("sharecopon",sessionManager.getUserId(),sessionManager.getCityId(),tripType)));
        call.enqueue(new Callback<CouponResponceFields>() {

            @Override
            public void onResponse(@NonNull Call<CouponResponceFields> call, @NonNull Response<CouponResponceFields> response) {
                if (response.body() != null) {
                    if (response.body().getResponseCode() == 410) {
                        Toast.makeText(CouponActivity.this, "Session expired", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponseCode() == 400) {
                        if (response.body().getDataArray() != null && response.body().getDataArray().size() > 0) {
                            couponsList = (ArrayList<ResponseDataArray>) response.body().getDataArray();

                            ArrayList<ResponseDataArray> todaysCoupon=new ArrayList<ResponseDataArray>();

                            /* filtering coupons
                               for today and
                               category id
                             */

                            if( categoryId == 13 ) {
                                for (ResponseDataArray coupon:couponsList
                                        ) {
                                    if( coupon.getTag().equals("today")  &&  coupon.getCat_type() == categoryId ) {
                                        todaysCoupon.add(coupon);
                                    }
                                }
                            } else {
                                for (ResponseDataArray coupon:couponsList
                                        ) {
                                    if( coupon.getTag().equals("today")  && ( coupon.getCat_type() == 0 || coupon.getCat_type() == categoryId ) ) {
                                        todaysCoupon.add(coupon);
                                    }
                                }
                            }

                            if( todaysCoupon.size() > 0 ) {
                                adapter = new CouponAdapter(todaysCoupon, ctx);
                                couponList.setAdapter(adapter);
                            } else {
                                showNoCouponsScreen();
                            }
                        }
                    } else {
                        showNoCouponsScreen();
                    }
                } else {
                    showNoCouponsScreen();
                }
            }

            @Override
            public void onFailure(Call<CouponResponceFields> call, Throwable t) {
                Log.d("aaaa","error");
            }
        });
    }

    private void showNoCouponsScreen() {
        noCouponScreen.setVisibility(View.VISIBLE);
    }

    @Override
    public void addCoupon(ResponseDataArray coupon) {
        Intent i=new Intent();
        i.putExtra(Constants.COUPON_NAME,coupon.getCouponName());
        i.putExtra(Constants.COUPON_VALUE,coupon.getCouponValue());
        i.putExtra(Constants.COUPON_ID,coupon.getCouponId());
        i.putExtra(Constants.COUPON_SOURCE,couponSource);
        setResult(Constants.PICK_COUPONS,i);
        finish();
    }

    @Override
    public void rideDetail(CabDetailHopNGo cab) {
//        do nothing
    }
}
