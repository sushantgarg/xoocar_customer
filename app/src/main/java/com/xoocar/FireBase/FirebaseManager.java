package com.xoocar.FireBase;

import android.content.Context;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.xoocar.EventBusModels.Config;
import com.xoocar.EventBusModels.Message;
import com.xoocar.EventBusModels.RemoveMarker;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.models.CabPojo;
import com.xoocar.models.CabPricing;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by sushant on 9/5/17.
 */

public class FirebaseManager {
    //same for leader and follower
    private DatabaseReference rootReference;

    private volatile static FirebaseManager instance;
    private GeoQuery geoQuery;

    public static FirebaseManager getInstance() {
        if(instance == null) {
            synchronized (FirebaseManager.class) {
                instance = new FirebaseManager();
            }
        }
        return instance;
    }

    private FirebaseManager() {
        // Constructor hidden because this is a singleton
        rootReference= FirebaseDatabase.getInstance().getReference();
    }

    public void getGeoFireUsers(double lat, double lng, final Context ctx) {
        GeoFire geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("geo_fire"));
        if( geoQuery != null ){
            geoQuery.removeAllListeners();
        }

         geoQuery = geoFire.queryAtLocation(new GeoLocation(lat, lng), new SessionManager(ctx).getGeoRadius());
        EventBus.getDefault().post(new Message("clearCabMarkers"));

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                Log.d("aaaa-key_entered "+key ,location.latitude+" "+location.longitude);
                getCabDetail(key,ctx);
            }

            @Override
            public void onKeyExited(String key) {
                Log.d("aaaa-exited",key);
                EventBus.getDefault().post(new RemoveMarker(key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                getCabDetail(key,ctx);
                Log.d("aaaa-key_moved "+key,location.latitude+" "+location.longitude);
            }

            @Override
            public void onGeoQueryReady() {
                EventBus.getDefault().post(new Message("hide_pd"));
                Log.d("aaaa-key_entered","ready");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                System.err.println("There was an error with this query: " + error);
            }
        });

    }

    private void getCabDetail(String key, final Context ctx) {
        rootReference.child("Drivers").child(key).orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    CabPojo cab = new Gson().fromJson(new Gson().toJson(dataSnapshot.getValue()), CabPojo.class);

                    if( cab.getStatus().equals("1") ) {
                        CabsRealm cabRealm = new CabsRealm(cab);
                        RealmManager.addCab(cabRealm, ctx);
                        EventBus.getDefault().post(cabRealm);
                    }
                } catch (Exception e) {
                    Log.d("aaaa","firebase error 8");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getPrizing(String s) {

        rootReference.child("Pricing").child(s).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapsho:dataSnapshot.getChildren()) {
                    CabPricing price=new Gson().fromJson(dataSnapsho.getValue().toString(),CabPricing.class);
                    price.setCityId(dataSnapshot.getKey());
                    RealmManager.addCategory(price,dataSnapshot.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getCabPosition(String driverId) {
        ValueEventListener cabPosition = rootReference.child("Drivers").child(driverId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    CabPojo cab = new Gson().fromJson(new Gson().toJson(dataSnapshot.getValue()), CabPojo.class);
                    CabsRealm cabRealm = new CabsRealm(cab);
                    EventBus.getDefault().post(cabRealm);
                } catch (Exception e) {
                    Log.d("aaaa", "error in firebase 7");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getConfig() {
        rootReference.child("basic_configuration").orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    Config config = new Gson().fromJson(new Gson().toJson(dataSnapshot.getValue()), Config.class);
                    EventBus.getDefault().post(config);
                } catch (Exception e) {
                    Log.d("aaaa","firebase error 8");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void removeGeoFireListener() {
        if(geoQuery != null) geoQuery.removeAllListeners();
    }
}
