package com.xoocar.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xoocar.CommonMethods;
import com.xoocar.Constants;
import com.xoocar.CouponActivity;
import com.xoocar.EventBusModels.CouponDetail;
import com.xoocar.EventBusModels.Message;
import com.xoocar.EventBusModels.PaymentType;
import com.xoocar.PaymentActivity;
import com.xoocar.R;
import com.xoocar.Realm.RealmManager;
import com.xoocar.Requests.HopNGo.CabDetailHopNGo;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.WaitingScreenHopNGo;
import com.xoocar.models.CabPricing;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by sushant on 8/9/17.
 */

public class HopNGoFragment extends Fragment {
    private TextView distance,ett,numPeople,arrivingIn,rideEstimation,cabType,applyCouponHop,paymmentModeHop;
    private ImageView cabIcon;
    CabDetailHopNGo cabDetailHopNGo;
    private Context ctx;
    private ImageView paymentIconHopNGo;
    private double amt;
    private String estTime;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hopngo,container,false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx=context;
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if( arrivingIn != null ) {
            setEta();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        String cab = getArguments().getString(Constants.HOPNGO_CAB_DETAIL);
        cabDetailHopNGo=new Gson().fromJson(cab,CabDetailHopNGo.class);

        distance=getActivity().findViewById(R.id.distanceHop);
        numPeople=getActivity().findViewById(R.id.numPeople);
        arrivingIn=getActivity().findViewById(R.id.arrivingTimeHop);
        paymmentModeHop=getActivity().findViewById(R.id.paymmentModeHop);
        rideEstimation=getActivity().findViewById(R.id.rideEstimateHop);
        cabType=getActivity().findViewById(R.id.categoryTextHop);
        paymentIconHopNGo=getActivity().findViewById(R.id.paymentIconHopNGo);
        final Button confirmBookingHop = getActivity().findViewById(R.id.confirmBookingHop);
        applyCouponHop=getActivity().findViewById(R.id.applyCouponHop);
        cabIcon=getActivity().findViewById(R.id.cabIcon);

        ett=getActivity().findViewById(R.id.etaHop);
        final SessionManager sessionManager = new SessionManager(ctx);

        if( Double.parseDouble(sessionManager.getWalletBalance()) >= 100 ){
            paymmentModeHop.setText("wallet");
        }

        setCabIcon();
        setRideEstimate();
        setEta();

        applyCouponHop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickCoupon = new Intent(ctx, CouponActivity.class);
                pickCoupon.putExtra(Constants.TRIPTYPE,"2");
                pickCoupon.putExtra(Constants.VEHICLE_TYPE,cabDetailHopNGo.getId());
                pickCoupon.putExtra(Constants.COUPON_SOURCE,"hopngo");
                startActivityForResult ( pickCoupon, Constants.PICK_COUPONS);
            }
        });

        confirmBookingHop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if( Double.parseDouble(sessionManager.getWalletBalance() ) < 0 ){
                    //show previous balance dialog
                  showConfirmationDialog();

              } else {
                  confirmBookingHop();
              }

            }
        });

        paymmentModeHop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent paymentMode = new Intent (ctx, PaymentActivity.class);
                startActivityForResult( paymentMode, Constants.PAYMENT_CODE);

            }
        });
    }

    private void showConfirmationDialog() {

        dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmation_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        TextView tv=dialog.findViewById(R.id.message_cancel);
        TextView titleTv=dialog.findViewById(R.id.dialog_title);

        titleTv.setText("Balance Pending");
        String pendingMsg="Previous cancellation charge of "+getString(R.string.rupee_symbol)+new SessionManager(ctx).getWalletBalance()+" will be added in this ride fare.";
        tv.setText(pendingMsg);

        dialog.findViewById(R.id.ok_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        confirmBookingHop();
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        dialog.show();
    }

    private void confirmBookingHop() {

        cabDetailHopNGo.setPaymenyMode(paymmentModeHop.getText().toString());
        EventBus.getDefault().post(new Message("HideHopnGo"));

        Intent i=new Intent(ctx, WaitingScreenHopNGo.class);
        i.putExtra(Constants.HOPNGO_CAB_DETAIL,new Gson().toJson(cabDetailHopNGo));
        i.putExtra(Constants.RIDE_ESTIMATE,String.valueOf(amt));
        i.putExtra(Constants.ETA, estTime);
        i.putExtra(Constants.DRIVER_IDS, cabDetailHopNGo.getDriverId());
        startActivity(i);

    }

    private void setCabIcon() {
        switch (cabDetailHopNGo.getId()){
            case "1":cabIcon.setImageResource(R.drawable.lift_svg);
                break;
            case "2":cabIcon.setImageResource(R.drawable.cab_easy);
                break;
            case "3":cabIcon.setImageResource(R.drawable.easyplus_svg);
                break;
            case "5":cabIcon.setImageResource(R.drawable.easy_prime_svg);
                break;
            case "6":cabIcon.setImageResource(R.drawable.ranger_svg);
                break;
            default:cabIcon.setImageResource(R.drawable.lift_svg);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent( CouponDetail event ) {
        Log.d("aaaa","event");
        if(event.getSource().equals("hopngo")) {
            applyCouponHop.setText(event.getName());
            cabDetailHopNGo.setCouponId(event.getId());
            applyCouponHop.setTextColor(Color.parseColor("#6eff63"));
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentType event) {
        /* Do something */
        paymmentModeHop.setText(event.getPaymentType());

        if(event.getPaymentType().equals("cash")){
            paymentIconHopNGo.setImageResource(R.drawable.cash_icon);
        }else{
            paymentIconHopNGo.setImageResource(R.drawable.wallet_icon);
        }
    }

    private void setRideEstimate() {

        SessionManager sessionManager=new SessionManager(ctx);
        CabPricing categoryPrice= RealmManager.getCabCategory(cabDetailHopNGo.getId());

        if(categoryPrice != null) {

            float distanceCharge;
            int dis = sessionManager.getDestinationDistanceMeters() / 1000 - categoryPrice.getBaseKm();
            int rangeValue = categoryPrice.getRange2Lower();
            int charge1 = categoryPrice.getRange1Charge();
            int charge2 = categoryPrice.getRange2Charge();

            if (dis <= rangeValue) {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge1;
            } else {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge2;
            }

            amt = categoryPrice.getBaseFare()
                    + distanceCharge
                    + (sessionManager.getDestinationDurationSeconds() / 60) * categoryPrice.getPerMinCharge();

            amt = amt + (amt * sessionManager.getTaxRate() / 100);

            estTime = String.valueOf(sessionManager.getDestinationDurationSeconds() / 60);
            ett.setText(estTime + " min");
            rideEstimation.setText("Rs " + amt);
            distance.setText((sessionManager.getDestinationDistanceMeters() / 1000) + " Km");
            CabPricing cabTypes = RealmManager.getCabCategory(cabDetailHopNGo.getId());
            if(cabTypes != null) {
                cabType.setText("Cab Category: " + cabTypes.getCategoryName());
                numPeople.setText(cabTypes.getNumberOfSeats() + " People");
            }
        }

    }

    private void setEta() {
//        double arrTime=Double.parseDouble(cabDetailHopNGo.getArrivalTime())/0.6;
            arrivingIn.setText("Arriving in: "+ CommonMethods.getEta(RealmManager.getCabByCategory(cabDetailHopNGo.getId()).get(0),ctx));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
