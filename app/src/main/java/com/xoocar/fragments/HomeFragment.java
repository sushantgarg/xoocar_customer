package com.xoocar.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xoocar.Adapter.PagerAdapter;
import com.xoocar.EventBusModels.Message;
import com.xoocar.R;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sushant on 28/7/17.
 */

public class HomeFragment extends Fragment implements TabLayout.OnTabSelectedListener{

    private ViewPager mViewPager;
    Context ctx;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home,container,false);
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        ctx = context;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final TabLayout tabLayout =  getActivity().findViewById(R.id.tabLayout);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#6b6b6b"));

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Bike"));
        tabLayout.addTab(tabLayout.newTab().setText("Lift"));
        tabLayout.addTab(tabLayout.newTab().setText("Easy"));
        tabLayout.addTab(tabLayout.newTab().setText("Plus"));
        tabLayout.addTab(tabLayout.newTab().setText("Ranger"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.addOnTabSelectedListener(this);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

        mViewPager = getActivity().findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(4);
        PagerAdapter adapter = new PagerAdapter( fragmentManager, tabLayout.getTabCount() );
        mViewPager.setAdapter( adapter );

        EventBus.getDefault().post(new Message("0"));


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tabLayout.setScrollPosition(position,positionOffset,true);
            }

            @Override
            public void onPageSelected(int position) {
                EventBus.getDefault().post(new Message(""+position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    public void selectFirstFragment() {
        mViewPager.setCurrentItem(0);
        EventBus.getDefault().post(new Message("0"));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }
}
