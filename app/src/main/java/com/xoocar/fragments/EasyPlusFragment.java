package com.xoocar.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Constants;
import com.xoocar.CouponActivity;
import com.xoocar.EventBusModels.CouponDetail;
import com.xoocar.EventBusModels.Message;
import com.xoocar.EventBusModels.PaymentType;
import com.xoocar.PaymentActivity;
import com.xoocar.R;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.WaitingScreenRideNow;
import com.xoocar.models.CabPricing;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by sushant on 8/1/17.
 */

public class EasyPlusFragment extends Fragment implements View.OnClickListener {
    private Button confirmBooking;
    private Context ctx;
    private TextView arrivingTime;
    private TextView rideEstimate,etaText;
    private TextView distance,applyCoupon,paymentModePlus;
    private String couponId;
    private ImageView paymentIconEasyPlus;
    private SessionManager sessionManager;
    private double amt;
    private String estTime;
    private Dialog dialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_easy_plus,container,false);
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx=context;
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if( arrivingTime != null ) {
            setEta();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        arrivingTime= getActivity().findViewById(R.id.arrivingTimeEasyPlus);
        rideEstimate= getActivity().findViewById(R.id.rideEstimatePlus);
        etaText=  getActivity().findViewById(R.id.etaPlus);
        distance=  getActivity().findViewById(R.id.distancePlus);
        applyCoupon=  getActivity().findViewById(R.id.applyCouponPlus);
        confirmBooking=getActivity().findViewById(R.id.confirmBooking2);
        paymentModePlus=getActivity().findViewById(R.id.paymentModePlus);
        paymentIconEasyPlus=getActivity().findViewById(R.id.paymentIconEasyPlus);
        confirmBooking.setOnClickListener(this);
        applyCoupon.setOnClickListener(this);
        sessionManager=new SessionManager(ctx);

        if( Double.parseDouble(sessionManager.getWalletBalance()) >= 100f ){
            paymentModePlus.setText("wallet");
            paymentIconEasyPlus.setImageResource(R.drawable.wallet_svg);
        }


        paymentModePlus.setOnClickListener(this);
//        setEta();
//        setRideEstimate();
    }

    @Override
    public void onResume() {
        super.onResume();
        setEta();
        setRideEstimate();
    }

    private void setRideEstimate() {

        SessionManager sessionManager=new SessionManager(ctx);

        CabPricing categoryPrice=RealmManager.getCabCategory("3");

        if(categoryPrice != null) {

            float distanceCharge;
            int dis = sessionManager.getDestinationDistanceMeters() / 1000 - categoryPrice.getBaseKm();
            int rangeValue = categoryPrice.getRange2Lower();
            int charge1 = categoryPrice.getRange1Charge();
            int charge2 = categoryPrice.getRange2Charge();

            if (dis <= rangeValue) {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge1;
            } else {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge2;
            }

            amt = categoryPrice.getBaseFare()
                    + distanceCharge
                    + (sessionManager.getDestinationDurationSeconds() / 60) * categoryPrice.getPerMinCharge();

            amt = amt + (amt * sessionManager.getTaxRate() / 100);

            estTime = String.valueOf(sessionManager.getDestinationDurationSeconds() / 60);
            etaText.setText(estTime + " min");
            rideEstimate.setText("Rs " + (int) amt);
            distance.setText((sessionManager.getDestinationDistanceMeters() / 1000) + " Km");
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmBooking2:

                if( Double.parseDouble( sessionManager.getWalletBalance() ) > 0 ){
                    confirmBooking();
                } else {
                    // show previous balance dialog
                    showConfirmationDialog();
                }
                break;

            case R.id.applyCouponPlus:
                Intent pickCoupon = new Intent(ctx, CouponActivity.class);
                pickCoupon.putExtra(Constants.TRIPTYPE,"2");
                pickCoupon.putExtra(Constants.VEHICLE_TYPE,"3");
                pickCoupon.putExtra(Constants.COUPON_SOURCE,"plus");
                startActivityForResult ( pickCoupon, Constants.PICK_COUPONS);
                break;
            case R.id.paymentModePlus:
                Intent payment = new Intent(ctx, PaymentActivity.class);
                startActivityForResult ( payment, Constants.PAYMENT_CODE );
        }
    }

    private void showConfirmationDialog() {

        dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmation_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        TextView tv=dialog.findViewById(R.id.message_cancel);
        TextView titleTv=dialog.findViewById(R.id.dialog_title);

        titleTv.setText("Balance Pending");
        String pendingMsg="Previous cancellation charge of "+getString(R.string.rupee_symbol)+sessionManager.getWalletBalance()+" will be added in this ride fare.";
        tv.setText(pendingMsg);

        dialog.findViewById(R.id.ok_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        confirmBooking();
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        dialog.show();
    }

    private void confirmBooking() {
        List<CabsRealm> cabs = RealmManager.getCabByCategory("3");
        String driverIds = "";

        if (RealmManager.getCabByCategory("3").size() > 0) {
            EventBus.getDefault().post(new Message("HideRideNow"));

            for(int i=0;i<cabs.size();i++){
                if( i != cabs.size()-1 ) {
                    driverIds = driverIds+cabs.get(i).getUid()+",";
                }else{
                    driverIds = driverIds+cabs.get(i).getUid();
                }
            }

            Intent i=new Intent(ctx, WaitingScreenRideNow.class);
            i.putExtra(Constants.COUPON_ID,couponId);
            i.putExtra(Constants.PAYMENT_MODE, paymentModePlus.getText().toString());
            i.putExtra(Constants.VEHICLE_TYPE,"3");
            i.putExtra(Constants.RIDE_ESTIMATE,String.valueOf(amt));
            i.putExtra(Constants.ETA,estTime);
            i.putExtra(Constants.DRIVER_IDS,driverIds);
            ctx.startActivity(i);
        }else{
            Toast.makeText(ctx, "Not available in ths category", Toast.LENGTH_SHORT).show();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Message event) {
        /* Do something */
        Log.d("aaaa","event");
        if(event.getMessage().equals("RefreshCabs")) {
            setEta();
            setRideEstimate();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CouponDetail event) {
        Log.d("aaaa","event");
        if( event.getSource().equals("plus") ) {
            couponId = event.getId();
            applyCoupon.setText(event.getName());
            applyCoupon.setTextColor(Color.parseColor("#6eff63"));
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentType event) {
        /* Do something */
        paymentModePlus.setText(event.getPaymentType());

        if(event.getPaymentType().equals("cash")){
            paymentIconEasyPlus.setImageResource(R.drawable.cash_svg);
        }else{
            paymentIconEasyPlus.setImageResource(R.drawable.wallet_svg);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setEta() {
        if (RealmManager.getCabByCategory("3").size() > 0) {
            arrivingTime.setText("Arriving in:  " + RealmManager.getCabByCategory("3").get(0).getDurationValue()+ " min");
    } else {
        arrivingTime.setText("Not available");
    }
    }

}
