package com.xoocar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xoocar.Adapter.RideHistoryListAdapter;
import com.xoocar.R;
import com.xoocar.Realm.RealmManager;

/**
 * Created by sushant on 9/8/17.
 */

public class RideScheduleFragment extends Fragment {

    private Context ctx;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_ride_schedule,container,false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx=context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView scheduledList = getActivity().findViewById(R.id.rideScheduledList);
        scheduledList.setLayoutManager(new LinearLayoutManager(ctx));

        RideHistoryListAdapter adapter = new RideHistoryListAdapter(RealmManager.getRideHistoryByCategory("Schedule"), ctx);
        scheduledList.setAdapter(adapter);
    }
}
