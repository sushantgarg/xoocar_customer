package com.xoocar.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocar.Constants;
import com.xoocar.CouponActivity;
import com.xoocar.EventBusModels.CouponDetail;
import com.xoocar.EventBusModels.Message;
import com.xoocar.EventBusModels.PaymentType;
import com.xoocar.PaymentActivity;
import com.xoocar.R;
import com.xoocar.Realm.CabsRealm;
import com.xoocar.Realm.RealmManager;
import com.xoocar.SessionManager.SessionManager;
import com.xoocar.WaitingScreenRideNow;
import com.xoocar.models.CabPricing;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by sushant on 8/1/17.
 */

public class EasyRangerFragment extends Fragment implements View.OnClickListener {
    private Context ctx;
    private TextView arrivingTime,applyCoupon;
    private TextView rideEstimate,etaText,distance,paymentModeRanger;
    private String couponId;
    private ImageView paymentIconRanger;
    private SessionManager sessionManager;
    private double amt;
    private String estTime;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_easy_ranger,container,false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx=context;
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if( arrivingTime != null ) {
            setEta();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
        arrivingTime= getActivity().findViewById(R.id.arrivingTimeRanger);
        rideEstimate= getActivity().findViewById(R.id.rideEstimateRanger);
        Button confirmBooking = getActivity().findViewById(R.id.confirmBooking5);
        applyCoupon=getActivity().findViewById(R.id.applyCouponRanger);
        paymentModeRanger=getActivity().findViewById(R.id.paymentModeRanger);
        paymentIconRanger=getActivity().findViewById(R.id.paymentIconRanger);
        etaText=  getActivity().findViewById(R.id.etaRanger);
        distance=  getActivity().findViewById(R.id.distanceRanger);

        paymentModeRanger.setOnClickListener(this);
        confirmBooking.setOnClickListener(this);
        applyCoupon.setOnClickListener(this);
        sessionManager=new SessionManager(ctx);

        if( Double.parseDouble(sessionManager.getWalletBalance()) > 100f ){
            paymentModeRanger.setText("wallet");
            paymentIconRanger.setImageResource(R.drawable.wallet_svg);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setEta();
        setRideEstimate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmBooking5:

                if( Double.parseDouble( sessionManager.getWalletBalance() ) > 0 ){
                    confirmBooking();
                } else {
                    // show previous balance dialog
                    showConfirmationDialog();
                }
                break;
            case R.id.applyCouponRanger:
                Intent pickCoupon = new Intent(ctx, CouponActivity.class);
                pickCoupon.putExtra(Constants.TRIPTYPE,"2");
                pickCoupon.putExtra(Constants.VEHICLE_TYPE,"6");
                pickCoupon.putExtra(Constants.COUPON_SOURCE,"ranger");
                startActivityForResult ( pickCoupon, Constants.PICK_COUPONS);
                break;
            case R.id.paymentModeRanger:
                Intent payment = new Intent(ctx, PaymentActivity.class);
                startActivityForResult ( payment, Constants.PAYMENT_CODE );
        }
    }

    private void showConfirmationDialog() {
        dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmation_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        TextView tv=dialog.findViewById(R.id.message_cancel);
        TextView titleTv=dialog.findViewById(R.id.dialog_title);

        titleTv.setText("Balance Pending");
        String pendingMsg="Previous cancellation charge of "+getString(R.string.rupee_symbol)+sessionManager.getWalletBalance()+" will be added in this ride fare.";
        tv.setText(pendingMsg);

        dialog.findViewById(R.id.ok_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        confirmBooking();
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

        dialog.show();

    }

    private void confirmBooking() {
        if( RealmManager.getCabByCategory("6").size() > 0 ) {
            EventBus.getDefault().post(new Message("HideRideNow"));

            List<CabsRealm> cabs = RealmManager.getCabByCategory("6");
            String driverIds = "";

            for(int i=0;i<cabs.size();i++){
                if( i != cabs.size()-1 ) {
                    driverIds = driverIds+cabs.get(i).getUid()+",";
                }else{
                    driverIds = driverIds+cabs.get(i).getUid();
                }
            }

            Intent i=new Intent(ctx, WaitingScreenRideNow.class);
            i.putExtra(Constants.COUPON_ID,couponId);
            i.putExtra(Constants.PAYMENT_MODE, paymentModeRanger.getText().toString());
            i.putExtra(Constants.VEHICLE_TYPE,"6");
            i.putExtra(Constants.RIDE_ESTIMATE,String.valueOf(amt));
            i.putExtra(Constants.ETA,estTime);
            i.putExtra(Constants.DRIVER_IDS,driverIds);
            ctx.startActivity(i);
        } else {
            Toast.makeText(ctx, "Not available in this category", Toast.LENGTH_SHORT).show();
        }

    }

    private void setRideEstimate() {

        SessionManager sessionManager=new SessionManager(ctx);

        CabPricing categoryPrice=RealmManager.getCabCategory("6");

        if(categoryPrice != null) {

            float distanceCharge;
            int dis = sessionManager.getDestinationDistanceMeters() / 1000 - categoryPrice.getBaseKm();
            int rangeValue = categoryPrice.getRange2Lower();
            int charge1 = categoryPrice.getRange1Charge();
            int charge2 = categoryPrice.getRange2Charge();

            if (dis <= rangeValue) {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge1;
            } else {
                distanceCharge = (sessionManager.getDestinationDistanceMeters() / 1000) * charge2;
            }

            amt = categoryPrice.getBaseFare()
                    + distanceCharge
                    + (sessionManager.getDestinationDurationSeconds() / 60) * categoryPrice.getPerMinCharge();

            amt = amt + (amt * sessionManager.getTaxRate() / 100);

            estTime = String.valueOf(sessionManager.getDestinationDurationSeconds() / 60);
            etaText.setText(estTime + " min");
            rideEstimate.setText("Rs " + (int) amt);
            distance.setText((sessionManager.getDestinationDistanceMeters() / 1000) + " Km");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Message event) {
        if(event.getMessage().equals("RefreshCabs")) {
            setEta();
            setRideEstimate();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CouponDetail event) {
        Log.d("aaaa","event");
        if( event.getSource().equals("ranger") ) {
            couponId = event.getId();
            applyCoupon.setText(event.getName());
            applyCoupon.setTextColor(Color.parseColor("#6eff63"));
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentType event) {
        /* Do something */
        paymentModeRanger.setText(event.getPaymentType());

        if(event.getPaymentType().equals("cash")){
            paymentIconRanger.setImageResource(R.drawable.cash_svg);
        }else{
            paymentIconRanger.setImageResource(R.drawable.wallet_svg);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setEta() {
        if( RealmManager.getCabByCategory("6").size() > 0 ) {
            arrivingTime.setText("Arriving in:  " + RealmManager.getCabByCategory("6").get(0).getDurationValue()+ " min");
        } else {
            arrivingTime.setText("Not available");
        }
    }
}
