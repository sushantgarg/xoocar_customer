package com.xoocar.RetroFit;

import com.xoocar.Requests.AddCouponAfterScheduled.AddCouponLaterRequestFields;
import com.xoocar.Requests.AddCouponAfterScheduled.AddCouponLaterResponceFields;
import com.xoocar.Requests.BillDetail.BillDetailRequestFields;
import com.xoocar.Requests.BillDetail.BillDetailsResponceFields;
import com.xoocar.Requests.CancelRide.CancelRideRequestFields;
import com.xoocar.Requests.CancelRide.CancelRideResponce;
import com.xoocar.Requests.CancelScheduledRide.CancelRideResponceFields;
import com.xoocar.Requests.ChangePaymentMode.ChangePAymentRequestFields;
import com.xoocar.Requests.ChangePaymentMode.ChangePaymentResponceFields;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsRequestFields;
import com.xoocar.Requests.CheckUserExist.CheckUserExistsResponceFields;
import com.xoocar.Requests.ConfirmBooking.FieldsGetBooking;
import com.xoocar.Requests.ConfirmBooking.ResponceBooking;
import com.xoocar.Requests.ConfirmIntercityBooking.ConfirmIntercityRequestFields;
import com.xoocar.Requests.ConfirmIntercityBooking.ConfirmIntercityResponceFields;
import com.xoocar.Requests.EditDestination.EditDestinationResponceFields;
import com.xoocar.Requests.EditDestination.EditDestinationrequestFields;
import com.xoocar.Requests.GetCabs.GetCabsFields;
import com.xoocar.Requests.GetCabs.ResponceModel;
import com.xoocar.Requests.GetCategoyRateCard.GetCategoryRateCardRequestFields;
import com.xoocar.Requests.GetCategoyRateCard.GetCategoryRateCardResponceFields;
import com.xoocar.Requests.GetCityId.GetCityIdRequestFields;
import com.xoocar.Requests.GetCityId.GetCityIdResponceFields;
import com.xoocar.Requests.GetConfigeration.GetConfigRequestFields;
import com.xoocar.Requests.GetConfigeration.GetConfigResponceFields;
import com.xoocar.Requests.GetCoupons.CouponResponceFields;
import com.xoocar.Requests.GetCoupons.GetCoupons;
import com.xoocar.Requests.GetDriverPosition.GetDriverPositionResponce;
import com.xoocar.Requests.GetDriverPosition.GetDriverRequestFields;
import com.xoocar.Requests.GetRateCard.GetRateCardRequestFields;
import com.xoocar.Requests.GetRateCard.GetRateCardResponceFields;
import com.xoocar.Requests.GetRideById.RequestFieldsGetCabById;
import com.xoocar.Requests.GetRideById.RequestModelGetCabById;
import com.xoocar.Requests.HopNGo.RequestFieldsHopnGo;
import com.xoocar.Requests.HopNGo.ResponceHopnGo;
import com.xoocar.Requests.IntercityCities.GetCitiesRequest;
import com.xoocar.Requests.IntercityCities.GetCitiesResponceFields;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsRequestFields;
import com.xoocar.Requests.IntercityDestination.GetIntercityDestsResponceFields;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateRequestFields;
import com.xoocar.Requests.IntercityRideRstimate.GetRideEstimateResponceFields;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneRequestFields;
import com.xoocar.Requests.IsRatingDone.IsRatingDoneResponceFields;
import com.xoocar.Requests.Login.LoginRequestFields;
import com.xoocar.Requests.Login.LoginResponceFields;
import com.xoocar.Requests.ResendBooking.RequestFields;
import com.xoocar.Requests.ResendBooking.ResponceFields;
import com.xoocar.Requests.RideHistory.RideHIstoryRequestFields;
import com.xoocar.Requests.RideHistory.RideHIstoryResponceFields;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailRequestField;
import com.xoocar.Requests.RideHistoryDetail.RideHistoryDetailResponceField;
import com.xoocar.Requests.SendRefferal.SendReferalRequestFields;
import com.xoocar.Requests.SendRefferal.SendReferalResponceFields;
import com.xoocar.Requests.SignUp.SignUpRequestFields;
import com.xoocar.Requests.SubmitQuerry.SubmitQuerryRequestFields;
import com.xoocar.Requests.SubmitQuerry.SubmitQuerryResponceFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingRequetFields;
import com.xoocar.Requests.SubmitRating.SubmitRatingResponce;
import com.xoocar.Requests.WalletBalanceRequest.RequestWalletBalance;
import com.xoocar.Requests.WalletBalanceRequest.ResponceWalletBalance;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by sushant on 8/2/17.
 */

public interface RequestInterface {

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<ResponceModel> getOrders(@Header("auth-token") String headerValue,@Url String url, @Body GetCabsFields orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<CouponResponceFields> getCoupons(@Header("auth-token") String headerValue,@Url String url, @Body GetCoupons orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<ResponceHopnGo> getHopnGoCabs(@Header("auth-token") String headerValue,@Url String url, @Body RequestFieldsHopnGo orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<ResponceWalletBalance> getWalletBalance(@Header("auth-token") String headerValue,@Url String url, @Body RequestWalletBalance orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<ResponceBooking> requestBooking(@Header("auth-token") String headerValue,@Url String url, @Body FieldsGetBooking orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<ResponceFields> resendBooking(@Header("auth-token") String headerValue,@Url String url, @Body RequestFields orders);


//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<RequestFieldsGetCabById> requestCabById(@Header("auth-token") String headerValue,@Url String url, @Body RequestModelGetCabById orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<CancelRideResponce> cancelRide(@Header("auth-token") String headerValue,@Url String url, @Body CancelRideRequestFields orders);

//    @Headers("auth-token:9a7bd55975c21dffb7b14bfc691943d60ed89f5c61a0c541fa86a3e0716741f1d7c2dbc4b78f7ce5ac977fa7e6578a15705912e0a28d8d315d005ee52302776b")
    @POST()
    Call<GetDriverPositionResponce> getDriverPosition(@Header("auth-token") String headerValue,@Url String url, @Body GetDriverRequestFields orders);

    @POST()
    Call<CheckUserExistsResponceFields> isUserExist(@Url String url, @Body CheckUserExistsRequestFields orders);

    @POST()
    Call<LoginResponceFields> login(@Url String url, @Body LoginRequestFields orders);

    @POST()
    Call<LoginResponceFields> signUp(@Url String url, @Body SignUpRequestFields orders);

    @POST()
    Call<BillDetailsResponceFields> getBillDetail(@Header("auth-token") String headerValue, @Url String url, @Body BillDetailRequestFields orders);

    @POST()
    Call<SubmitRatingResponce> submitrating(@Header("auth-token") String headerValue, @Url String url, @Body SubmitRatingRequetFields orders);

    @POST()
    Call<CancelRideResponceFields> cancelScheduledRide(@Header("auth-token") String headerValue, @Url String url, @Body CancelRideRequestFields orders);

    @POST()
    Call<IsRatingDoneResponceFields> isRatingDone(@Header("auth-token") String headerValue, @Url String url, @Body IsRatingDoneRequestFields orders);

    @POST()
    Call<ChangePaymentResponceFields> changePayment(@Header("auth-token") String headerValue, @Url String url, @Body ChangePAymentRequestFields orders);

    @POST()
    Call<AddCouponLaterResponceFields> changeCoupon(@Header("auth-token") String headerValue, @Url String url, @Body AddCouponLaterRequestFields orders);

    @POST()
    Call<SendReferalResponceFields> sendReferal(@Header("auth-token") String headerValue, @Url String url, @Body SendReferalRequestFields orders);

    @POST()
    Call<EditDestinationResponceFields> editDestination(@Header("auth-token") String headerValue, @Url String url, @Body EditDestinationrequestFields orders);

    @POST()
    Call<RideHIstoryResponceFields> rideHistory(@Header("auth-token") String headerValue, @Url String url, @Body RideHIstoryRequestFields orders);


    @POST()
    Call<RideHistoryDetailResponceField> rideHistoryDetail(@Header("auth-token") String headerValue, @Url String url, @Body RideHistoryDetailRequestField orders);

     @POST()
    Call<SubmitQuerryResponceFields> submitQuerry(@Header("auth-token") String headerValue, @Url String url, @Body SubmitQuerryRequestFields orders);

    @POST()
    Call<GetCityIdResponceFields> getCities(@Header("auth-token") String headerValue, @Url String url, @Body GetCityIdRequestFields orders);


    @POST()
    Call<GetCategoryRateCardResponceFields> getCategoryRateCard(@Header("auth-token") String headerValue, @Url String url, @Body GetCategoryRateCardRequestFields orders);

     @POST()
        Call<GetRateCardResponceFields> getRateCard(@Header("auth-token") String headerValue, @Url String url, @Body GetRateCardRequestFields orders);

    @POST()
    Call<GetCitiesResponceFields> getIntercityCities(@Header("auth-token") String headerValue, @Url String url, @Body GetCitiesRequest orders);

    @POST()
    Call<GetIntercityDestsResponceFields> getIntercityDestination(@Header("auth-token") String headerValue, @Url String url, @Body GetIntercityDestsRequestFields orders);

    @POST()
    Call<GetRideEstimateResponceFields> getIntercityRideEstimate(@Header("auth-token") String headerValue, @Url String url, @Body GetRideEstimateRequestFields orders);

    @POST()
    Call<ConfirmIntercityResponceFields> confirmIntercityRequest(@Header("auth-token") String headerValue, @Url String url, @Body ConfirmIntercityRequestFields orders);

    @POST()
    Call<GetConfigResponceFields> getConfigurations( @Url String url, @Body GetConfigRequestFields orders);

}
